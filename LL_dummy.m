% returns log likelihood of data given parameters
% for the purpose of sampling from the prior:
% i.e. a constant

function [LL,is_upper_bound] = LL_dummy(~,~,~)

LL = 0;
is_upper_bound = 0;

end