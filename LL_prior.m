% calculates the log likelihood of parmaeters given priors
% returns 0 if all parameters are within range, -Inf otherwise

% input arguments:
% p: struct containing parameter values.
% of the same form as that constructed by construct_parameters_single.
% mex_flag: logical. 1 iff solving ODEs using precompiled files.

% output argument:
% LL: scalar.  0 if all parameters are within range, -Inf otherwise

function LL = LL_prior(p,mex_flag)
LL = -Inf;

range = return_range();

names = {p.pname};
p_subset = [p(~strcmp(names,'interval')).value];
% first check that all parameters are within prior bounds
in_range = all(p_subset >= range(:,1)' & p_subset <= range(:,2)');

if(in_range)
    nV = 1;
    % then check that beta and p are within bounds
    [p1,in_range_auxiliary] = transform(p,'baseline');
    if(in_range_auxiliary)
        % then check minimal conditions outlined in SM A
        c = compartment_labelling(nV,'baseline');
        iv = find_iv_subset(p,c,'baseline');
        max_time = 8;
        if(mex_flag)
            timespan = [0,max_time];
            int_tol = [1e-6 1e-8 1];
            [sol.t,sol.y] = ODE_prior_c(timespan,iv,p1,[],int_tol);
        else
            timespan = 0:.1:max_time;
            [sol.y,~,~] = lsode(@(x,t) ODE_prior(t,x,p1,[]),iv,timespan);
            sol.y = sol.y';
            sol.t = timespan;
        end
        
        [peak_viral_load,maxloc] = max(sol.y(c.VR,:));
        viral_load_thres = iv(end)*10;
        % viral load rises by at least one order of magnitude
        high_viral_load = peak_viral_load > viral_load_thres;
        
        peak_time = sol.t(maxloc) < 7; % peak viral load < 7 days
        
        immune_thres = 1e3;
        end_early_period = find(sol.t>5,1);
        pA_times_kappaA_times_B_01 = find_mu(p,names,'pA_times_kappaA_times_B_0');
        % antibody levels don't rise too early
        abs_level = pA_times_kappaA_times_B_01*sol.y(c.A,end_early_period) < immune_thres;
        
        C_0_times_kappaE_times_a_times_d11 = find_mu(p,names,'C_0_times_kappaE_times_a_times_d11');
        kC_on_a_times_d11 = find_mu(p,names,'kC_on_a_times_d11');
        kC_on_a_times_d12 = find_mu(p,names,'kC_on_a_times_d12');
        % T cell levels don't rise too early
        CD8_level = C_0_times_kappaE_times_a_times_d11*(sol.y(c.C(1),end_early_period) ...
            + sum(sol.y(c.E(:,1),end_early_period))...
            + sol.y(c.M(1),end_early_period)) +...
            C_0_times_kappaE_times_a_times_d11*kC_on_a_times_d11/kC_on_a_times_d12...
            *(sol.y(c.C(2),end_early_period) ...
            + sum(sol.y(c.E(:,2),end_early_period))...
            + sol.y(c.M(2),end_early_period)) < immune_thres;
        
        if(high_viral_load && peak_time && abs_level && CD8_level)
            LL = 0;
        end
    end
end
end

function out = find_mu(p,names,pname)
out = 10^p(strcmp(names,pname)).value;
end