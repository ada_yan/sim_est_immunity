% returns the log likelihood of the data given model parameters

% input arguments:
% data: struct constructed by construct_data_and_model
% observables: struct returned by return_viral_load given parameters
% sigma: scalar. standard deviation of measurement error

% output arguments
% LL: scalar. log likelihood
% is_upper_bound: logical. If the integration failed at some timepoints, LL is an upper
% bound of the actual likelihood.

function [LL,is_upper_bound] = LL_virus(data,observables,sigma)

obs_threshold = 10;
LL = 0;
is_upper_bound = 0;
viruses = fieldnames(data);
viruses(strcmp(viruses,'parameters')) = [];

for i = 1:length(viruses)
    [LL_new,is_upper_bound_new] = calc_LL(data.(viruses{i}),observables.(viruses{i}),sigma,obs_threshold);
    LL = LL + LL_new;
    is_upper_bound = is_upper_bound + is_upper_bound_new;
end

end

% if data above threshold, probability of data given observables is
% normally distributed
% probability of data below threshold given observable is the integral of
% the normal distribution centred at observable, up to the threshold
% if some observables are absent, is upper bound
function [LL,is_upper_bound] =calc_LL(data,observables,sigma,obs_threshold)
% to ensure that when numerical precision affects value of observables, a
% small difference (e.g. observables = 1e-6 vs. observables = 0) doesn't
% send the likelihood from finite to -Inf
numerical_threshold = 1e-6;
observables = max(observables,numerical_threshold); 
% indices where the data is above the threshold and integration was
% successful -- lognormally distributed error
above_threshold_and_present = (data > obs_threshold) & (observables >= 0);
% indices where the data is above the threshold and integration was
% unsuccessful -- calculate uppoer bound of LL at these points
above_threshold_and_absent = (data > obs_threshold) & (observables == -1);
% indices where the data is below the threshold and integration was
% successful -- treat data as censored
below_threshold_and_present = (data == 0) & (observables >= 0);
% ignore indices where the data is below the threshold and integration was
% unsuccessful

is_upper_bound = any((data >= 0) & (observables == -1));

LL_above_threshold_and_present = log(log10npdf(data(above_threshold_and_present),log10(observables(above_threshold_and_present)),sigma));
LL_above_threshold_and_absent = log(log10npdf(data(above_threshold_and_absent),log10(data(above_threshold_and_absent)),sigma));
LL_below_threshold_and_present = log(log10ncdf(obs_threshold,log10(observables(below_threshold_and_present)),sigma));
LL = sum(LL_above_threshold_and_present) + ...
    sum(LL_above_threshold_and_absent) + ...
    sum(LL_below_threshold_and_present);
end