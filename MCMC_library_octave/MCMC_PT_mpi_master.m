% runs MCMC chains for different temperatures in parallel, and performs
% swaps

% outputs final models (including parameters) for each temperature and
% joint posterior log likelihood for each temperature


% input arguments:
% data: struct created by model_construction_fn
% model: struct array of models for all parallel chains,
% each created by model_construction_fn
% MCMC_options: struct created by MCMC_options_fn
% mpi_info: struct: info pertaining to rank of process, total number of processes etc.
% created by allocate_models_to_processes

function MCMC_PT_mpi_master(data,model,MCMC_options,mpi_info) %#ok<INUSL>

% number of potential swaps for each chain
nswaps = round(MCMC_options.total_iterations/MCMC_options.iterations_per_swap);

% offset toggles between swapping 1<-->2, 3<-->4... and 2<-->3, 4<-->5...
offset = 0;

% number of actual swaps conducted for each chain
swaps = zeros(1,length(MCMC_options.temperatures)-1);
% log likelihood before each swap for each chain
master_likelihood_last = zeros(length(MCMC_options.temperatures),1);
need_intervention = master_likelihood_last;
% acceptance ratio for each chain
master_accept_count = zeros(length(MCMC_options.temperatures),length(model(1).parameters));

mpi = ~isempty(mpi_info);

model_temp = rm_fnhandle(model);
save(strcat(MCMC_options.out_filename,'master.mat'),...
    'data','model_temp','MCMC_options','-v7');

% if running in series
if(~mpi)
    mpi_info.my_parameters = find([model(1).parameters.fit_option] == 1);
    % generate random starting values
    for i = 1:length(MCMC_options.temperatures)
        if(MCMC_options.random_start)
            model(i).parameters = model(1).generate_random_parameters(data,model(i));
        end
        [likelihood,~] = model(1).return_likelihood(model(i).parameters,data,model(i),0,[],1);
        master_likelihood_last(i) = sum(likelihood);
    end
else
    % submasters to send models to
    % receive random starting values from submasters
    dest = mpi_info.total_chain_split(1,:);
    for i = 1:length(MCMC_options.temperatures)
        if(MCMC_options.random_start)
            [model(i).parameters.value] = num2cell(MPI_Recv(dest(i), 2*i-1, mpi_info.MPI_COMM_WORLD)){:};
        end
        master_likelihood_last(i) = MPI_Recv(dest(i),2*i,mpi_info.MPI_COMM_WORLD);
    end
end

% setup filenames to write out
fid.LL = strcat(MCMC_options.out_filename,'log_likelihood.csv');
fid.swaps = strcat(MCMC_options.out_filename,'swaps.csv');
fid.acceptance = cell(size(MCMC_options.temperatures));
fid.proposal_widths = fid.acceptance;
for i = 1:length(fid.acceptance)
    fid.acceptance{i} = strcat(MCMC_options.out_filename,'acceptance',num2str(i),'.csv');
    fid.proposal_widths{i} = strcat(MCMC_options.out_filename,'proposal_widths',num2str(i),'.csv');
end
if(MCMC_options.calibrate_temperatures)
    fid.temps = strcat(MCMC_options.out_filename,'temperatures.csv');
    csvwrite(fid.temps,MCMC_options.temperatures,'append','on');
end
% write initial LL
csvwrite(fid.LL,horzcat(0,master_likelihood_last'),'append','on');

% infrastructure for sending and receiving from submasters
n_messages = 4;
tag = 1:length(MCMC_options.temperatures);
tag = n_messages*tag;

model_temp = rm_fnhandle(model);
mpi_info_temp = mpi_info;
if(mpi)
    mpi_info_temp = rmfield(mpi_info_temp,'MPI_COMM_WORLD');
end
save(strcat(MCMC_options.out_filename,'master.mat'),...
    'data','model_temp','mpi_info_temp','MCMC_options','-v7');


for k = 1:nswaps
    tic
    % if running in series, run everything myself
    if(~mpi)
        for n = 1:MCMC_options.iterations_per_swap
            for j = 1:length(MCMC_options.temperatures)
                [model(j), master_accept_count(j,:),likelihood_last,...
                    intervention] = MCMC_subset_parameters...
                    (data,model(j),MCMC_options,j,mpi_info.my_parameters,j);
                master_likelihood_last(j) = sum(likelihood_last);
                % flag for manual intervention if evaluating log likelihood went wrong
                if(isempty(intervention))
                    need_intervention(j) = 0;
                else
                    need_intervention(j) = min(intervention);
                end
            end
        end
    else
        tag = tag + n_messages*length(MCMC_options.temperatures);
        if(k ~= 1) % already have information if first iteration
            % master sends parameters to submasters
            for j = 1:length(MCMC_options.temperatures)
                % send model j to dest, where dest = submaster assigned model j
                % note: sending cells unreliable, hence split up message
                MPI_Send([model(j).parameters.value], dest(j), tag(j), mpi_info.MPI_COMM_WORLD);
                MPI_Send([model(j).parameters.proposal_width], dest(j), tag(j)+1, mpi_info.MPI_COMM_WORLD);
                MPI_Send(MCMC_options.temperatures, dest(j), tag(j)+2, mpi_info.MPI_COMM_WORLD);
            end
            % submasters and slaves run MCMC here
        end
        
        for j = 1:length(MCMC_options.temperatures)
            % receive model j from dest, where dest = submaster assigned model j
            [model(j).parameters.value] = num2cell(MPI_Recv(dest(j),tag(j),mpi_info.MPI_COMM_WORLD)){:};
            master_likelihood_last(j) = MPI_Recv(dest(j),tag(j)+1,mpi_info.MPI_COMM_WORLD);
            master_accept_count(j,:) = master_accept_count(j,:) + ...
                MPI_Recv(dest(j),tag(j)+2,mpi_info.MPI_COMM_WORLD);
            need_intervention(j) = MPI_Recv(dest(j),tag(j)+3,mpi_info.MPI_COMM_WORLD);
        end
    end

    % flag for manual intervention if evaluating log likelihood went wrong

    if(any(need_intervention))
        model_temp = rm_fnhandle(model);
        dir_string = strcat(MCMC_options.out_filename,'intervention/master/');
        if(~exist(dir_string,'dir'))
            mkdir_retro(dir_string);
        end
        save(sprintf('%s%d_master.mat',dir_string,min(need_intervention(need_intervention > 0))),...
            'k','need_intervention','data','model_temp','mpi_info_temp','MCMC_options','-v7');
    end
    
    csvwrite(fid.LL,horzcat(k,master_likelihood_last'),'append','on');
    
    % one round of swaps: consider swapping each adjacent pair of chains
    [model,master_likelihood_last,offset,swaps] = parallel_tempering(model,master_likelihood_last,MCMC_options.temperatures,offset,swaps);
    
    csvwrite(fid.LL,horzcat(k,master_likelihood_last'),'append','on');
    
    % calibrate proposal widths

    if(MCMC_options.calibrate_proposal_widths)
        spacing = MCMC_options.calibrate_proposal_widths;
    else
        spacing = 10;
    end
    if(mod(k,spacing) == 0)
        master_accept_ratio = master_accept_count/MCMC_options.calibrate_proposal_widths/...
            MCMC_options.iterations_per_swap;
        if(MCMC_options.calibrate_proposal_widths)
            model = calibrate_proposal_width(model,master_accept_ratio);
        end
        for i = 1:length(MCMC_options.temperatures)
            % write acceptance count either when calibrating or every 10
            % swaps
            csvwrite(fid.acceptance{i},master_accept_ratio(i,:),'append','on');
            csvwrite(fid.proposal_widths{i},[model(i).parameters.proposal_width],'append','on');
        end
        master_accept_count = zeros(size(master_accept_count));
    end
    
    % calibrate temperatures if desired
    if(mod(k,MCMC_options.calibrate_temperatures) == 0)
        csvwrite(fid.swaps,swaps/MCMC_options.calibrate_temperatures,'append','on');
        MCMC_options.temperatures = calibrate_temperatures(MCMC_options.temperatures,swaps/MCMC_options.calibrate_temperatures);
        swaps = zeros(size(swaps));
        csvwrite(fid.temps,MCMC_options.temperatures,'append','on');
    end
    a = toc;
    
    fprintf('%d total iterations \n',MCMC_options.iterations_per_swap*k);
    fprintf('Last %d iterations in %f s \n',MCMC_options.iterations_per_swap,a);
    model_temp = rm_fnhandle(model);
    save(strcat(MCMC_options.out_filename,'master.mat'),...
        'k','data','model_temp','mpi_info_temp','MCMC_options','-v7');
end

fclose('all');

end
