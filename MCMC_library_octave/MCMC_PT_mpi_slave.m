% runs MCMC chains for different temperatures in parallel, and performs
% swaps

% outputs final models (including parameters) for each temperature and
% joint posterior log likelihood for each temperature

% input arguments:
% data: struct created by model_construction_fn
% model: struct array of models for one or more parallel chains
% for which the slave is responsible, each created by model_construction_fn
% MCMC_options: struct created by MCMC_options_fn
% mpi_info: struct: info pertaining to rank of process, total number of processes etc.
% created by allocate_models_to_processes

function MCMC_PT_mpi_slave(data,model,MCMC_options,mpi_info)
% number of potential swaps for each chain
nswaps = round(MCMC_options.total_iterations/MCMC_options.iterations_per_swap);

for k = 1:nswaps
    % processes run MCMC for each chain they are responsible for
    MCMC_mpi_slave(data,model,MCMC_options,mpi_info,mpi_info.models_received);
end

fclose('all');

end
