% runs MCMC chains for different temperatures in parallel, and performs
% swaps

% outputs final models (including parameters) for each temperature and
% joint posterior log likelihood for each temperature

% input arguments:
% data: struct created by model_construction_fn
% model: struct array of models for one or more parallel chains
% for which the submaster is responsible, each created by model_construction_fn
% MCMC_options: struct created by MCMC_options_fn
% mpi_info: struct: info pertaining to rank of process, total number of processes etc.
% created by allocate_models_to_processes

function MCMC_PT_mpi_submaster(data,model,MCMC_options,mpi_info)

% number of potential swaps for each chain
nswaps = round(MCMC_options.total_iterations/MCMC_options.iterations_per_swap);

% initialise
fid = [];
likelihood_last = zeros(size(mpi_info.models_received));
need_intervention = likelihood_last;
accept_count = zeros(length(mpi_info.models_received),length(model(1).parameters));

for i = 1:length(mpi_info.models_received)

    % generate random starting values if necessary
    if(MCMC_options.random_start)
        model(i).parameters = model.generate_random_parameters(data,model(i));
        % and send them to master
        MPI_Send([model(i).parameters.value], 0, 2*mpi_info.models_received(i)-1, mpi_info.MPI_COMM_WORLD);
    end

    % calculate log likelihood for starting values
    [likelihood,~] = model.return_likelihood(model(i).parameters,data,model(i),0,[],1);
    likelihood_last(i) = sum(likelihood);
    % and send them to master
    MPI_Send(likelihood_last(i), 0, 2*mpi_info.models_received(i), mpi_info.MPI_COMM_WORLD);
    [model(i).parameters.proposal_width] = ...
    num2cell([model(i).parameters.proposal_width_max]*MCMC_options.proposal_width_weight(mpi_info.models_received(i))){:};
end

% for cold chain
if(mpi_info.rnk == 1)
    % open files to write values of parameters and observables for cold chain for each iteration
    fid.params = strcat(MCMC_options.out_filename,'parameters.csv');
    csvwrite(fid.params,[model(1).parameters.value],'append','on');
    observables = model(1).equations.return_observables(model(1).parameters(data(1).parameters),data(1));
    obsname = fieldnames(observables);
    fid.obs = cell(length(data),length(obsname));
    for i = 1:length(data)
        for m = 1:length(obsname)
            fid.obs{i,m} = strcat(MCMC_options.out_filename,...
                obsname{m},num2str(i),'.csv');
        end
    end
end

% infrastructure for sending and receiving from master
n_messages = 4;
tag = n_messages*mpi_info.models_received;

% run MCMC

for k = 1:nswaps
    tag = tag + n_messages*length(MCMC_options.temperatures);
    %     submasters receive parameters from master for each chain they are responsible for
    if(k ~= 1) % already have information if at first iteration
        for i = 1:length(mpi_info.models_received)
            [model(i).parameters.value] = num2cell(MPI_Recv(0, tag(i), mpi_info.MPI_COMM_WORLD)){:};
            [model(i).parameters.proposal_width] = num2cell(MPI_Recv(0, tag(i)+1, mpi_info.MPI_COMM_WORLD)){:};
            MCMC_options.temperatures = MPI_Recv(0, tag(i)+2, mpi_info.MPI_COMM_WORLD);
        end
    end
    for i = 1:length(mpi_info.models_received)
        % processes run MCMC for each chain they are responsible for
        [model(i), likelihood_last(i), accept_count(i,:),need_intervention(i)] = ...
            MCMC_mpi_submaster(data,model(i),MCMC_options,mpi_info,mpi_info.models_received(i),fid);
    end
    %     note two for loops such that the ordering of the loop can be
    %     different from that of the master
    for i = 1:length(mpi_info.models_received)
        % submasters relay the results back to the master
        MPI_Send([model(i).parameters.value], 0, tag(i), mpi_info.MPI_COMM_WORLD);
        MPI_Send(likelihood_last(i), 0, tag(i)+1, mpi_info.MPI_COMM_WORLD);        
        MPI_Send(accept_count(i,:), 0, tag(i)+2, mpi_info.MPI_COMM_WORLD);
        MPI_Send(need_intervention(i), 0, tag(i)+3, mpi_info.MPI_COMM_WORLD);
    end
end

fclose('all');

end
