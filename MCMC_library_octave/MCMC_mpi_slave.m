% function to calculate final estimate of parameters after 
% MCMC_options.iterations_per_swap iterations given data, model,
% initial estimate of parameters using MCMC chain at a given temperature

% input arguments:
% data: struct created by model_construction_fn
% model: struct of model for parallel chain for which
% the submaster is responsible,
% each created by model_construction_fn
% MCMC_options: struct created by MCMC_options_fn
% mpi_info: struct: info pertaining to rank of process, total number of processes etc.
% created by allocate_models_to_processes
% model_no: index of this parallel chain

function MCMC_mpi_slave(data,model,MCMC_options,mpi_info,model_no)


% receive proposal widths and temperatures from submaster
tag = 4*MCMC_options.iterations*length(mpi_info.my_parameters);
[model.parameters.proposal_width] = num2cell(MPI_Recv(mpi_info.models_received, tag, mpi_info.MPI_COMM_WORLD)){:};
MCMC_options.temperatures = MPI_Recv(mpi_info.models_received, tag+1, mpi_info.MPI_COMM_WORLD);

for n = 1:MCMC_options.iterations_per_swap
    for j = 1:length(mpi_info.my_parameters)
        % slaves receive
        % parameters from submaster
        tag = n*length(mpi_info.my_parameters) + j;
        [model.parameters.value] = num2cell(MPI_Recv(mpi_info.models_received, tag, mpi_info.MPI_COMM_WORLD)){:};
        % run MCMC for the assigned subset of parameters
        [model, accept_count_sub,LL,intervention] = MCMC_subset_parameters...
            (data,model,MCMC_options,model_no,mpi_info.my_parameters{j},mpi_info.rnk);

        % slaves send their parameters to submaster
        tag = 4*(n*length(mpi_info.my_parameters) + j);
        MPI_Send([model.parameters.value], mpi_info.models_received, tag, mpi_info.MPI_COMM_WORLD);
        MPI_Send(accept_count_sub, mpi_info.models_received, tag+1, mpi_info.MPI_COMM_WORLD);
        MPI_Send(LL, mpi_info.models_received, tag+2, mpi_info.MPI_COMM_WORLD);
        MPI_Send(intervention, mpi_info.models_received, tag+3, mpi_info.MPI_COMM_WORLD);
    end
    
end

end

