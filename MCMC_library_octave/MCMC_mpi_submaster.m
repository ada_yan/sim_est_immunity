% function to calculate estimate of parameters after 
% MCMC_options.iterations_per_swap iterations
% given data, model,
% initial estimate of parameters using MCMC chain at a given temperature

% input arguments:
% data: struct created by model_construction_fn
% model: struct of model for parallel chain for which
% the submaster is responsible,
% each created by model_construction_fn
% MCMC_options: struct created by MCMC_options_fn
% mpi_info: struct: info pertaining to rank of process, total number of processes etc.
% created by allocate_models_to_processes
% model_no: index of this parallel chain
% fid: struct: filenames for writing out

% output arguments:
% model_last: updated struct array after MCMC_options.iterations_per_swap iterations
% likelihood_last: log likelihood at last iteration
% accept_count: vector of length model.parameters: number of accepted proposals
% for each parameter
% need_intervention_flag: integer: passes info to master about
% failures in evaluating log likelihood

function [model_last, likelihood_last, accept_count,need_intervention_flag] =...
    MCMC_mpi_submaster(data,model,MCMC_options,mpi_info,model_no,fid)

% initialise

accept_count = 0;
LL = cell(length(mpi_info.slaves{model_no})+1,1);
likelihood_last = [];
intervention = LL;
need_intervention_flag = 0;

% if more processes than chains
if(mpi_info.siz > length(MCMC_options.temperatures))
    % submaster sends parameters to slaves
    tag = 4*MCMC_options.iterations*length(mpi_info.my_parameters);
    MPI_Send([model.parameters.proposal_width], mpi_info.slaves{model_no},tag,mpi_info.MPI_COMM_WORLD);
    MPI_Send(MCMC_options.temperatures, mpi_info.slaves{model_no},tag+1,mpi_info.MPI_COMM_WORLD);
end

for n = 1:MCMC_options.iterations_per_swap
    for j = 1:length(mpi_info.my_parameters)
        % if more processes than chains
        if(mpi_info.siz > length(MCMC_options.temperatures))
            % submaster sends parameters to slaves
            tag = n*length(mpi_info.my_parameters) + j;
            MPI_Send([model.parameters.value],mpi_info.slaves{model_no},tag,mpi_info.MPI_COMM_WORLD);
        end
        % run MCMC for the assigned subset of parameters
        [model, accept_count_sub,LL{1},intervention{1}] = MCMC_subset_parameters...
            (data,model,MCMC_options,model_no,mpi_info.my_parameters{j},mpi_info.rnk);
        accept_count = accept_count + accept_count_sub;
        
        % if more processes than chains
        if(mpi_info.siz > length(MCMC_options.temperatures))
            % submaster receives parameters from slaves
            tag = 4*(n*length(mpi_info.my_parameters) + j);
            for i = 1:length(mpi_info.slaves{model_no})
                source = mpi_info.slaves{model_no}(i);
                values = MPI_Recv(source, tag, mpi_info.MPI_COMM_WORLD);
                accept_count = accept_count + MPI_Recv(source, tag+1, mpi_info.MPI_COMM_WORLD);
                [model.parameters(mpi_info.slave_parameters{model_no,i}{j}).value] = ...
                    num2cell(values(mpi_info.slave_parameters{model_no,i}{j})){:};
                LL{i+1} = MPI_Recv(source, tag+2, mpi_info.MPI_COMM_WORLD);
                intervention{i+1} = MPI_Recv(source, tag+3, mpi_info.MPI_COMM_WORLD);
            end
        end

        % deal with failures in evaluating log likelihood
        need_intervention = any(~cellfun(@isempty,intervention));
        if(need_intervention)
            % create unique id for intervention at submaster level
            if(need_intervention_flag == 0)
                need_intervention_flag = cell2mat(intervention');
                need_intervention_flag = min(need_intervention_flag);
            end
            model_temp = rm_fnhandle(model);
            mpi_info_temp = mpi_info;
            mpi_info_temp = rmfield(mpi_info_temp,'MPI_COMM_WORLD');
            dir_string = strcat(MCMC_options.out_filename,'intervention/sub/');
            if(~exist(dir_string,'dir'))
                mkdir_retro(dir_string);
            end
            save(sprintf('%s%d_%d_%d_%d.mat',dir_string,need_intervention_flag,mpi_info.rnk,n,j),...
                'model_temp','mpi_info_temp','n','j','LL',...
                'intervention','accept_count','MCMC_options','likelihood_last','-v7');
        end
        if(n == MCMC_options.iterations_per_swap && ~isempty(model.equations.combine_LL{j}))
            likelihood_last = model.equations.combine_LL{j}(LL);
        end
    end
    
    if(model_no == 1)
        % write cold parameters to file
        csvwrite(fid.params,[model.parameters.value],'append','on');
    end
end

% calculate log likelihood at last iteration
model_last = model;
if(isempty(likelihood_last))
    [likelihood_last,~] = model.return_likelihood(model.parameters,data,model,...
        0,[],1);
    likelihood_last = sum(likelihood_last);
end

end

