% sets up data and model, then runs MCMC with parallel tempering

% input arguments:
% previous: empty or string: 
% if we're continuing to run from a previous chain,
% name of .mat file for previous chain
% data_filename: string: file from which to read in data
% parameters_filename: empty or string:
% optionally, details of parameters can be read in from a file and passed
% to model_construction_fn.  Alternatively, they can be coded into
% model_construction_fn.
% dir_base: string. directory in which to store output files
% job_id: empty or string. If non-empty, this string forms part of the output folder name.
% If empty, an identifier is generated based on the system clock.
% shell_filename: path to bash script with which this function was run. 
% For keeping a copy of Matlab files for reproducibility.
% model_construction_fn: function handle to function to construct model and data
% needs to construct the structs model.parameters, model.return_likelihood and data
% MCMC_options_fn: function handle to function to setup MCMC options 
% e.g. the number of iterations.
% mpi: logical: 1 = run in parallel, 0 = run in series

function MCMC_runme_mpi_wrapper(previous,data_filename,parameters_filename,...
    dir_base,job_id,shell_filename,model_construction_fn,MCMC_options_fn,mpi)
% ensures proper scrolling of output in terminal
more off;

% log starting time
fprintf(ctime(time))

% initialise MPI
if(mpi)
    MPI_Init ();
    mpi_info.MPI_COMM_WORLD = MPI_Comm_Load ('NEWORLD');
    mpi_info.rnk   = MPI_Comm_rank (mpi_info.MPI_COMM_WORLD);
    % no. of processes doing MCMC updates
    mpi_info.siz   = MPI_Comm_size (mpi_info.MPI_COMM_WORLD) - 1;
else
    mpi_info.rnk = 0;
end

% load previous chain if applicable
if(~isempty(previous))
    load(previous,'model_temp','MCMC_options');
    MCMC_options_previous = MCMC_options;
else
    model_temp = [];
    MCMC_options_previous = [];
end

% construct model and data
[data,model,MCMC_options] = model_construction_fn(...
    data_filename,parameters_filename,dir_base,job_id,shell_filename,MCMC_options_fn,~mpi_info.rnk);

% running in parallel
if(mpi)
    % decide what each process does.
    % need a master process which coordinates all the parallel chains;
    % submaster processes which are each responsible for one or more chains, 
    % and report to the master process; and
    % slave processes which do the computation within a chain, and
    % report to one submaster process.
    mpi_info = allocate_models_to_processes(mpi_info,length(MCMC_options.temperatures),length(data),model.parameters);
    
    % run MCMC -- different functions depending on the role of this process
    if(~mpi_info.rnk)
        MPI_Send(MCMC_options.out_filename, 1:mpi_info.siz, 1, mpi_info.MPI_COMM_WORLD);
        [MCMC_options,model] = duplicate_model_add_previous(MCMC_options,model,...
            model_temp,1:length(MCMC_options.temperatures),MCMC_options_previous);
        MCMC_PT_mpi_master(data,model,MCMC_options,mpi_info);
    elseif(mpi_info.is_submaster)
        model = repmat(model,length(mpi_info.models_received),1);
        MCMC_options.out_filename = MPI_Recv(0,1,mpi_info.MPI_COMM_WORLD);
        [MCMC_options,model] = duplicate_model_add_previous(MCMC_options,model,...
            model_temp,mpi_info.models_received,MCMC_options_previous);
        MCMC_PT_mpi_submaster(data,model,MCMC_options,mpi_info);
    else
        MCMC_options.out_filename = MPI_Recv(0,1,mpi_info.MPI_COMM_WORLD);
        [MCMC_options,model] = duplicate_model_add_previous(MCMC_options,model,...
            model_temp,mpi_info.models_received,MCMC_options_previous);
        MCMC_PT_mpi_slave(data,model,MCMC_options,mpi_info);
    end
    MPI_Finalize ();
else
    % running in series
    [MCMC_options,model] = duplicate_model_add_previous(MCMC_options,model,...
        model_temp,1:length(MCMC_options.temperatures),MCMC_options_previous);
    MCMC_PT_mpi_master(data,model,MCMC_options,[]);
end
end

% duplicate model according to the number of models each process receives,
% and load old parameters if necessary

% input arguments:
% MCMC_options: struct created by MCMC_options_fn
% model: struct created by model_construction_fn
% model_temp: empty or struct array containing models from previous run
% models_needed: vector of integers: indices of parallel chains the process deals with
% MCMC_options_previous: empty or struct array containing MCMC options from previous run

% output arguments:
% MCMC_options: struct: updated MCMC options
% model: struct array: models for the parallel chains with the correct starting values

function [MCMC_options,model] = duplicate_model_add_previous(MCMC_options,model,...
    model_temp,models_needed,MCMC_options_previous)

% duplicate model for the number of parallel chains required
model = repmat(model,length(models_needed),1);

% sorry, I don't know what this does, but it's not a case used when running this study
if(length(models_needed) == 2)
    [model(1).parameters.value] = num2cell([model_temp(1).parameters.value]){:};
end

% if there is a previous run
if(~isempty(MCMC_options_previous))
    % read temperatures, parameter values, and proposal widths from previous run
    MCMC_options.temperatures = MCMC_options_previous.temperatures;
    for i = 1:length(models_needed)
        [model(i).parameters.value] = num2cell([model_temp(models_needed(i)).parameters.value]){:};
        [model(i).parameters.proposal_width] = ...
        num2cell([model_temp(models_needed(i)).parameters.proposal_width]){:};
    end
else
    % calculate proposal widths
    for i = 1:length(models_needed)
        [model(i).parameters.proposal_width] = ...
        num2cell([model(i).parameters.proposal_width_max]*MCMC_options.proposal_width_weight(models_needed(i))){:};
    end
end
end
