% function to calculate final estimate of parameters given data, model,
% initial estimate of parameters using MCMC chain at a given temperature

% input arguments:
% data: struct created by model_construction_fn
% model_in: struct created by model_construction_fn
% model_no: integer: index of this chain in parallel tempering
% subset_parameters: vector of integers: indices of parameters
% to be sampled over
% rnk: integer: rank of this process in mpi

% output arguments:
% model: updated version of model_in after sampling
% accept_count: vector of integers of length model.parameters:
% the number of accepted proposals for each parameter
% LL_old: integer or vector: log likelihood (or its components) at last iteration
% intervention: logical: indicates whether evaluation of the log likelihood
% failed in a way that matter as some point, requiring manual intervention

function [model, accept_count,LL_old,intervention] = MCMC_subset_parameters...
    (data,model_in,MCMC_options,model_no,subset_parameters,rnk)

more off;
model = model_in;
accept_count = zeros(1,length(model.parameters));
intervention = [];

LL_old = [];
rands = rand(length(subset_parameters),2);
count = 1;
if(~isempty(subset_parameters))
    for j = subset_parameters
        prop_parameters = model.parameters;
        % propose new value for each parameter in group which is to be
        % fitted
        delta = model.parameters(j).proposal_width;
        prop_parameters(j).value = model.parameters(j).value + (2*rands(count,1)-1)*delta;
        % either calculate the parts of the current log likelihood 
        % relevant to the acceptance of this step
        % or retrieve them from an earlier calculation
        [LL_old,~] = model.return_likelihood(model.parameters,data,model,j,LL_old,0);
        % draw random number to determine LL of proposed parameters required to accept
        LL_threshold = log(rands(count,2))*MCMC_options.temperatures(model_no) + sum(LL_old(LL_old ~= Inf));
        % error checking: if the log likelihood of the current parameters
        % is evaluated to be -Inf
        if(LL_threshold == -Inf)
                dir_string = strcat(MCMC_options.out_filename,'accept_inf/'); % fix to depend on MCMC_options.out_filename
                if(~exist(dir_string,'dir'))
                    mkdir_retro(dir_string);
                end
                list = dir(dir_string);
                intervention = [intervention,length(list)-1]; %#ok<AGROW>
                model_temp = rm_fnhandle(model); %#ok<NASGU>
                temperatures = MCMC_options.temperatures; %#ok<NASGU>
                save(strcat(dir_string,sprintf('%d_%d.mat',intervention(end),rnk)),...
                    'data','model_temp','temperatures','model_no','j','rands','-v7');
            error('old LL = -Inf')
        end
        % calculate LL of proposed parameters
        [prop_LL,is_upper_bound] = model.return_likelihood(prop_parameters,data,model,j,LL_old,1);
        % accept/reject
        if(sum(prop_LL(prop_LL < Inf)) >= LL_threshold)
            if(is_upper_bound) % only have upper bound on LL -- tentatively reject but call for manual intervention
                dir_string = strcat(MCMC_options.out_filename,'intervention/subset/');
                if(~exist(dir_string,'dir'))
                    mkdir_retro(dir_string);
                end
                list = dir(dir_string);
                intervention = [intervention,length(list)-1]; %#ok<AGROW>
                model_temp = rm_fnhandle(model); %#ok<NASGU>
                temperatures = MCMC_options.temperatures; %#ok<NASGU>
                % save the parameters where evaluation of the log likelihood failed
                save(strcat(dir_string,sprintf('%d_%d.mat',intervention(end),rnk)),...
                    'data','model_temp','temperatures','model_no','j','rands','-v7');
            end
            model.parameters = prop_parameters;
            accept_count(j) = accept_count(j) + 1;
            LL_old = prop_LL;
        end
        count = count+1;
    end
end

end