% MCMCfileinit - provide filename for the model run
%
% Arguments: data_set <string>: Name of data_set
%            dir_base <string>: directory in which to make results
%            directory
%            job_id <string>: if given, create output folder using this string
%            shell_filename <string>: name of shell script used to run
%            itt_tot <int>: Number of iterations performed
%            copy <logical>: copy source code to results directory
%
% Output: filename_dir <string>: directory used for model run

function filename_dir = MCMCfileinit(data_set,dir_base,job_id,shell_filename,itt_tot,copy)

% create job_id if not given
tmp_clock = clock;
if(isempty(job_id))
    filename_base = strcat(data_set,'-', ...
        num2str(itt_tot),'-',datestr(now,'yyyy-mm-dd'),'-', ...
        num2str(tmp_clock(4)),'-', num2str(tmp_clock(5)));
else
    if(isnumeric(job_id))
        job_id = num2str(job_id);
    end
    filename_base = strcat(data_set,'-', ...
        num2str(itt_tot),'-',job_id);
end

% check if folder already exists to avoid overwriting
d = dir(dir_base);
isub = [d(:).isdir]; %# returns logical vector
nameFolds = {d(isub).name}';
while(ismember(filename_base,nameFolds))
    filename_base = strcat(filename_base,'x');
end
filename_dir = strcat(dir_base,filename_base,'/');

% copy source code if necessary
if(copy)
    code_dir = strcat(filename_dir,'code/');
    mkdir_retro(code_dir);
    copyfile('*.m',code_dir);
    copyfile(shell_filename,code_dir);
    if(isunix)
        copyfile('MCMC_library_octave/*.m',code_dir);
    end
end

end