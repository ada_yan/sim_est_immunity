% assigns parallel chains to processes

% input arguments:
% mpi_info: struct containing rank of process and number of total processes
% n_temperatures: integer: number of parallel chains
% n_data_sets: integer: 1 if mode is not hierarchical, more otherwise
% parameters: model.parameters struct created by model_construction_fn

% output argument:
% mpi_info_out: struct containing everything in mpi_info plus info on which 
% chains and parameters the process is responsible for

function mpi_info_out = allocate_models_to_processes...
    (mpi_info,n_temperatures,n_data_sets,parameters)

mpi_info.is_submaster = (mpi_info.rnk && mpi_info.rnk <= n_temperatures);
% split the chains across processes

% case 1: more chains than processes -- each process gets more than
% one chain, chains don't get subdivided
if(mpi_info.siz <= n_temperatures)
    mpi_info.total_chain_split = mod(1:n_temperatures,mpi_info.siz);
    mpi_info.total_chain_split(mpi_info.total_chain_split == 0) = mpi_info.siz;
    % case 2: more processes than chains -- each process gets one chain,
    % chains get subdivided between its processes later
else
    mpi_info.total_chain_split = split_m_things_across_n_groups(n_temperatures,mpi_info.siz)';
end

% assign one process for each chain to be the submaster
% if process is the submaster, keep track of which processes it is
% the submaster of

% keep track of which chains the process is responsible for
mpi_info.models_received = find(mpi_info.total_chain_split == mpi_info.rnk);
[~,mpi_info.models_received] = ind2sub(size(mpi_info.total_chain_split),mpi_info.models_received);

% more processes than chains: split parameters for each chain between processes
% this is done by assigning a subset of data sets, and their associated parameters, to each process

if(mpi_info.rnk && mpi_info.siz > n_temperatures)
    mpi_info.my_data_sets = find_my_data_sets(mpi_info,n_data_sets,mpi_info.rnk);
    mpi_info.my_parameters = find_parameters(mpi_info.my_data_sets,parameters,mpi_info.is_submaster);
else
    % if more chains than processes, assign all parameters in chain
    % to each process
    mpi_info.my_parameters{1} = find([parameters.fit_option] == 1);
end

% submasters track slaves and which parameters they have
if(mpi_info.is_submaster)
    mpi_info.slaves = cell(n_temperatures,1);
    for i = mpi_info.models_received
        mpi_info.slaves{i} = mpi_info.total_chain_split(2:end,i);
    end
    mpi_info.slave_parameters = cell(n_temperatures,size(mpi_info.total_chain_split,1)-1);
    for i = mpi_info.models_received
        for j = 1:length(mpi_info.slaves{i})
            slave_data_sets = find_my_data_sets(mpi_info,n_data_sets,mpi_info.slaves{i}(j));
            mpi_info.slave_parameters{i,j} = find_parameters(slave_data_sets,parameters,0);
        end
    end
end

% duplicate model for PT
mpi_info.n_models_received = length(mpi_info.models_received);
mpi_info_out = mpi_info;
end

% splits m indices evenly across n groups, dealing with non-exact multiples

function out_array = split_m_things_across_n_groups(m,n)
a = mod(n,m);
a = mod(m-a,m);
% padding with -1 if number of processes isn't an exact multiple of the
% number of chains
split_array = horzcat(1:n,-1*ones(1,a));
out_array = reshape(split_array,m,length(split_array)/m);
end

% only useful for hierarchical models;
% otherwise returns 1

function my_data_sets = find_my_data_sets(mpi_info,n_data_sets,rnk)
% find how many processes are sharing your chain
n_slaves_sharing_chain = sum(mpi_info.total_chain_split(:,mpi_info.models_received) >= 0);
% order these processes
position_in_chain = find(mpi_info.total_chain_split(:,mpi_info.models_received) == rnk);
% allocate data sets to processes
data_split = split_m_things_across_n_groups(n_slaves_sharing_chain,n_data_sets);
if(size(data_split,2) == 1)
    my_data_sets = data_split(position_in_chain);
else
    my_data_sets = data_split(position_in_chain,:);
end
end

% only useful for hierarchical models;
% otherwise return indices of all parameters
% allocate parameters associated with each data set to
% processes allocated that data set
% also identify parameters common to more than one data set
% (will get assigned to submaster)

function my_parameters = find_parameters(my_data_sets,parameters,is_submaster)
my_parameters{2} = [];
my_parameters{1} = [];
for j = 1:length(my_data_sets)
    for i = 1:length(parameters)
        if(parameters(i).fit_option == 1 && ...
                length(parameters(i).data_set_no) == 1 && ...
                any(parameters(i).data_set_no == my_data_sets(j)))
            my_parameters{1}(end+1) = i;
        end
    end
end
if(is_submaster)
    for i = 1:length(parameters)
        if(parameters(i).fit_option == 1 && length(parameters(i).data_set_no) > 1)
            my_parameters{2}(end+1) = i;
        end
    end
end
end