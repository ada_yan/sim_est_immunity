% calibrate width of proposal distribution for each parameter

% input arguments:
% model: struct array of models for n parallel chains
% acceptance_ratio: nxm matrix where m is the number of parameters.
% the proportion of accepted proposals for each parameter.

% output argument:
% model_out: models with updated proposal widths

function model_out = calibrate_proposal_width(model,acceptance_ratio)

model_out = model;
fitted = [model(1).parameters.fit_option];
for i = 1:length(model)
	% find parameters for which the proportion of accepted proposals is too small
    too_small = acceptance_ratio(i,:) < .3;
    % or too large
    too_large = acceptance_ratio(i,:) > .5;
    % scale the proposal widths accordingly
    new_width = [model(i).parameters.proposal_width].*fitted.*...
        (too_small*.75+too_large*1.5+(1-too_small-too_large));
    % make sure they don't exceed the width of the prior distribution
    new_width = min(new_width,[model(i).parameters.proposal_width_max]);
    [model_out(i).parameters.proposal_width] = num2cell(new_width){:};
end
end