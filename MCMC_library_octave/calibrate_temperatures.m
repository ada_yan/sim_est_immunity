% calibrate temperatures for parallel chains

% input arguments:
% temperatures: vector of length n: current temperatures of chains
% swap_ratio: vector of length n - 1: (proportion of accepted swaps
% out of proposed swaps)/2
% the factor of 2 arises because of the way the swaps are recorded, and how
% we alternate between swapping 1<->2, 3<->4.... and 2<->3, 4<->5...

% output argument:
% temperatures_out: vector of length n: new temperatures of chains
function temperatures_out = calibrate_temperatures(temperatures,swap_ratio)

diff_temp = diff(temperatures);
% find chains between which the swap ratio is too large
too_large = swap_ratio > .2; % note factor of 2 from main text -- see above
% find chains between which the swap ratio is too small
too_small = swap_ratio < .05;
% adjust differences between temperatures accordingly
diff_temp = diff_temp.*(too_large*1.5 + too_small*.75 + (1-too_large-too_small));
% reconstruct temperatures from their differences
temperatures_out = cumsum([temperatures(1),diff_temp]);

end
