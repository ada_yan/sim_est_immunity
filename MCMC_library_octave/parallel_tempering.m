% conduct parallel tempering

% input arguments:
% model_in: struct array of n models at different temperatures, 
% each created by model_construction_fn
% likelihood_last_in: vector of length n: log 
% likelihood of the last iteration of each model
% temperatures: vector of length n: temperatures of chains
% offset_in: 0 or 1: whether to swap chains 1 <->2, 3<-> 4... 
% or 2<->3, 4<->5...
% swaps_in: vector of length n-1: number of parallel tempering swaps
% conducted previously

% output arguments:
% model_out: swapped models after parallel tempering, of same form as model_in
% likelihood_last_out: vector of length n: 
% likelihoods corresponding to swapped models
% offset_out : 1 - offset_in
% swaps_out: swaps_in + number of parallel tempering swaps
% conducted in this round

function [model_out,likelihood_last_out,offset_out,swaps_out] = ...
  parallel_tempering(model_in,likelihood_last_in,temperatures,offset_in,swaps_in)

model_out = model_in;
likelihood_last_out = likelihood_last_in;
swaps_out = swaps_in;

i = offset_in + 1;
while i+1 <= length(temperatures)
    j = i+1;
    % delta = log(probability of swapping model states i and j)
    delta = (1/temperatures(j)-1/temperatures(i))*...
    (likelihood_last_in(i)-likelihood_last_in(j));
    % perform swap of model states with probability exp(delta)
    if rand <= exp(delta)              
%             swap parameter values between temperatures
      i_params = [model_in(i).parameters.value];
      j_params = [model_in(j).parameters.value];
      i_params = num2cell(i_params);
      j_params = num2cell(j_params);
      [model_out(i).parameters.value] = j_params{:};
      [model_out(j).parameters.value] = i_params{:};

      likelihood_last_out(i) = likelihood_last_in(j);
      likelihood_last_out(j) = likelihood_last_in(i);
      % tally number of actual swaps conducted
      swaps_out(i) = swaps_out(i) + 1;
    end
    i=i+2;
end
offset_out=1-offset_in;

end