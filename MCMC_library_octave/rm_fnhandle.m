% remove all function handles from a struct for the purposes of saving to mat file

% input argument: 
% model_in: struct

% output argument:
% model_out: same struct with function handles removed

function model_out = rm_fnhandle(model_in)

names = fieldnames(model_in(1));
for i = 1:length(model_in)
    for j = 1:length(names)
        if(strcmp(names{j},'equations') || strcmp(names{j},'return_likelihood') ||...
                strcmp(names{j},'generate_random_parameters'))
            continue;
        end
        model_out(i).(names{j}) = model_in(i).(names{j});
    end
end
end