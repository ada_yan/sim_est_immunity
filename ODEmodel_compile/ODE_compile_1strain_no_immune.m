clear mStruct
mStruct.s.x1 = 1;
mStruct.s.x2 = 2;
mStruct.s.x3 = 3;
mStruct.s.x4 = 4;
mStruct.p.k1 = 1;
mStruct.p.k2 = 2;
mStruct.p.k3 = 3;
mStruct.p.k4 = 4;
mStruct.p.k5 = 5;
mStruct.p.k6 = 6;
mStruct.p.k7 = 7;
mStruct.p.k8 = 8;
mStruct.p.k9 = 9;
mStruct.p.k10 = 10;
mStruct.p.k11 = 11;
mStruct.p.k12 = 12;
mStruct.p.k13 = 13;
mStruct.p.k14 = 14;
mStruct.p.k15 = 15;
mStruct.p.k16 = 16;
mStruct.p.k17 = 17;
mStruct.p.k18 = 18;
mStruct.p.k19 = 19;
mStruct.p.k20 = 20;
mStruct.p.k21 = 21;
mStruct.p.k22 = 22;
mStruct.p.k23 = 23;
mStruct.p.k24 = 24;
mStruct.p.k25 = 25;
mStruct.p.k26 = 26;
mStruct.p.k27 = 27;
mStruct.p.k28 = 28;
mStruct.p.k29 = 29;
convertToC(mStruct,'ODEmodel_mex/ODE_1strain_no_immune.m');
dxdt_filename = 'general_functions/ODEMEXv12/Parser/outputC/model/dxdt.c';
if(ismac)
    extra = '''''';
else
    extra = '';
end
system(horzcat('sed -i',extra,' -e ''s/\(\.\.\.\)//g'' ',dxdt_filename));
system(horzcat('sed -i',extra,' -e ''s/\(F , F , \)/F , /g'' ',dxdt_filename));
system(horzcat('sed -i',extra,' -e ''s/\(F1 , F1 , \)/F1 , /g'' ',dxdt_filename));
system(horzcat('sed -i',extra,' -e ''s/\(F2 , F2 , \)/F2 , /g'' ',dxdt_filename));
system(horzcat('sed -i',extra,' -e ''s/\(sum_F , sum_F , \)/sum_F , /g'' ',dxdt_filename));
system(horzcat('sed -i',extra,' -e ''s/\(deltaI1 , deltaI1 , \)/deltaI1 , /g'' ',dxdt_filename));
% compileC('ODEmodel_1strain_no_immune_c');
