function xdot = ODE_1strain_knockout_E(~,x,p,~)
% type N parameters: don't vary with ferrets
nB = 5;
% type F parameters: vary between ferrets
g = p(1);
deltaF = p(2);
phi_times_mean_pF = p(3);
T_0 = p(4);
rho = p(5);
alpha1 = p(6);
% type V parameters: vary between ferrets and viruses
% viral hyperparameters
beta1 = p(7);
pV1 = p(8);
deltaI1 = p(9);
deltaV_minus_deltaVR1 = p(10);
deltaVR1 = p(11);
% innate immune response hyperparameters specific to viruses
pF_on_mean_pF1 = p(12);
kappaF_times_mean_pF1 = p(13);
s_times_mean_pF1 = p(14);
% humoral immune response hyperparameters specific to viruses
pA_times_kappaA_times_B_01 = p(15);
deltaA1 = p(16);
deltaB1 = p(17);
kB1 = p(18);
betaB1 = p(19);
tauB1 = p(20);
% unit conversion hyperparameters specific to viruses
pR1 = p(21);
% compartment numbering
T = maximum(x(1),0);
R = maximum(x(2),0);
I1 = maximum(x(3),0);
V1 = maximum(x(4),0);
F = maximum(x(5),0);
B11 = maximum(x(6),0);
B21 = maximum(x(7),0);
B31 = maximum(x(8),0);
B41 = maximum(x(9),0);
B51 = maximum(x(10),0);
B61 = maximum(x(11),0);
P1 = maximum(x(12),0);
A1 = maximum(x(13),0);
VR1 = maximum(x(14),0);
xdot = zeros(14,1);
xdot(1) = g*(T_0 - T - R - I1 )/T_0*(T + R) - (beta1*V1 + phi_times_mean_pF*F)*T + rho*R;
xdot(2) = phi_times_mean_pF*F*T - rho*R;
xdot(3) = beta1*V1*T - (deltaI1+kappaF_times_mean_pF1*F...
)*I1;
xdot(4) = pV1/(1+s_times_mean_pF1*F)*I1 - (deltaV_minus_deltaVR1+deltaVR1+pA_times_kappaA_times_B_01*A1+beta1*T)*V1;
xdot(5) = pF_on_mean_pF1*I1 + - deltaF*F;
xdot(6) = -VR1/(kB1+VR1)*betaB1*B11;
xdot(7) = -xdot(6) - B21/tauB1*nB - deltaB1*B21;
xdot(8) = (2*B21-B31)/tauB1*nB - deltaB1*B31;
xdot(9) = (2*B31-B41)/tauB1*nB - deltaB1*B41;
xdot(10) = (2*B41-B51)/tauB1*nB - deltaB1*B51;
xdot(11) = (2*B51-B61)/tauB1*nB - deltaB1*B61;
xdot(12) = 2*B61/tauB1*nB-deltaB1*P1;
xdot(13) = P1 - deltaA1*A1;
xdot(14) = pV1*pR1*alpha1/(1+s_times_mean_pF1*F)*I1 - deltaVR1*VR1 - alpha1*beta1*T*V1;
