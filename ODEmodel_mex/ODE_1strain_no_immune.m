function xdot = ODE_1strain_no_immune(~,x,p,~)
% type N parameters: don't vary with ferrets
% type F parameters: vary between ferrets
g = p(1);
deltaF = p(2);
phi_times_mean_pF = p(3);
T_0 = p(4);
rho = p(5);
alpha1 = p(6);
% type V parameters: vary between ferrets and viruses
% viral hyperparameters
beta1 = p(7);
pV1 = p(8);
deltaI1 = p(9);
deltaV_minus_deltaVR1 = p(10);
deltaVR1 = p(11);
% innate immune response hyperparameters specific to viruses
pF_on_mean_pF1 = p(12);
kappaF_times_mean_pF1 = p(13);
s_times_mean_pF1 = p(14);
% humoral immune response hyperparameters specific to viruses
pA_times_kappaA_times_B_01 = p(15);
deltaA1 = p(16);
deltaB1 = p(17);
kB1 = p(18);
betaB1 = p(19);
tauB1 = p(20);
% unit conversion hyperparameters specific to viruses
pR1 = p(21);
% type T parameters: vary between ferrets and T cell pools
betaC1 = p(22);
deltaE1 = p(23);
tauM1 = p(24);
tauE1 = p(25);
eps1 = p(26);
kC_on_a_times_d11 = p(27);
kC_on_a_times_d12 = p(28);
C_0_times_kappaE_times_a_times_d11 = p(29);
% compartment numbering
T = maximum(x(1),0);
I1 = maximum(x(2),0);
V1 = maximum(x(3),0);
VR1 = maximum(x(4),0);
xdot = zeros(4,1);
xdot(1) = g*(T_0 - T - I1 )/T_0*(T) - (beta1*V1 + 0)*T;
xdot(2) = beta1*V1*T - deltaI1*I1;
xdot(3) = pV1*I1 - (deltaV_minus_deltaVR1+deltaVR1+beta1*T)*V1;
xdot(4) = pV1*pR1*alpha1*I1 - deltaVR1*VR1 - alpha1*beta1*T*V1;
