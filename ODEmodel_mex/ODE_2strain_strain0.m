function xdot = ODE_2strain_strain0(~,x,p,~)
deltaVR1 = p(11);
VR1 = maximum(x(1),0);
VR2 = maximum(x(2),0);
xdot = zeros(2,1);
xdot(1) = - deltaVR1*VR1;
xdot(2) = - deltaVR1*VR2;
