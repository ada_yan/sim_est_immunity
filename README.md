# Repository for `Quantifying innate and adaptive immunity during influenza infection using sequential infection experiments and mathematical models'

## Overview

* [compile_mex.sh](https://bitbucket.org/ada_yan/sim_est_immunity/src/master/compile_mex.sh) precompiles the ODEs to speed up solving (optional).
* [print_data.sh](https://bitbucket.org/ada_yan/sim_est_immunity/src/master/print_data.sh) generates the synthetic data.
The generated data is also provided in the input folder.
* [run.sh](https://bitbucket.org/ada_yan/sim_est_immunity/src/master/run.sh) provides example code to fit a model to the data.
* [calc_diagnostics.R](https://bitbucket.org/ada_yan/sim_est_immunity/src/master/calc_diagnostics.R) calculates convergence diagnostics for the MCMC chains.
* [postprocessing.m](https://bitbucket.org/ada_yan/sim_est_immunity/src/master/postprocessing.m) generates predictions from the fitted models and the true parameters.
* [plot_sim_est_paper.m](https://bitbucket.org/ada_yan/sim_est_immunity/src/master/plot_sim_est_paper.m) plots the figures.

## Installing

### Software

* [compile_mex.sh](https://bitbucket.org/ada_yan/sim_est_immunity/src/master/compile_mex.sh) requires Octave and Matlab.
* [print_data.sh](https://bitbucket.org/ada_yan/sim_est_immunity/src/master/print_data.sh) requires MATLAB and Octave.
* [run.sh](https://bitbucket.org/ada_yan/sim_est_immunity/src/master/run.sh) requires Octave (and MATLAB if using the CVODEs wrapper to speed up ODE solving).
* [calc_diagnostics.R](https://bitbucket.org/ada_yan/sim_est_immunity/src/master/calc_diagnostics.R) requires R.
* [postprocessing.m](https://bitbucket.org/ada_yan/sim_est_immunity/src/master/postprocessing.m) requires Octave.
* [plot_sim_est_paper.m](https://bitbucket.org/ada_yan/sim_est_immunity/src/master/plot_sim_est_paper.m) requires MATLAB.

I (Ada Yan) used MATLAB 2015b, Octave 3.6.2 and R 3.4.0.

## Packages

* Octave: 
    * [run.sh](https://bitbucket.org/ada_yan/sim_est_immunity/src/master/run.sh) requires Octave package [io](https://octave.sourceforge.io/io/)
    * To run the chains in parallel, either [mpi](https://octave.sourceforge.io/mpi/) or [openmpi_ext](https://packages.debian.org/wheezy/octave-openmpi-ext) is required.  MPI must be installed on your system.  However, the code supports running the chains in series without MPI (see [run.sh](https://bitbucket.org/ada_yan/sim_est_immunity/src/master/run.sh)).
    * To speed up ODE solving, the [ODEMEX wrapper](https://bitbucket.org/JoepVanlier/odemex/overview) is required.  I put the `ODEMEXv12` folder in the [general_functions](https://bitbucket.org/ada_yan/sim_est_immunity/src/master/general_functions/) folder.  Please follow the instructions provided by the wrapper documentation for installation.
In addition, I have provided some files in the [ODEMEXv12_ed](https://bitbucket.org/ada_yan/sim_est_immunity/src/master/general_functions/ODEMEXv12_ed/) folder.
After installation of the ODEMEX wrapper, please add the files in the [ODEMEXv12_ed](https://bitbucket.org/ada_yan/sim_est_immunity/src/master/general_functions/ODEMEXv12_ed/) folder to the `ODEMEXv12` folder (overwriting existing files with the same names).
The code also supports solving the ODEs without precompiling (see [run.sh](https://bitbucket.org/ada_yan/sim_est_immunity/src/master/run.sh)). Solving without precompiling is so slow that the problem basically becomes intractable, but it's useful for cross-checking (or if you can't get the wrapper working).
* R:
    * [calc_diagnostics.R](https://bitbucket.org/ada_yan/sim_est_immunity/src/master/calc_diagnostics.R) requires R packages [coda](https://cran.r-project.org/package=coda), [dplyr](https://CRAN.R-project.org/package=dplyr) and [xtable](https://CRAN.R-project.org/package=xtable).

## Authors

Ada W. C. Yan wrote the code.  Ada W. C. Yan, Sophie G. Zaloumis, Julie A. Simpson and James M. McCaw wrote the paper.