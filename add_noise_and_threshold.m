function observables_array = add_noise_and_threshold(observables, sigma, seed)

if(~IsOctave && ~isempty(seed))
    rng(seed);
end

names = fieldnames(observables);
threshold = 10; % measurement threshold

observables_array = zeros(length(observables.(names{1})),length(names));

for j = 1:length(names)
    for k = 1:length(observables.(names{j}))
        if(observables.(names{j})(k) > 0) % -1 denotes no measurement -- skip these values
            % add measurement noise
            observables.(names{j})(k) = log10nrnd(log10(observables.(names{j})(k)),sigma);
            % impose observation threshold
            if(observables.(names{j})(k) < threshold)
                observables.(names{j})(k) = 0;
            end
        end
    end
    observables_array(:,j) = observables.(names{j});
end

end