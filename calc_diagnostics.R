#' see whether a set of parallel MCMC chains have converged;
#' if so, calculate effective sample size and burn-in
#' 
#' @param filenames character vector of filenames, each corresponding to one csv
#' file containing samples from an MCMC chain
#' @param fixed_xreact: logical vector of length 1: TRUE iff the parameters
#' $log_{10}k_{C11}$,$log_{10}k_{C12}$ are fixed
#' @return if convergence has occurred, return a list with the elements
#' burn_in: the number of burn-in iterations
#' n_samples: the total number of samples (not effective samples) across chains after burn-in
#' psrf: a data frame containing the potential scale reduction factor
#' and effective sample size for each parameter
#' if convergence has not occurred, return NULL

library(coda)
library(dplyr)
library(xtable)

calc_diagnostics <- function(filenames, fixed_xreact){
    data <- lapply(filenames,function(x)read.csv(x,header = FALSE))
    data_dir <- dirname(filenames[1])
    if(fixed_xreact){
        fixed_params <- c(12,29,30)
    }else{
        fixed_params <- 12
    }
    # discard parameters which are fixed
    data <- lapply(data,function(x) x[,-fixed_params])
    # determine length of shortest chain
    min_length <- min(sapply(data,function(x)dim(x)[1]))
    # if prsf for all paramters below threshold, converged
    thres = 1.1
    # calculate potential scale reduction factor
    # note: discards first half of chain as default
    for (k in seq(1e4,min_length,1e4)){
        data_temp <- lapply(data,function(x)x[1:k,])
        data_temp <- lapply(data_temp,mcmc)
        combinedchains <- mcmc.list(data_temp)
        psrf <- gelman.diag(combinedchains)
        # if the largest of the 97.5% percentile of the PSRFs across
        # parameters is less than the threshold,
        # declare convergence
        message(max(psrf[[1]][,2]))
        if(max(psrf[[1]][,2]) < thres){
            burn_in <- ceiling(k/2)
            write.csv(burn_in, file = paste0(data_dir, "/burn_in.csv"))
            n_samples <- (min_length-ceiling(k/2))*length(filenames)
            # keep second half of converged chain, plus all samples afterwards
            data <- lapply(data,function(x)x[(ceiling(k/2)+1):min_length,])
            data <- lapply(data,mcmc)
            combinedchains <- mcmc.list(data)
            # calculate effective sample size
            size_combined <- effectiveSize(combinedchains)
            # print latex table
            a <- as.data.frame(psrf[[1]])
            size_combined <- as.data.frame(size_combined)
            cat_data_frame <- bind_cols(a,size_combined)
            colnames(cat_data_frame)[3] <- "Effective size"
            names <- c("$\\log_{10}g$","$\\log_{10}\\delta_F$","$\\log_{10}\\bar{\\phi}$",
                       "$\\log_{10}T_0$","$\\log_{10}\\rho$","$\\log_{10}\\alpha$",
                       "$\\log_{10}R_0$","$\\log_{10}r$","$\\log_{10}\\delta_I$",
                       "$\\log_{10}(\\delta_V - \\delta_{VR})$","$\\log_{10}\\delta_{VR}$","dummy",
                       "$\\log_{10}\\bar{\\kappa}_F$","$\\log_{10}\\bar{s}$","$\\log_{10}\\bar{\\kappa}_A$",
                       "$\\log_{10}\\delta_A$","$\\log_{10}\\delta_B$","$\\log_{10}k_B$","$\\log_{10}\\beta_B$",
                       "$\\log_{10}\\tau_B$","$\\log_{10}p_R$","$\\log_{10}\\gamma_0$","$\\log_{10}V_0$",
                       "$\\log_{10}\\beta_C$","$\\log_{10}\\delta_E$","$\\log_{10}\\tau_M$",
                       "$\\log_{10}\\tau_E$","$\\log_{10}\\epsilon$",
                       "$\\log_{10}k_{C11}$","$\\log_{10}k_{C12}$","$\\bar{\\kappa}_{E11}$","$\\sigma$")
            names <- names[-fixed_params]
            row.names(cat_data_frame) <- names
            print(xtable(cat_data_frame),sanitize.text.function = identity, file = paste0(data_dir, "/psrf.txt"))
            output <- list(burn_in = burn_in, n_samples = n_samples, psrf = cat_data_frame)
            saveRDS(output,paste0(data_dir, "/output.rds"))
            return(output)
        }
    }
    invisible(NULL)
}