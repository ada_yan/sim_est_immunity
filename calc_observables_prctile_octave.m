% given samples from an MCMC chain, calculate trajectories and percentiles
% for compartments in ODE
% input arguments:
% dir_strings: a cell array of size 1xn where n is the number of parallel chains
% burn_in: a scalar integer: number of iterations to discard as burn-in
% samples_per_chain: either empty or an integer. If an integer, when making predictions,
% thin chains to this many samples by sampling uniformly across the iterations.
% comps: 0 or a vector of integers. If 0, this function returns the total viral load.
% If non-zero, indicates the indices of the compartments which should be returned
% by calc_observables_prctile_octave out of the ones returned by return_all_compartments
% calc_true: logical. Use 1 if the first sample is the true parameters; then
% calc_observables_prctile_octave only calculates the compartments for the first sample.
% model: string e.g. 'baseline', 'XIT' or 'knockout_adaptive' (see solve_ode.m for all options)
% intervals: empty or a vector of integers. If empty, this function returns the percentiles and trajectories
% for a single infection. If non-empty, this function returns the percentiles and trajectories
% for a single infection and for the inter-exposure intervals specified.
% mex_flag: logical. 1 iff solving ODEs using precompiled files.

function calc_observables_prctile_octave(dir_strings,burn_in,samples_per_chain,...
    comps,calc_true,model,intervals,mex_flag)

more off

prc = [2.5,25,75,97.5]; % % the percentiles to calculate.  Actually only plot 2.5 and 97.5
combined_observables = [];

for i = 1:length(dir_strings)
    load(strcat(dir_strings{i},'parameters.csv'));
    % load template for parameter struct
    load(strcat(dir_strings{i},'master.mat'),'model_temp'); 
    % if thinning, determine indices of samples to use when calculating
    % percentiles
    if(~isempty(samples_per_chain))
        ind = round(linspace(burn_in+1,size(parameters,1),samples_per_chain));
    else
        ind = burn_in+1:size(parameters,1);
    end
    % thin
    parameters = parameters(ind,:);
    p = model_temp(1).parameters;
    if(calc_true)
        parameters = parameters(1,:);
    end
    
    % calculate observables for one set of parameters to determine
    % dimensions
    observables = return_all_compartments(p,parameters(1,:),model,intervals,mex_flag);
    
    % initialise array to store compartments to keep
    if(comps ~= 0)
        n_comps = length(comps);
    elseif(isempty(intervals))
        n_comps = 1;
    else
        n_comps = 2;
    end
    if(ndims(observables) == 2) %#ok<*ISMAT>
        observables = zeros(size(parameters,1),size(observables,1),n_comps);
    else
        observables = zeros(size(parameters,1),size(observables,1),...
            size(observables,2),n_comps);
    end
    
    % calculate observables for the parameter sets
    for j = 1:size(parameters,1)
        if(mod(j,10) == 0)
            fprintf('%d %d\n',i,j)
        end
        temp = return_all_compartments(p,parameters(j,:),model,intervals,mex_flag);
        if(ndims(observables) == 2)
            if(comps == 0)
                observables(j,:,:,:)  = temp(:,end+1-n_comps:end);
            else
                observables(j,:,:,:)  = temp(:,comps);
            end
        else
            if(comps == 0)
                observables(j,:,:,:)  = temp(:,:,end+1-n_comps:end);
            else
                observables(j,:,:,:)  = temp(:,:,comps);
            end
        end
    end
    
    % concatenate the observables across the chains
    combined_observables = [combined_observables;observables];
    if(calc_true)
        if(ndims(observables) == 2)
            observables_true = squeeze(observables(1,:,:));
        else
            observables_true = squeeze(observables(1,:,:,:));
        end
        if(comps == 0)
            save(strcat(dir_strings{i},model,'VR_true.mat'),'observables_true','-v7')
        else
            save(strcat(dir_strings{i},model,'all_comps_true.mat'),'observables_true','-v7')
        end
        return
    end
end

observables_prctile = prctile_octave(combined_observables,prc);
if(comps == 0)
    save(strcat(dir_strings{1},model,'VR_prctile.mat'),'observables_prctile','-v7')
else
    save(strcat(dir_strings{1},model,'all_comps_prctile.mat'),'observables_prctile','-v7')
end

% save up to 100 trajectories only
if(size(combined_observables,1) > 100)
    save_ind = round(linspace(1,size(combined_observables,1),100));
    if(ndims(observables) == 2)
        combined_observables = combined_observables(save_ind,:,:);
    else
        combined_observables = combined_observables(save_ind,:,:,:);
    end
end
observables = combined_observables;
if(comps == 0)
    save(strcat(dir_strings{1},model,'VR_trajectories.mat'),'observables','-v7')
else
    save(strcat(dir_strings{1},model,'all_comps_trajectories.mat'),'observables','-v7')
end

end