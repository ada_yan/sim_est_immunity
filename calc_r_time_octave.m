% given samples from an MCMC chain, calculate r_2(t) (percentiles and sampled trajectories)
% for the baseline model, and models XC and XIT
% input arguments:
% dir_strings: a cell array of size 1xn where n is the number of parallel chains
% burn_in: a scalar integer: number of iterations to discard as burn-in
% samples_per_chain: either empty or an integer. If an integer, when making predictions,
% thin chains to this many samples by sampling uniformly across the iterations.
% calc_true: logical. Use 1 if the first sample is the true parameters; then
% mex_flag: logical. 1 iff solving ODEs using precompiled files.

function calc_r_time_octave(dir_strings,burn_in,samples_per_chain,...
    calc_true,mex_flag)

more off
prc = [2.5,50,97.5]; % the percentiles to calculate.  Actually only plot 2.5 and 97.5
max_n_parameters = 100;
max_IEI = 20;
days = 0:30;

for i = 1:length(dir_strings)
    if(calc_true && i > 1)
        continue;
    end
    load(strcat(dir_strings{i},'parameters.csv'));
    % load template for parameter struct
    load(strcat(dir_strings{i},'master.mat'),'model_temp');
    % if thinning, determine indices of samples to use when calculating
    % percentiles
    if(~isempty(samples_per_chain))
        ind = round(linspace(burn_in+1,size(parameters,1),samples_per_chain));
    else
        ind = burn_in+1:size(parameters,1);
    end
    parameters = parameters(ind,:);
    p = model_temp(1).parameters;
    pnames = {model_temp(1).parameters.pname};
    c = compartment_labelling(2,'baseline');
    if(calc_true)
        parameters = parameters(1,:);
    end
    
    % calculate observables for max_n_parameters at a time to minimise
    % memory requirements
    parameters_now = 1:min(max_n_parameters,size(parameters,1));
    while(~isempty(parameters_now))
        observables = return_all_compartments(p,parameters(1,:),'baseline',[],mex_flag);
        observables = zeros(length(parameters_now),size(observables,2),size(observables,3));
        for j = 1:length(parameters_now)
            if(mod(parameters_now(j),100) == 0)
                fprintf('%d %d\n',i,parameters_now(j))
            end
            temp = return_all_compartments(p,parameters(parameters_now(j),:),'baseline',[],mex_flag);
            observables(j,:,:)  = squeeze(temp(1,:,:));
        end
        
        % calculate transformed parameters
        transformed_p = retrieve_parameters(parameters(parameters_now,:),pnames);
        % and use these in conjunction with the observables to calculate
        % r_2(t)
        r_time_temp = retrieve_terms(observables,c,days+max_IEI+1,transformed_p);
        
        % concatenate to previous results
        clear observables
        names = fieldnames(r_time_temp);
        if(~exist('r_time','var'))
            r_time = r_time_temp;
        else
            for k = 1:length(names)
                r_time.(names{k}) = [r_time.(names{k});r_time_temp.(names{k})];
            end
        end
        
        parameters_now = parameters_now(end)+1:min(parameters_now(end)+max_n_parameters,size(parameters,1));
    end
end

if(calc_true)
    for i = 1:length(names)
        r_time_true.(names{i}) = squeeze(r_time.(names{i}));
    end
    save(strcat(dir_strings{1},'r_time_true.mat'),'r_time_true','-v7');
else
    % calculate percentiles
    for i = 1:length(names)
        r_time_prctile.(names{i}) = prctile_octave(r_time.(names{i}),prc);
    end
    % save up to 100 trajectories
    if(size(r_time.(names{1}),1) > 100)
        save_ind = round(linspace(1,size(r_time.(names{1}),1),100));
        for i = 1:length(names)
            r_time.(names{i}) = r_time.(names{i})(save_ind,:);
        end
    end
    save(strcat(dir_strings{1},'r_time_prctile.mat'),'r_time_prctile','-v7')
    save(strcat(dir_strings{1},'r_time_trajectories.mat'),'r_time','-v7');
end
end

% transforms parameters for calculating r_2(t)

% input arguments:
% parameters: matrix whoere each row is a parameter set (sample from chain)
% pnames: cell array containing parameter names as strings

% output argument:
% p: struct with parameter values needed to calculate r_2(t)
function p = retrieve_parameters(parameters,pnames)

% unlog everything
for i = 1:length(pnames)
    p.(pnames{i}) = 10.^parameters(:,i);
end

p.deltaV = (10.^parameters(:,strcmp(pnames,'deltaV_minus_deltaVR')))+...
    (10.^parameters(:,strcmp(pnames,'deltaVR')));
p.C_0_times_kappaE_times_a_times_d12 = p.C_0_times_kappaE_times_a_times_d11.*...
    p.kC_on_a_times_d11./p.kC_on_a_times_d12;
p.beta1_T_0 = (-p.r.^2-p.deltaI.*p.r)./(p.r - p.deltaI.*(p.R_0 - 1)) - p.deltaV;
p.pV = p.R_0.*p.deltaI.*(p.beta1_T_0 + p.deltaV)./p.beta1_T_0;
p.beta1 = p.beta1_T_0./p.T_0;
end

% calculate r_2(t) given a set of parameters and observables

% input arguments:
% observables: mxnxq array where m is the number of samples, n is the
% number of timepoints over which the ODEs are solved, and q is the number
% of compartments
% c: struct of compartment labels created by compartment_labelling
% days: vector of integers: indices of th timepoints for which we're
% calculating r_2(t)
% p: struct outputted by retrieve_parameters

% output argument:
% r_time: struct with fields baseline, XC and XIT.
% Each field is a matrix, with each row corresponding to r_2(t) for a
% single sample.
function r_time = retrieve_terms(observables,c,days,p)

terms.F = squeeze(observables(:,days,c.F));
terms.effect_F2 = bsxfun(@times,p.s_times_mean_pF,terms.F);
terms.effect_F3 = bsxfun(@times,p.kappaF_times_mean_pF,terms.F);
terms.pV_scaled = bsxfun(@rdivide,p.pV,1+terms.effect_F2);
terms.T = squeeze(observables(:,days,c.T));
terms.beta1_T = bsxfun(@times,p.beta1,terms.T);

terms.E = squeeze(sum(observables(:,days,c.E(:,3)),3));

terms.effect_E = bsxfun(@times,p.C_0_times_kappaE_times_a_times_d12,terms.E);
terms.I_loss = (bsxfun(@plus,p.deltaI,terms.effect_F3) + terms.effect_E);
terms.V_loss = bsxfun(@plus,terms.beta1_T,p.deltaV);

if(size(terms.beta1_T,ndims(terms.beta1_T)) == 1) % if calculating for one parameter set only,
    % some vectors strangely get transposed
    terms.beta1_T = terms.beta1_T';
    terms.V_loss = terms.V_loss';
end

terms.R_e = terms.beta1_T.*terms.pV_scaled./terms.I_loss./terms.V_loss;
r_time.baseline = -(terms.V_loss+terms.I_loss)/2+...
    sqrt((terms.I_loss-(terms.V_loss)).^2+4*terms.beta1_T.*terms.pV_scaled)/2;

terms.I_loss_XC = bsxfun(@plus,p.deltaI,terms.effect_E);
terms.beta1_T_XC = p.beta1.*p.T_0;
terms.V_loss_XC = p.beta1.*p.T_0 + p.deltaV;

if(size(terms.I_loss_XC,ndims(terms.I_loss_XC)) == 1)
    terms.I_loss_XC = terms.I_loss_XC';
end

r_time.XC = -bsxfun(@plus,terms.V_loss_XC,terms.I_loss_XC)/2+...
    sqrt(bsxfun(@plus,bsxfun(@minus,terms.I_loss_XC,terms.V_loss_XC).^2,...
    4*terms.beta1_T_XC.*p.pV))/2;

terms.I_loss_XIT = bsxfun(@plus,p.deltaI,terms.effect_F3);
r_time.XIT = -(terms.V_loss+terms.I_loss_XIT)/2+...
    sqrt((terms.I_loss_XIT-(terms.V_loss)).^2+4*terms.beta1_T.*terms.pV_scaled)/2;

r_time.XI = -bsxfun(@plus,terms.V_loss_XC,terms.I_loss_XIT)/2+...
    sqrt(bsxfun(@minus,terms.I_loss_XIT,terms.V_loss_XC).^2+4*bsxfun(@times,terms.beta1_T_XC,terms.pV_scaled))/2;

end
