
% calculate the viral load when the components of the immune response
% mediating cross-protection are restricted

% input arguments:
% dir_string_true: string: folder containing MCMC chain starting at true parameter value
% dir_strings: 1x3 cell array: 1x3 cell array containing folder names for parallel chains for fitted model
% burn_in: scalar integer: number of iterations to discard as burn-in
% mex_flag: logical. 1 iff solving ODEs using precompiled files.

function calc_viral_load_separate_func(dir_string_true,dir_strings,burn_in,mex_flag, calc_true)
intervals = [1,7,14];
if(isempty(strfind(dir_strings{1},'ferret')))
    args = {'XC','XIT','XI','XT'};
else
    args = {'XC','XIT','XI','XT','XI1','XI2','XI3'};
end

for i = 1:length(args)
    if(calc_true)
        calc_observables_prctile_octave({dir_string_true},0,'',...
            0,1,args{i},intervals,mex_flag)
    end
    if(~isempty(dir_strings))
        calc_observables_prctile_octave(dir_strings,burn_in,1e4,...
            0,0,args{i},intervals,mex_flag)
    end
end
end