% this function exists for historical reasons when there were multiple parts to the likelihood.
% now, it returns the input.
function LL = combine_LL(LL_in,~,~,~)
LL = sum(LL_in{1});
end