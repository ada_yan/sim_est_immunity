function combine_csv(filenames_in,filename_out)
csv_out = [];

% the csvs are such that the last row of file i is the same as the first
% row of file i+1, so ignore first row if i>1
for i = 1:length(filenames_in)
    temp = csvread(filenames_in{i});
    if(i == 1)
        csv_out = [csv_out;temp];
    else
        csv_out = [csv_out;temp(2:end,:)];
    end        
end
temp = strfind(filename_out,'/');
dir_string = filename_out(1:temp(end));
if(~exist(dir_string,'dir'))
    mkdir(dir_string);
end
csvwrite(filename_out,csv_out);
end