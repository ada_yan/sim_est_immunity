filenames_in = {'results/zarnitsyna_data_sequential_noise1-100000-1_full_new/parameters.csv',...
'results/zarnitsyna_data_sequential_noise1-100000-1_full_c/parameters.csv',...
'results/zarnitsyna_data_sequential_noise1-100000-1_full_c2/parameters.csv'};
filename_out = 'results/zarnitsyna_data_1_combined/parameters.csv';

combine_csv(filenames_in,filename_out)

filenames_in = {'results/zarnitsyna_data_sequential_noise1-100000-2_full_new/parameters.csv',...
'results/zarnitsyna_data_sequential_noise1-100000-2_full_c/parameters.csv',...
'results/zarnitsyna_data_sequential_noise1-100000-2_alt3/parameters.csv'};
filename_out = 'results/zarnitsyna_data_2_combined/parameters.csv';

combine_csv(filenames_in,filename_out)

filenames_in = {'results/zarnitsyna_data_sequential_noise1-100000-3_full_new/parameters.csv',...
'results/zarnitsyna_data_sequential_noise1-100000-3_full_c/parameters.csv',...
'results/zarnitsyna_data_sequential_noise1-100000-3_full_c2/parameters.csv'};
filename_out = 'results/zarnitsyna_data_3_combined/parameters.csv';

combine_csv(filenames_in,filename_out)

filenames_in = {'results/different_params_data_sequential_noise1-100000-1_full_new/
parameters.csv',...
'results/different_params_data_sequential_noise1-100000-1_full_c/parameters.csv',...
'results/different_params_data_sequential_noise1-100000-1_full_c2/parameters.csv'};
filename_out = 'results/different_params_data_1_combined/parameters.csv';

combine_csv(filenames_in,filename_out)

filenames_in = {'results/different_params_data_sequential_noise1-100000-2_full_new/parameters.csv',...
'results/different_params_data_sequential_noise1-100000-2_full_c/parameters.csv',...
'results/different_params_data_sequential_noise1-100000-2_full_c2/parameters.csv'};
filename_out = 'results/different_params_data_2_combined/parameters.csv';

combine_csv(filenames_in,filename_out)

filenames_in = {'results/different_params_data_sequential_noise1-100000-3_full_new/parameters.csv',...
'results/different_params_data_sequential_noise1-100000-3_full_c/parameters.csv',...
'results/different_params_data_sequential_noise1-100000-3_full_c2/parameters.csv'};
filename_out = 'results/different_params_data_3_combined/parameters.csv';

combine_csv(filenames_in,filename_out)
