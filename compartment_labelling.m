% label vector indices in the ODE for readability
% input arguments:
% model: string e.g. 'baseline', 'XIT' or 'knockout_adaptive' (see solve_ode.m for all options)
% output arguments:
% c: a struct containing names of the compartments and the indices to which they correspond

function c = compartment_labelling(nV,model)
nE = 20;
nB = 5;

switch model
    case {'baseline', 'knockout_innate'}
        if(nV == 1)
            nT = 2;
        else
            nT = 3;
        end
        % compartment numbering
        c.T = 1; % target cells
        c.R = 2;
        c.I = c.R+1:c.R+nV; % infected cells
        c.V = c.I(end)+1:c.I(end)+nV; % virus
        c.F = c.V(end)+1; % interferon
        c.C = c.F+1:c.F+nT;
        c.E = reshape(c.C(end)+1:c.C(end)+nE*nT,nT,nE)';
        c.M = c.E(end,end)+1:c.E(end,end)+nT;
        c.B = reshape(c.M(end)+1:c.M(end)+(nB+1)*nV,nV,nB+1)';
        c.P = c.B(end,end)+1:c.B(end,end)+nV;
        c.A = c.P(end,end)+1:c.P(end,end)+nV;
        c.VR = c.A(end)+1:c.A(end)+nV;
    case 'knockout_adaptive'
        c.T = 1; % target cells
        c.R = 2;
        c.I = c.R+1:c.R+nV; % infected cells
        c.V = c.I(end)+1:c.I(end)+nV; % virus
        c.F = c.V(end)+1; % interferon
        c.B = reshape(c.F+1:c.F+(nB+1)*nV,nV,nB+1)';
        c.P = c.B(end,end)+1:c.B(end,end)+nV;
        c.A = c.P(end,end)+1:c.P(end,end)+nV;
        c.VR = c.A(end)+1:c.A(end)+nV;
    case 'no_immune'
        c.T = 1; % target cells
        c.I = c.T+1:c.T+nV; % infected cells
        c.V = c.I(end)+1:c.I(end)+nV; % virus
        c.VR = c.V+1:c.V+nV;
    case 'knockout_humoral'
        nT = 2;
        
        % compartment numbering
        c.T = 1; % target cells
        c.R = 2;
        c.I = c.R+1:c.R+nV; % infected cells
        c.V = c.I(end)+1:c.I(end)+nV; % virus
        c.F = c.V(end)+1; % interferon
        c.C = c.F+1:c.F+nT;
        c.E = reshape(c.C(end)+1:c.C(end)+nE*nT,nT,nE)';
        c.M = c.E(end,end)+1:c.E(end,end)+nT;
        c.VR = c.M(end)+1:c.M(end)+nV;
    case 'knockout_cellular'
        c.T = 1; % target cells
        c.R = 2;
        c.I = c.R+1:c.R+nV; % infected cells
        c.V = c.I(end)+1:c.I(end)+nV; % virus
        c.F = c.V(end)+1; % interferon
        c.B = reshape(c.F+1:c.F+(nB+1)*nV,nV,nB+1)';
        c.P = c.B(end,end)+1:c.B(end,end)+nV;
        c.A = c.P(end,end)+1:c.P(end,end)+nV;
        c.VR = c.A(end)+1:c.A(end)+nV;
    case 'XIT'
        if(nV == 1)
            nT = 2;
        else
            nT = 4;
        end
        % compartment numbering
        c.T = 1; % target cells
        c.R = 2;
        c.I = c.R+1:c.R+nV; % infected cells
        c.V = c.I(end)+1:c.I(end)+nV; % virus
        c.F = c.V(end)+1; % interferon
        c.C = c.F+1:c.F+nT;
        c.E = reshape(c.C(end)+1:c.C(end)+nE*nT,nT,nE)';
        c.M = c.E(end,end)+1:c.E(end,end)+nT;
        c.B = reshape(c.M(end)+1:c.M(end)+(nB+1)*nV,nV,nB+1)';
        c.P = c.B(end,end)+1:c.B(end,end)+nV;
        c.A = c.P(end,end)+1:c.P(end,end)+nV;
        c.VR = c.A(end)+1:c.A(end)+nV;
    case 'XC'
        if(nV == 1)
            nT = 2;
        else
            nT = 3;
        end
        
        % compartment numbering
        c.T = 1:nV; % target cells
        c.R = c.T(end)+1:c.T(end)+nV;
        c.I = c.R(end)+1:c.R(end)+nV; % infected cells
        c.V = c.I(end)+1:c.I(end)+nV; % virus
        c.F = c.V(end)+1:c.V(end)+nV; % interferon
        c.C = c.F(end)+1:c.F(end)+nT;
        c.E = reshape(c.C(end)+1:c.C(end)+nE*nT,nT,nE)';
        c.M = c.E(end,end)+1:c.E(end,end)+nT;
        c.B = reshape(c.M(end)+1:c.M(end)+(nB+1)*nV,nV,nB+1)';
        c.P = c.B(end,end)+1:c.B(end,end)+nV;
        c.A = c.P(end,end)+1:c.P(end,end)+nV;
        c.VR = c.A(end)+1:c.A(end)+nV;
    case 'XI'
        if(nV == 1)
            nT = 2;
        else
            nT = 4;
        end
        % compartment numbering
        c.T = 1:nV; % target cells
        c.R = c.T(end)+1:c.T(end)+nV;
        c.I = c.R(end)+1:c.R(end)+nV; % infected cells
        c.V = c.I(end)+1:c.I(end)+nV; % virus
        c.F = c.V(end)+1; % interferon
        c.C = c.F+1:c.F+nT;
        c.E = reshape(c.C(end)+1:c.C(end)+nE*nT,nT,nE)';
        c.M = c.E(end,end)+1:c.E(end,end)+nT;
        c.B = reshape(c.M(end)+1:c.M(end)+(nB+1)*nV,nV,nB+1)';
        c.P = c.B(end,end)+1:c.B(end,end)+nV;
        c.A = c.P(end,end)+1:c.P(end,end)+nV;
        c.VR = c.A(end)+1:c.A(end)+nV;
    case 'XT'
        if(nV == 1)
            nT = 2;
        else
            nT = 4;
        end
        % compartment numbering
        c.T = 1; % target cells
        if(nV == 1)
            c.R = c.T + (1:nV);
        else
            c.R12 = c.T + 1;
            c.R = c.R12 + (1:nV);
        end
        c.I = c.R(end)+1:c.R(end)+nV; % infected cells
        c.V = c.I(end)+1:c.I(end)+nV; % virus
        c.F = c.V(end)+(1:nV); % interferon
        c.C = c.F(end)+(1:nT);
        c.E = reshape(c.C(end)+1:c.C(end)+nE*nT,nT,nE)';
        c.M = c.E(end,end)+1:c.E(end,end)+nT;
        c.B = reshape(c.M(end)+1:c.M(end)+(nB+1)*nV,nV,nB+1)';
        c.P = c.B(end,end)+1:c.B(end,end)+nV;
        c.A = c.P(end,end)+1:c.P(end,end)+nV;
        c.VR = c.A(end)+1:c.A(end)+nV;
    case {'XI1','XI2','XI3'}
        if(nV == 1)
            nT = 2;
        else
            nT = 4;
        end
        % compartment numbering
        c.T = 1:nV; % target cells
        c.R = c.T(end)+1:c.T(end)+nV;
        c.I = c.R(end)+1:c.R(end)+nV; % infected cells
        c.V = c.I(end)+1:c.I(end)+nV; % virus
        c.F = c.V(end)+1:c.V(end)+nV; % interferon
        c.C = c.F(end)+1:c.F(end)+nT;
        c.E = reshape(c.C(end)+1:c.C(end)+nE*nT,nT,nE)';
        c.M = c.E(end,end)+1:c.E(end,end)+nT;
        c.B = reshape(c.M(end)+1:c.M(end)+(nB+1)*nV,nV,nB+1)';
        c.P = c.B(end,end)+1:c.B(end,end)+nV;
        c.A = c.P(end,end)+1:c.P(end,end)+nV;
        c.VR = c.A(end)+1:c.A(end)+nV;
end
end