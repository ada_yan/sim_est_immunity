# precompiles ODE files to speed up solving
#!/bin/bash    
	
matlab -r "addpath(genpath('general_functions'));addpath('ODEmodel_compile');ODE_compile_prior;quit;"
octave --eval 'addpath(genpath("general_functions"));
	compileC_oct("ODE_prior_c");'
	
matlab -r "addpath(genpath('general_functions'));addpath('ODEmodel_compile');ODE_compile_1strain;quit;"
octave --eval 'addpath(genpath("general_functions"));
	compileC_oct("ODE_1strain_c");'
	
matlab -r "addpath(genpath('general_functions'));addpath('ODEmodel_compile');ODE_compile_1strain_strain0;quit;"
octave --eval 'addpath(genpath("general_functions"));
	compileC_oct("ODE_1strain_strain0_c");'
	
matlab -r "addpath(genpath('general_functions'));addpath('ODEmodel_compile');ODE_compile_2strain;quit;"
octave --eval 'addpath(genpath("general_functions"));
	compileC_oct("ODE_2strain_c");'
	
matlab -r "addpath(genpath('general_functions'));addpath('ODEmodel_compile');ODE_compile_2strain_strain1;quit;"
octave --eval 'addpath(genpath("general_functions"));
	compileC_oct("ODE_2strain_strain1_c");'
	
matlab -r "addpath(genpath('general_functions'));addpath('ODEmodel_compile');ODE_compile_2strain_strain0;quit;"
octave --eval 'addpath(genpath("general_functions"));
	compileC_oct("ODE_2strain_strain0_c");'

matlab -r 		"addpath(genpath('general_functions'));addpath('ODEmodel_compile');ODE_compile_1strain_strain0_all;quit;"	
octave --eval 'addpath(genpath("general_functions"));
	addpath("ODEmodel_compile");
	compileC_oct("ODE_1strain_strain0_all_c");'

matlab -r 		"addpath(genpath('general_functions'));addpath('ODEmodel_compile');ODE_compile_2strain_XIT;quit;"	
octave --eval 'addpath(genpath("general_functions"));
	addpath("ODEmodel_compile");
	compileC_oct("ODE_2strain_XIT_c");'
	
matlab -r 		"addpath(genpath('general_functions'));addpath('ODEmodel_compile');ODE_compile_2strain_strain1_XIT;quit;"	
octave --eval 'addpath(genpath("general_functions"));
	addpath("ODEmodel_compile");
	compileC_oct("ODE_2strain_strain1_XIT_c");'

matlab -r 		"addpath(genpath('general_functions'));addpath('ODEmodel_compile');ODE_compile_2strain_strain0_XIT;quit;"	
octave --eval 'addpath(genpath("general_functions"));
	addpath("ODEmodel_compile");
	compileC_oct("ODE_2strain_strain0_XIT_c");'
	
matlab -r 		"addpath(genpath('general_functions'));addpath('ODEmodel_compile');ODE_compile_2strain_XI;quit;"	
octave --eval 'addpath(genpath("general_functions"));
	addpath("ODEmodel_compile");
	compileC_oct("ODE_2strain_XI_c");'

matlab -r 		"addpath(genpath('general_functions'));addpath('ODEmodel_compile');ODE_compile_2strain_strain1_XI;quit;"	
octave --eval 'addpath(genpath("general_functions"));
	addpath("ODEmodel_compile");
	compileC_oct("ODE_2strain_strain1_XI_c");' 

matlab -r 		"addpath(genpath('general_functions'));addpath('ODEmodel_compile');ODE_compile_2strain_strain0_XI;quit;"	
octave --eval 'addpath(genpath("general_functions"));
	addpath("ODEmodel_compile");
	compileC_oct("ODE_2strain_strain0_XI_c");'

matlab -r 		"addpath(genpath('general_functions'));addpath('ODEmodel_compile');ODE_compile_2strain_XC;quit;"	
octave --eval 'addpath(genpath("general_functions"));
	addpath("ODEmodel_compile");
	compileC_oct("ODE_2strain_XC_c");'
	
matlab -r 		"addpath(genpath('general_functions'));addpath('ODEmodel_compile');ODE_compile_2strain_strain1_XC;quit;"	
octave --eval 'addpath(genpath("general_functions"));
	addpath("ODEmodel_compile");
	compileC_oct("ODE_2strain_strain1_XC_c");'

matlab -r 		"addpath(genpath('general_functions'));addpath('ODEmodel_compile');ODE_compile_2strain_strain0_XC;quit;"	
octave --eval 'addpath(genpath("general_functions"));
	addpath("ODEmodel_compile");
	compileC_oct("ODE_2strain_strain0_XC_c");'
	
matlab -r 		"addpath(genpath('general_functions'));addpath('ODEmodel_compile');ODE_compile_2strain_XI1;quit;"	
octave --eval 'addpath(genpath("general_functions"));
	addpath("ODEmodel_compile");
	compileC_oct("ODE_2strain_XI1_c");'

matlab -r 		"addpath(genpath('general_functions'));addpath('ODEmodel_compile');ODE_compile_2strain_strain1_XI1;quit;"	
octave --eval 'addpath(genpath("general_functions"));
	addpath("ODEmodel_compile");
	compileC_oct("ODE_2strain_strain1_XI1_c");'

matlab -r 		"addpath(genpath('general_functions'));addpath('ODEmodel_compile');ODE_compile_2strain_strain0_XI1;quit;"	
octave --eval 'addpath(genpath("general_functions"));
	addpath("ODEmodel_compile");
	compileC_oct("ODE_2strain_strain0_XI1_c");'

matlab -r 		"addpath(genpath('general_functions'));addpath('ODEmodel_compile');ODE_compile_2strain_XI2;quit;"	
octave --eval 'addpath(genpath("general_functions"));
	addpath("ODEmodel_compile");
	compileC_oct("ODE_2strain_XI2_c");'
	
matlab -r 		"addpath(genpath('general_functions'));addpath('ODEmodel_compile');ODE_compile_2strain_strain1_XI2;quit;"	
octave --eval 'addpath(genpath("general_functions"));
	addpath("ODEmodel_compile");
	compileC_oct("ODE_2strain_strain1_XI2_c");'

matlab -r 		"addpath(genpath('general_functions'));addpath('ODEmodel_compile');ODE_compile_2strain_strain0_X0;quit;"	
octave --eval 'addpath(genpath("general_functions"));
	addpath("ODEmodel_compile");
	compileC_oct("ODE_2strain_strain0_X0_c");'
	
matlab -r 		"addpath(genpath('general_functions'));addpath('ODEmodel_compile');ODE_compile_2strain_XI3;quit;"	
octave --eval 'addpath(genpath("general_functions"));
	addpath("ODEmodel_compile");
	compileC_oct("ODE_2strain_XI3_c");'

matlab -r 		"addpath(genpath('general_functions'));addpath('ODEmodel_compile');ODE_compile_2strain_strain1_XI3;quit;"	
octave --eval 'addpath(genpath("general_functions"));
	addpath("ODEmodel_compile");
	compileC_oct("ODE_2strain_strain1_XI3_c");' 
  

matlab -r 		"addpath(genpath('general_functions'));addpath('ODEmodel_compile');ODE_compile_1strain_knockout_A;quit;"	
octave --eval 'addpath(genpath("general_functions"));
	addpath("ODEmodel_compile");
	compileC_oct("ODE_1strain_knockout_A_c");'

matlab -r 		"addpath(genpath('general_functions'));addpath('ODEmodel_compile');ODE_compile_1strain_strain0_knockout_A;quit;"	
octave --eval 'addpath(genpath("general_functions"));
	addpath("ODEmodel_compile");
	compileC_oct("ODE_1strain_strain0_knockout_A_c");'
	
matlab -r 		"addpath(genpath('general_functions'));addpath('ODEmodel_compile');ODE_compile_1strain_knockout_E;quit;"	
octave --eval 'addpath(genpath("general_functions"));
	addpath("ODEmodel_compile");
	compileC_oct("ODE_1strain_knockout_E_c");'

matlab -r 		"addpath(genpath('general_functions'));addpath('ODEmodel_compile');ODE_compile_1strain_strain0_knockout_E;quit;"	
octave --eval 'addpath(genpath("general_functions"));
	addpath("ODEmodel_compile");
	compileC_oct("ODE_1strain_strain0_knockout_E_c");'
	
matlab -r 		"addpath(genpath('general_functions'));addpath('ODEmodel_compile');ODE_compile_1strain_no_immune;quit;"	
octave --eval 'addpath(genpath("general_functions"));
	addpath("ODEmodel_compile");
	compileC_oct("ODE_1strain_no_immune_c");'

matlab -r 		"addpath(genpath('general_functions'));addpath('ODEmodel_compile');ODE_compile_1strain_strain0_no_immune;quit;"	
octave --eval 'addpath(genpath("general_functions"));
	addpath("ODEmodel_compile");
	compileC_oct("ODE_1strain_strain0_no_immune_c");'