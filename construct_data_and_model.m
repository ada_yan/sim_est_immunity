% read in data and set up model for fitting with MCMC library

% input arguments:
% exp: string. 'prior','single','sequential' or 'sequential_high_xreact'.
% mex_flag: logical. 1 iff solving ODEs using precompiled files.
% data_filename: string. name of csv file containing synthetic data.
% dir_base: string. directory in which to store output files
% job_id: empty or string. If non-empty, this string forms part of the output folder name.
% If empty, an identifier is generated based on the system clock.
% shell_filename: path to bash script with which this function was run. For keeping a copy of Matlab files for reproducibility.
% MCMC_options_fn: function handle to function to setup MCMC options e.g. the number of iterations.
% copy: logical. Iff 1, keeps a copy of Matlab files for reproducibility.

% output arguments:
% data: struct containing synthetic data. 
% If single infection, field Vj contains a vector with the total viral load for ferret j.
% If sequential infection, field Vij contains a vector with the total viral load for the ith strain for ferret j.
% model: struct containing the following fields:
% model.return_likelihood: function which takes model parameters and returns the log likelihood. 
% This function uses the equations in model.equations.
% model.equations.return_observables: function which takes model parameters and returns observables
% model.equations.LL_data_p: function which takes data, observables and the measurement error
% and returns the log likelihood of data given parameters
% model.equations.LL_p_prior: function which takes model parameters and returns
% the log likelihood of the parameters given the prior
% model.equations.combine_LL: function which combines all the parts of the likelihood
% (doesn't do anything special, exists for historical reasons)
% model.generate_random_parameters: function which takes the model and data
% and generates a starting point for the MCMC chain
% MCMC_options: struct containing options for fitting e.g. the number of iterations
function [data,model,MCMC_options] = construct_data_and_model(exp,mex_flag,data_filename,~,dir_base,job_id,shell_filename,MCMC_options_fn,copy)

MCMC_options = MCMC_options_fn();
data = read_ferret_data(data_filename, exp);

model.parameters = construct_parameters_single;

if(strcmp(exp,'sequential_high_xreact'))
    % modify parameter values for high cross-reactivity data set
	model.parameters(end-3).value = log10(1e5/.1);
	model.parameters(end-2).value = log10(1e5/.9);
	model.parameters(end-1).value = log10(100*6e-5*1.5*.1);
elseif(strcmp(exp,'single') && isempty(strfind(data_filename, 'zarnitsyna')))
    % fix k_{C11} and k_{C12}
	model.parameters(end-3).fit_option = 0;
	model.parameters(end-2).fit_option = 0;
end

% as our model is not hierarchical, this doesn't do anything useful
data = sort_parameters(data,model.parameters);

% make output filename
if(iscell(data_filename))
    data_filename = data_filename{1};
end

slashes = strfind(data_filename,'/');
dots = strfind(data_filename,'.');

MCMC_options.data_name = data_filename(slashes(end)+1:dots(end)-1);
MCMC_options.out_filename = MCMCfileinit(MCMC_options.data_name,dir_base,job_id,shell_filename,MCMC_options.total_iterations,copy);

% function to return likelihood for MCMC
model.return_likelihood = @return_likelihood;
% subfunctions for model.return_likelihood
if(strcmp(exp, 'prior'))
	model.equations.return_observables = @return_dummy_observables;
	model.equations.LL_data_p = @LL_dummy;
elseif(strcmp(exp,'single'))
    % function to calculate total viral load given parameters
	model.equations.return_observables = @(p) return_viral_load(p,0,mex_flag);
    % function to calculate likelihood of data given total viral load
	model.equations.LL_data_p = @LL_virus;
else
	model.equations.return_observables = @(p) return_viral_load(p,1,mex_flag);
	model.equations.LL_data_p = @LL_virus;
end
% function to calculate likelihood of parameters given prior
model.equations.LL_p_prior = @(p) LL_prior(p, mex_flag);
model.equations.combine_LL = {@combine_LL};

% function to generate random parameters
model.generate_random_parameters = ...
    @(data,model) generate_random_parameters(strcmp(exp,'single'),...
    data,model,data_filename);

end

% reads in synthetic data

% input arguments:
% filename: string: name of csv file
% exp: string: 'prior','single','sequential' or 'sequential_high_xreact'

% output argument:
% data: struct containing synthetic data. 
% If single infection, field Vj contains a vector with the total viral load for ferret j.
% If sequential infection, field Vij contains a vector with the total viral load for the ith strain for ferret j.

function data = read_ferret_data(filename, exp)
switch exp
    case 'prior'
        data.V = 0;
    case 'single'
        M = csvread(filename);
        n_data_sets = size(M,2);
        count = 1;
        for k = 1:n_data_sets
            data.(sprintf('V%d',k)) = M(:,count);
            count = count+1;
        end
    otherwise
	M = csvread(filename);
	n_data_sets = size(M,2)/2;
	count = 1;
	for j = 1:n_data_sets
		for i = 1:2
			data.(sprintf('V%d%d',i,j)) = M(:,count);
			count = count+1;
		end
	end
end
end

% exists for historical reasons.
% practically, given the struct data, just adds a field
% parameters = 1:length(parameters).

function data = sort_parameters(data,parameters)
np = length(parameters)-length(data)-1;
for i = 1:length(data)
    data(i).parameters = [1:np,np+i,length(parameters)];
end
end