% read in data and set up model for fitting with MCMC library

% input arguments:
% exp: string. 'prior','single','single_fixed_xreact','sequential' or 'sequential_high_xreact'.
% mex_flag: logical. 1 iff solving ODEs using precompiled files.
% data_filename: string. name of csv file containing synthetic data.
% dir_base: string. directory in which to store output files
% job_id: empty or string. If non-empty, this string forms part of the output folder name.
% If empty, an identifier is generated based on the system clock.
% shell_filename: path to bash script with which this function was run. For keeping a copy of Matlab files for reproducibility.
% MCMC_options_fn: function handle to function to setup MCMC options e.g. the number of iterations.
% copy: logical. Iff 1, keeps a copy of Matlab files for reproducibility.

% output arguments:
% data: struct containing synthetic data. 
% If single infection, field Vj contains a vector with the total viral load for ferret j.
% If sequential infection, field Vij contains a vector with the total viral load for the ith strain for ferret j.
% model: struct containing the following fields:
% model.return_likelihood: function which takes model parameters and returns the log likelihood. 
% This function uses the equations in model.equations.
% model.equations.return_observables: function which takes model parameters and returns observables
% model.equations.LL_data_p: function which takes data, observables and the measurement error
% and returns the log likelihood of data given parameters
% model.equations.LL_p_prior: function which takes model parameters and returns
% the log likelihood of the parameters given the prior
% model.equations.combine_LL: function which combines all the parts of the likelihood
% (doesn't do anything special, exists for historical reasons)
% model.generate_random_parameters: function which takes the model and data
% and generates a starting point for the MCMC chain
% MCMC_options: struct containing options for fitting e.g. the number of iterations
function [data,model,MCMC_options] = construct_data_and_model_prior(data_filename,~,dir_base,job_id,shell_filename,MCMC_options_fn,copy)

[data,model,MCMC_options] = construct_data_and_model('prior', 1, data_filename,~,dir_base,job_id,shell_filename,MCMC_options_fn,copy);

end