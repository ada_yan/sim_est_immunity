% specify model parameters and ranges for MCMC fitting
% output arguments:
% p: a struct array of length n where n is the number of parameters
% p(i) contains the fields
% layer: string: exists for historical reasons, put whatever you want
% pname: string: parameter name
% data_set_no: vector: exists for historical reasons, put 1 for all parameters
% fit_option: logical: 1 if the parameter is fitted, 0 if fixed
% value: scalar: true value of parameter
% proposal_width_max: scalar: when adapting the width of the proposal distribution,
% it cannot exceed this value (basically the range of the prior)
% type: string: exists for historical reasons, put whatever you want
function p = construct_parameters_single()

% the types are largely historical; these just get concatenated into a list
% of parameter names

type_F_names = {'g','deltaF','phi_times_mean_pF','T_0','rho','alpha'};

type_V_names = {'R_0','r','deltaI','deltaV_minus_deltaVR','deltaVR',...
    'pF_on_mean_pF','kappaF_times_mean_pF','s_times_mean_pF',...
    'pA_times_kappaA_times_B_0','deltaA','deltaB','kB','betaB',...
    'tauB','pR','gamma_0','V_0'};

type_T_names = {'betaC','deltaE','tauM','tauE','eps'};

type_B_names = {'kC_on_a_times_d11','kC_on_a_times_d12','C_0_times_kappaE_times_a_times_d11'};

names = horzcat(type_F_names,type_V_names,type_T_names,...
    type_B_names);
type = horzcat(repmat({'F'},1,length(type_F_names)),...
    repmat({'V'},1,length(type_V_names)),...
    repmat({'T'},1,length(type_T_names)),...
    repmat({'B'},1,length(type_B_names)));

% bounds of prior distribution for assigning initial width of proposal
% distribution
range = return_range();

p_proposal_width_maxs = range(1:end-1,2) - range(1:end-1,1);

% true parameter values
values = log10([.8,2,3.3e-5,7e7,2,.01,5e-7*12.6*7e7/2/(10+5e-7*7e7),-(5e-7*7e7+10+2)/2+...
    sqrt((2-(5e-7*7e7+10))^2+4*5e-7*7e7*12.6)/2,2,2,8,1,2.5e-3,5e-4,.8*3*10*15,.04,.11,2e8,1,4,4e4,4e4,1,...
    1,0.6,14,5,.02,1e5/.9,1e5/.1,100*6e-5*1.5*.9]);

p_single = repmat(struct('layer','p','pname','','data_set_no',1,'fit_option',1,...
    'value',0,'proposal_width_max',0,'type',''),length(names),1);

for i = 1:length(names)
    p_single(i).pname = names{i};
    p_single(i).type = type{i};
end

[p_single.value] = num2cell(values){:};
[p_single.proposal_width_max] = num2cell(p_proposal_width_maxs){:};

% don't fit pF_on_mean_pF -- must be equal to 1
p_single(strcmp(names,'pF_on_mean_pF')).fit_option = 0;

% measurement error
measurement_error = struct('layer','m','pname','sigma_m','data_set_no',1,'fit_option',1,...
    'value',.5,'proposal_width_max',range(end,2) - range(end,1),'type','O');

p = vertcat(p_single,measurement_error);

end