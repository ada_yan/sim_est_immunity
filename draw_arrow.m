function a = draw_arrow(g, n_days_in, n_days_total, linestyle)
a = annotation('arrow',(g.Position(1)+g.Position(3)*n_days_in/n_days_total)*ones(1,2),...
[.5,.3],'Color','k','LineStyle',linestyle,'LineWidth',2);
end