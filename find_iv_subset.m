% specifies initial values for solving ODEs given model and parameters

% input arguments:
% p: struct created by construct_parameters_single: parameters
% c: struct created by compartment_labelling: names of ODE compartments
% model: string e.g. 'baseline', 'XIT' or 'knockout_adaptive' (see solve_ode.m for all options)

% output arguments:
% iv: vector of initial values

function iv = find_iv_subset(p,c,model)
iv = zeros(c.VR(end),1);
names = {p.pname};
iv(c.T) = 10^(p(strcmp(names,'T_0')).value);
iv(c.V) = 10^p(strcmp(names,'V_0')).value*ones(size(c.V));
iv(c.VR) = iv(c.V)*10^p(strcmp(names,'gamma_0')).value*10^p(strcmp(names,'alpha')).value;

% some of the immune compartments don't exist in some of the models, hence
% the initial value vector depends on which model is being used
switch model
    case {'baseline','XC','XIT','XI','XT',...
            'XI1','XI2','XI3','knockout_innate'}
        iv(c.C) = ones(size(c.C));
        iv(c.B(1,:)) = ones(size(c.B(1,:)));
    case 'knockout_humoral'
        iv(c.C) = ones(size(c.C));
    case {'knockout_adaptive','knockout_cellular'}
        iv(c.B(1,:)) = ones(size(c.B(1,:)));
end
end