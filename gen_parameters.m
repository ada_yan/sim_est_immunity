% a wrapper for constructing and saving model parameters to then generate synthetic data

% input argument:
% exp: string. 'single','single_fixed_xreact','sequential' or 'sequential_high_xreact'.

function gen_parameters(exp)

[~,model,~] = construct_data_and_model(exp,1,'input/dummy.csv','','temp/','','',@setup_MCMC_options_no_calibrate,0);
p = model.parameters;
save(strcat('results/start/parameters_',exp,'.mat'),'p','-v7')

end
