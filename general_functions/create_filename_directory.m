% given a filename which includes a directory, create that directory

% input argument:
% filename: string
function create_filename_directory(filename)
    slashes = strfind(filename,'/');
    if(~isempty(slashes))
    	% read filename until last slash
        directory = filename(1:slashes(end));
        % then create that directory if it doesn't already exist
        if(~exist(directory,'dir'))
            mkdir_retro(directory);
        end
    end
end