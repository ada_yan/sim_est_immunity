function p = log10ncdf(x,mu,sigma)
%LOG10NCDF Log10normal cumulative distribution function (cdf).
%   P = LOG10NCDF(X,MU,SIGMA) returns values at X of the log10normal cdf with 
%   distribution parameters MU and SIGMA.  MU and SIGMA are the mean and 
%   standard deviation, respectively, of the associated normal distribution.  
%   The size of P is the common size of X, MU and SIGMA.  A scalar input 
%   functions as a constant matrix of the same size as the other inputs.
%   adapted from Matlab logncdf (Copyright 1993-2013 The MathWorks, Inc.)

% Negative data would create complex values, which erfc cannot handle.
x(x < 0) = 0;

z = (log10(x)-mu) ./ sigma;

% Use the complementary error function, rather than .5*(1+erf(z/sqrt(2))),
% to produce accurate near-zero results for large negative x.
p = 0.5 * erfc(-z ./ sqrt(2));

end
