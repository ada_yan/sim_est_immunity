function y = log10npdf(x,mu,sigma)
%LOGNPDF Log10normal probability density function (pdf).
%   Y = LOG10NPDF(X,MU,SIGMA) returns values at X of the log10normal pdf with 
%   distribution parameters MU and SIGMA. MU and SIGMA are the mean and 
%   standard deviation, respectively, of the associated normal distribution.  
%   The size of Y is the common size of the input arguments. A scalar input 
%   functions as a constant matrix of the same size as the other inputs.
%   adapted from Matlab lognpdf (Copyright 1993-2013 The MathWorks, Inc.)

% Negative data would create complex values, potentially creating spurious
% NaNi's in other elements of y.  Map them, and zeros, to the far right
% tail, whose pdf will be zero.
x(x <= 0) = Inf;

y = exp(-0.5 * ((log10(x) - mu)./sigma).^2) ./ (x .* sqrt(2*pi) .* sigma)./log(10);

end