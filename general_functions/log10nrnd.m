% returns y such that log10(y) is normally distributed with mean mu and
% standard deviation sigma
% takes same arguments as lognrnd
function y = log10nrnd(mu,sigma,varargin)
y = lognrnd(log(10)*mu,log(10)*sigma,varargin{:});
end