% make directory and parent directories if necessary

% input argument:
% dir_name: string
function mkdir_retro(dir_name)

% assume that dir_name is a directory. Append slash to end if there isn't already one
if(~strcmp(dir_name(end),'/'))
    dir_name = strcat(dir_name,'/');
end

% make directories starting from oldest parent
slashes = strfind(dir_name,'/');
for i = 1:length(slashes)
    dir_name_part = dir_name(1:slashes(i));
    if(exist(dir_name_part,'dir') == 0)
        mkdir(dir_name_part);
    end
end

end