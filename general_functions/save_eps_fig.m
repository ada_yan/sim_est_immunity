% save a figure in eps and fig formats

% input arguments:
% fig_handle: figure handle
% filename: string
% colour: logical 1 = save in colour, 0 = save in grayscale

function save_eps_fig(fig_handle,filename,colour)
if(~isempty(filename))
    create_filename_directory(filename);
    if(colour)
        print(fig_handle,filename,'-depsc');
    else
        print(fig_handle,filename,'-deps');
    end
    saveas(fig_handle,filename,'fig');
end
end