% generates synthetic data

% input arguments:
% sequential: logical: 1 iff sequential infection data to be generated
% p: struct returned by construct_parameters_single
% sigma: scalar: standard deviation of measurement error
% seed: integer: random number seed

% output arguments:
% observables_array: matrix concatenating all of the fields in observables
% where observables is a struct containing synthetic data.
% If single infection, field Vj contains a vector with the total viral load for ferret j.
% If sequential infection, field Vij contains a vector with the total viral load for the ith strain for ferret j.

function observables_array = generate_data(sequential,p,sigma,seed)

observables = return_viral_load(p,sequential,1);

observables_array = add_noise_and_threshold(observables, sigma, seed);

end
