% generates directories for reading MCMC chains for postprocessing,
% writing .mat files during post-processing and saving figures

% input arguments:
% data_set: 'main', 'zarnitsyna' or 'different_params'
% low: logical: 1 unless generating directories for high cross-reactivity data

% output argument:
% dir_string: struct containing (a subset of) the following fields:

% the middle fields:
% sequential: model fitted to sequential infection data
% single: model fitted to single infection data, where the
% degree of cross-reactivity in the cellular adaptive immune
% response is fixed

% the end fields:
% dir_string.....samples: 1x3 cell array containing
% folder names for parallel chains for fitted model
% dir_string.....start: 1x3 cell array containing
% folder names for the adaptive iterations of the chains
% dir_string.....true: string containing filename
% of a chain which starts at the true parameters

% the figure fields:
% figs: string containing folder in which to store main text figs
% figs_si: string containing folder in which to store supplementary figs

function dir_string = generate_dir_strings(data_set, low)
n_parallel_chains = 3;
if(low)
    dir_string.sequential.samples = gen_parallel_strings(data_set,'sequential',n_parallel_chains,0,0);
    dir_string.sequential.start = gen_parallel_strings(data_set,'sequential',n_parallel_chains,1,0);
    dir_string.figs = strcat('figs/figs_', data_set, '/');
    dir_string.figs_si = dir_string.figs; % historical
    dir_string.sequential.true = gen_parallel_strings(data_set,'sequential',n_parallel_chains,0,1);
else
    dir_string.sequential.samples = gen_parallel_strings(data_set,'high_xreact',n_parallel_chains,0,0);
    dir_string.sequential.true = gen_parallel_strings(data_set,'high_xreact',n_parallel_chains,0,1);
    dir_string.figs = 'figs/figs_si_high_xreact/';
end
dir_string.single.samples = gen_parallel_strings(data_set,'single',n_parallel_chains,0,0);
dir_string.single.start = gen_parallel_strings(data_set,'single',n_parallel_chains,1,0);
dir_string.prior.samples = gen_parallel_strings(data_set,'prior',n_parallel_chains,0,0);
dir_string.single.true = dir_string.sequential.true;
end

function dir_strings = gen_parallel_strings(data_set, filename,n_parallel_chains,start, true_pars)
if(true_pars)
    switch data_set
        case 'main'
            dir_strings = strcat('results/ferret_data_',filename,'_noise1-10000-fixed/');
        case 'zarnitsyna'
            dir_strings = 'input/model_misspecification/';
        case 'different_params'
            dir_strings = 'input/different_params/';
    end
    return
end
dir_strings = cell(1,n_parallel_chains);
for i = 1:n_parallel_chains
    if(start)
        dir_strings{i} = strcat('results/ferret_data_',filename,'_noise1-10000-random',num2str(i),'/');
    else
        dir_strings{i} = strcat('results/ferret_data_',filename,'_noise1_random',num2str(i),'_combined/');
    end
    if(~strcmp(data_set, 'main'))
        dir_strings{i} = replace_string(dir_strings{i}, data_set);
    end
end
end

function new_string = replace_string(old_string, data_set)
new_string = strrep(old_string, 'ferret', data_set);
new_string = strrep(new_string, 'random', '');
end