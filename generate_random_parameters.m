% generates random parameter set to start MCMC chain

% input arguments:
% fixed_xreact: logical. Iff 1, the degree of cross-reactivity
% in the cellular adaptive immune response is fixed.
% data: struct containing synthetic data constructed by
% construct_data_and_model
% model: struct containing model constructed by
% construct_data_and_model

% output argument:
% prop_p: struct containing random parameter values with the
% constraint that the log likelihood exceeds a threshold.
% the form of this struct is the same as that constructed by
% construct_parameters_single.

function prop_p = generate_random_parameters(fixed_xreact,data,model, data_filename)

LL_thres = -5000;
range = return_range();
range(range == Inf) = 0;
range(range == -Inf) = 0;
names = {model.parameters.pname};
if(fixed_xreact)
    if(~isempty(strfind(data_filename,'different')))
        kC_values = [5.2430, 8.7840];
    else
        kC_values = [log10(1e5/.9), log10(1e5/.1)];
    end
    range(strcmp(names,'kC_on_a_times_d11'),:) = repmat(kC_values(1),1,2);
    range(strcmp(names,'kC_on_a_times_d12'),:) = repmat(kC_values(2),1,2);
end

prop_p = model.parameters;
cond = 0;
while(cond == 0)
    for i = 1:size(range,1)
        prop_p(i).value = unifrnd_patch(range(i,1),range(i,2));
    end
    [LL,~] = model.return_likelihood(prop_p,data,model,0,[],1);
    cond = sum(LL) > LL_thres;
    
end
end

% version of unifrnd which works if in1 == in2
function out = unifrnd_patch(in1,in2)
if(in1 == in2)
    out = in1;
else
    out = unifrnd(in1,in2);
end
end