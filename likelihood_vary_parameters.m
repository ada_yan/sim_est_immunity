% calculates the likelihood of parameters given sequential infection data
% as the parameters are varied from the true value

% input argument:
% dir_string: string: folder in which to store outputted mat file
function likelihood_vary_parameters(dir_string)
[data,model,~] = construct_data_and_model(...
    'sequential','input/ferret_data_sequential_noise1.csv','','results/','','',...
    @setup_MCMC_options_no_calibrate,0);
range = return_range();
parameters = model.parameters;
baseline_values = [parameters.value];

% n_div+1 is the number of different values for each parameter
n_div = 10;
% cold chain only
temperature = 1;

LL_default = model.return_likelihood(parameters,data,model,temperature,[],1);
LL = cell(1,length(parameters));
varied_values = LL;
for i = 1:length(parameters)
    % change deltaA and deltaB only
    if(strcmp(parameters(i).pname,'deltaA') || strcmp(parameters(i).pname,'deltaB'))
        % construct array of values over which to calculate LL
        varied_values{i} = sort(unique([linspace(range(i,1),range(i,2),n_div),baseline_values(i)]));
        LL{i} = zeros(size(varied_values{i}));
        % return likelihood for each parameter set
        for j = 1:length(varied_values{i})
            parameters(i).value = varied_values{i}(j);
            [LL{i}(j),~] = model.return_likelihood(parameters,data,model,temperature,[],1);
        end
        parameters(i).value = baseline_values(i);
    end
end

save(strcat(dir_string,'/LL_vary_parameters.mat'),...
    'LL','LL_default','baseline_values','varied_values','-v7')
end