% the ODE compiler uses the function maximum, rather than max, to find the maximum number in an array.
% for compatibility, we define the same function in Octave.

% input arguments:
% any number of arguments for which max(varargin{:}) gives a valid answer

% output argument:
% out: scalar: max(varargin{:})

function out = maximum(varargin)
out = max(varargin{:});
end