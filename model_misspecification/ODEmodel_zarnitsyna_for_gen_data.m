% solve ODEs for model modified form Zarniotsyna et al (2016) as documented
% in File S6
% IEI: inter-exposure interval: 0 for single infection, or 1, 3, 5, 7, 10,
% 14 for sequential infection
% model: 'baseline', 'XC', 'XIT', 'XI', 'XT', 'knockout_adaptive',
% 'knockout_innate','knockout_humoral','no_immune','knockout_cellular'
% t_end: no. days after second infection for which to solve ODEs

function sol = ODEmodel_zarnitsyna_for_gen_data(IEI, model, t_end)
% label model compartments
c = compartment_labelling(model);
% get parameter values
p = setup_parameters_zarnitsyna();

% for knockouts, set relevant parameter values to zero
% (knockout_humoral is the same as baseline)
switch model
    case {'knockout_cellular', 'knockout_adaptive'}
        p.k_R = 0;
    case 'knockout_innate'
        p.k_M = 0;
    case 'no_immune'
        p.k_R = 0;
        p.k_M = 0;
end

% set initial conditions for first infection
iv = set_iv(p, c);
options = odeset('NonNegative',1:length(iv));

if(IEI == 0)
    % single infection solving times
    t_vec = 0:t_end;
else
    % sequnetial infection solving times for primary infection
    t_vec = -IEI:0;
end

% denotes that second strain should be set to 0 (eliminate attoviruses)
V_active = [1;0];
% solve for first infection
[t1, y1] = ode15s(@(t,x)ODE(t,x,p,c,model,V_active),t_vec,iv,options);

% if IEI = 1, t_vec is of length 2, so ode15s outputs intermediate values.
% keep only the values we want
if(IEI == 1)
    t1 = [t1(1);t1(end)];
    y1 = [y1(1,:);y1(end,:)];
end

% second infection
if(IEI > 0)
    % get initial conditions from end of first infection
    iv = y1(end,:);
    % add second strain
    iv(c.V(2)) = p.V_0;
    t_vec = 0:t_end;
    % both strains can now be non-zero
    V_active = [1;1];
    [t2, y2] = ode15s(@(t,x)ODE(t,x,p,c,model,V_active),t_vec,iv,options);
    % concatenate solutions
    sol.x = horzcat(t1(1:end-1)', t2');
    sol.y = horzcat(y1(1:end-1,c.V)', y2(:,c.V)');
else
    sol.x = t1;
    sol.y = y1(:,c.V(1))';
end
end

% ODE model (see File S6)
function xdot = ODE(~,x,p,c, model, V_active)
tol = 1e-3;
V_above_tol = x(c.V) > tol;
xdot = zeros(length(x),1);
switch model
    case {'XC', 'XI'}
        dT_infection = -p.beta .* x(c.T) .* x(c.V);
    otherwise
        dT_infection = -p.beta .* x(c.T) .* sum(x(c.V));
end
switch model
    case 'XT'
        dT_refractory = -p.k_M .* sum(x(c.M)) .* x(c.T);
        cells_for_infection = x(c.T) + flipud(x(c.R));
    case 'XC'
        dT_refractory = -p.k_M .* x(c.M) .* x(c.T) .* V_active;
        cells_for_infection = x(c.T);
    otherwise
        dT_refractory = -p.k_M .* x(c.M) .* x(c.T);
        cells_for_infection = x(c.T);
end
if(strcmp(model, 'XT'))
    xdot(c.T) = dT_infection + dT_refractory + p.g * (p.T_0 - x(c.T) - x(c.R12) - sum(x(c.I)));
    xdot(c.R) = p.k_M .* x(c.M) .* x(c.T) - ...
        p.beta .* x(c.R) .* flipud(x(c.V) .* V_active) - ...
        p.k_M .* flipud(x(c.M)) .* x(c.R) + p.g * x(c.R12);
    xdot(c.R12) =  sum(p.k_M .* flipud(x(c.M)) .* x(c.R)) - 2 * p.g * x(c.R12);
elseif(length(c.T) == 2)
    xdot(c.T) = dT_infection + dT_refractory + p.g * (p.T_0 - x(c.T) - x(c.I));
else
    xdot(c.T) = dT_infection + dT_refractory + p.g * (p.T_0 - x(c.T) - sum(x(c.I)));
end

switch model
    case {'XI','XIT','XT'}
        V_2_T_cell_pool_2 = 4;
    otherwise
        V_2_T_cell_pool_2 = 3;
end
T_cell_killing = [(1 - p.kappa) * x(c.T_R(1)) + p.kappa * x(c.T_R(3));...
    (1 - p.kappa) * x(c.T_R(2)) + p.kappa * x(c.T_R(V_2_T_cell_pool_2))];
xdot(c.I) = p.beta .* cells_for_infection .* x(c.V) .* V_active - p.k_R .* T_cell_killing .* x(c.I) - p.delta * x(c.I);
xdot(c.V) = (p.p * x(c.I) - p.c * x(c.V)) .* V_active .* V_above_tol;
switch model
    case {'XC','XT'}
        innate_induction = x(c.I) .* V_active;
    otherwise
        innate_induction = sum(x(c.I));
end
xdot(c.M) = p.sigma_M .* innate_induction ./ (p.phi_M + innate_induction) .* (1 - x(c.M)) -...
    p.d_M * x(c.M);
xdot(c.A) = p.gamma * x(c.V) - p.d_A * x(c.A);
switch model
    case {'XI','XIT','XT'}
        antigen_presentation = [x(c.A(1)) ./ (p.phi / (1 - p.kappa) + x(c.A(1)));
            x(c.A(2)) ./ (p.phi / (1 - p.kappa) + x(c.A(2)));
            x(c.A(1)) ./ (p.phi / p.kappa + x(c.A(1)));
            x(c.A(2)) ./ (p.phi / p.kappa + x(c.A(2)))];
    otherwise
        antigen_presentation = [x(c.A(1)) ./ (p.phi / (1 - p.kappa) + x(c.A(1)));
            x(c.A(2)) ./ (p.phi / (1 - p.kappa) + x(c.A(2)));
            (x(c.A(1)) + x(c.A(2))) ./ (p.phi / p.kappa + (x(c.A(1)) + x(c.A(2))))];
end

xdot(c.T_P) = -p.rho .* x(c.T_P) .* antigen_presentation;
xdot(c.T_E) = p.rho * (x(c.T_P) + x(c.T_E)) .* antigen_presentation...
    - (p.alpha + p.r) .* x(c.T_E) .* (1 - antigen_presentation) ...
    - p.mu .* x(c.T_E) .* mean(x(c.M));
xdot(c.T_M) = p.r * x(c.T_E) .* (1 - antigen_presentation);
xdot(c.T_R) = p.mu .* x(c.T_E) .* mean(x(c.M)) - p.d_R .* x(c.T_R);
end

% label compartments
function c = compartment_labelling(model)
n_V = 2;
switch model
    case {'XI', 'XIT', 'XT'}
        n_C = 4;
    otherwise
        n_C = 3;
end
switch model
    case {'XC', 'XI'}
        c.T = 1:n_V;
    otherwise
        c.T = 1;
end
switch model
    case 'XT'
        c.R = c.T + (1:n_V);
        c.R12 = c.R(end) + 1;
        c.I = c.R12 + (1:n_V);
    otherwise
        c.I = c.T(end) + (1:n_V);
end
c.V = c.I(end) + (1:n_V);
switch model
    case {'XC', 'XT'}
        c.M = c.V(end) + (1:n_V);
    otherwise
        c.M = c.V(end) + 1;
end
c.A = c.M(end) + (1:n_V);
c.T_P = c.A(end) + (1:n_C);
c.T_E = c.T_P(end) + (1:n_C);
c.T_M = c.T_E(end) + (1:n_C);
c.T_R = c.T_M(end) + (1:n_C);
end

% set initial conditions
function iv = set_iv(p, c)
iv = zeros(c.T_R(end),1);
iv(c.T) = p.T_0;
iv(c.V(1)) = p.V_0;
iv(c.M) = 1e-6;
iv(c.T_P) = 1;
end