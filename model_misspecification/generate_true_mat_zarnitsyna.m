% generate .mat file which contain the viral load trajectories for the
% `true' parameters and the Zarnitsyna et al. model, for the baseline
% model, models where immune components are removed, and models where
% the components mediating cross-protection are restricted
function generate_true_mat_zarnitsyna()

% need single infection trajectories for the models where immune components
% are removed
model_single = {'knockout_adaptive','knockout_cellular',...
    'knockout_humoral','knockout_innate','no_immune'};
model_sequential = {'XC','XI','XIT','XT'};

for i = 1:length(model_single)
    observables_true = return_viral_load_true(0, model_single{i},1);
    save(make_filename(model_single{i}), 'observables_true')
end

% need sequential infection trajectories for the models where
% the components mediating cross-protection are restricted
% calculate for IEI = 1 only
interval = 1;
for i = 1:length(model_sequential)
    observables_true = return_viral_load_true(interval, model_sequential{i},0);
    save(make_filename(model_sequential{i}), 'observables_true')
end


intervals = [0, 1, 3, 5, 7, 10, 14, 2, 6, 20];
model = 'baseline';
observables_true = return_viral_load_true(intervals, model,0);
save(make_filename(model), 'observables_true')


end

% calculate the viral load for the given model and IEI, and
% format into an array
function viral_load = return_viral_load_true(IEI, model, single)
t_end = 30;
t_start = -20;
n_V = 2;
viral_load = -1*ones(length(IEI),length(t_start:t_end), n_V);
n_days = size(viral_load,2);

if(single)
    sol = ODEmodel_zarnitsyna_for_gen_data(0, model, t_end);
    viral_load = sol.y;
else
    for i = 1:length(IEI)
        sol = ODEmodel_zarnitsyna_for_gen_data(IEI(i), model, t_end);
        if(IEI(i) == 0)
            viral_load(i,(n_days - length(sol.x) + 1):end,2) = sol.y';
        else
            viral_load(i,(n_days - length(sol.x) + 1):end,:) = sol.y';
        end
    end
end

end

function filename = make_filename(model)
dir_string = 'input/model_misspecification/';
filename = strcat(dir_string, model, 'VR_true.mat');
end