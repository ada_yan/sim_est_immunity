% function to generate data for File S5
% first, get a master.mat file from any MCMC run for the data in the main
% text
% then, make a parameters.csv file with one row, which has the parameter
% values in File S5
% this function combines the infomration in the parameters.csv and
% master.mat files to generate data with/without noise for the new
% parameter values

function print_data_different_params()

dir_string = 'input/different_params/';
% read in old parameter structure
load(strcat(dir_string,'master.mat'),'model_temp');
p = model_temp(1).parameters;
% read in new parameter values
parameters = csvread(strcat(dir_string,'parameters.csv'));
% replace old parameter values with new parameter values
for i = 1:length(p)
    p(i).value = parameters(i);
end

% generate single and sequential data with the new parameters
print_data_inner(p,'single')
print_data_inner(p,'sequential')

end

% generate single and sequential data with the new parameters
function print_data_inner(p,exp)
observables_array = generate_data(~strcmp(exp,'single'),p,.5,1);
csvwrite(strcat('input/different_params_data_',exp,'_noise1.csv'),observables_array);
observables_array = generate_data(~strcmp(exp,'single'),p,0,[]);
csvwrite(strcat('input/different_params_data_',exp,'_no_noise.csv'),observables_array);
end