% make csv for Zarnitsyna et al. data in file S6 with and without noise
function print_data_zarnitsyna()

% generate single and sequential infection data
for i = 0:1
    if(i == 0)
        filename = 'single';
    else
        filename = 'sequential';
    end
    % generate data with noise
    observables_array = generate_data_zarnitsyna(i,.5,1);
    csvwrite(strcat('input/zarnitsyna_data_',filename,'_noise1.csv'),observables_array);
    % generate data without noise
    observables_array = generate_data_zarnitsyna(i,0,[]);
    csvwrite(strcat('input/zarnitsyna_data_',filename,'_no_noise.csv'),observables_array);
end
end

% solve ODEs and add noise
% sequential: logical: 1 = sequential infection, 0 = single infection
% sigma: standard deviation of lognormal noise
% seed: random number generator seed
function observables_array = generate_data_zarnitsyna(sequential,sigma,seed)

% solve ODEs for viral load
observables = return_viral_load_zarnitsyna(sequential, 'baseline');

% add noise and observation threshold
observables_array = add_noise_and_threshold(observables, sigma, seed);
end