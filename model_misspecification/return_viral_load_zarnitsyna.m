% solve Zarnitysna et al. ODEs for different IEIs and put in struct (noise
% to be added later)
% sequential: logical: 1 for sequential infection, 0 for single infection
% model: 'baseline', 'XC', 'XIT', 'XI', 'XT', 'knockout_adaptive',
% 'knockout_innate','knockout_humoral','no_immune','knockout_cellular'
function observables = return_viral_load_zarnitsyna(sequential, model)

if(sequential)
    % inter-exposure intervals for sequential infection data
    IEI = [0, 1, 3, 5, 7, 10, 14];
else
    IEI = 0;
    % number of ferrets for single infection data set
    n_replicates = 13;
end

length_observables = 23;
t_end = 9;

for i = 1:length(IEI)
    sol = ODEmodel_zarnitsyna_for_gen_data(IEI(i), model, t_end);
    % format results into observables struct
    if(sequential)
        if(IEI(i) == 0)
            % inoculation timepoint of first infection not recorded
            sol.y = sol.y(2:end);
            observables.V11 = -1*ones(length_observables, 1);
            observables.V21 = observables.V11;
            observables.V21((IEI(end) + 1):end) = sol.y';
        else
            % inoculation timepoint of first infection not recorded
            sol.y = sol.y(:,2:end);
            for j = 1:2
                V_str = sprintf('V%d%d',j,i);
                observables.(V_str) = sol.y(j,:)';
                observables.(V_str) = ...
                    vertcat(-1*ones(length_observables - length(observables.(V_str)),1), ...
                    observables.(V_str));
                if(j == 2)
                    observables.(V_str)(1:IEI(end)) = -1;
                else
                    observables.(V_str)(IEI(end)) = -1;
                end
                
            end
        end
    else
        % inoculation timepoint not recorded
        sol.y = sol.y(2:end);
        for j = 1:n_replicates
            observables.(sprintf('V%d',j)) = sol.y';
        end
    end
end
end