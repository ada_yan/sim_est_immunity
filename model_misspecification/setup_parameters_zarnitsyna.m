% parameter values for Zarnitsyna et al. model, used with
% ODEmodel_zarnitsyna_for_gen_data to generate data in File S6
function p = setup_parameters_zarnitsyna()
p.beta = 3e-5;
p.p = 0.04;
p.c = 3;
p.delta = 1;
p.k_R = 0.014;
p.k_M = 8;
p.sigma_M = 2;
p.phi_M = 1;
p.d_M = 0.4;
p.gamma = 0.6;
p.d_A = 3.4;
p.rho = 4.3;
p.phi = 50;
p.mu = 2.4;
p.r = 0.14;
p.alpha = 0.8;
p.d_R = 0.2;
p.g = .5;
p.T_0 = 4e8;
p.V_0 = 10;
p.kappa = 0.01;
end