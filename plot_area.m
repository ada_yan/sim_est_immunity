% function to plot are with upper and lower bounds

% input arguments:
% time: vector of length n: x-values
% lower: vector of length n: y-values of the lower edge of the area
% upper: vector of length n: y-values of the upper edge of the area
% colour: something which specifies a colour, such as [0,0,0] or 'b'

% output argument:
% h: graphics handle to area

function h = plot_area(time,lower,upper,colour)
lower = squeeze(lower);
upper = squeeze(upper);
if(size(lower,1) == 1)
    lower = lower';
    upper = upper';
end
h1 = area(time,[lower,...
    upper-lower]);
h1(1).EdgeColor = 'none';
h1(2).EdgeColor = 'none';
h1(1).FaceColor = 'none';
h1(2).FaceColor = colour;
h = h1(2);
end