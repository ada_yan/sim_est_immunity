% wrapper for plotting an area showing the lower and upper percentiles of an observable
%
% input arguments:
% observables_prctile: either a two-dimensional or four-dimensional array.
% let the percentiles be a vector x of length m 
% and the times at which the percentiles are calculated be a vector t of length n.
% if observables_prctile is two-dimensional, the observables
% pertain to a single infection.
% observables_prctile is then an mxn array
% and observables_prctile(i,j) is the x(i)th percentile of the observable at time t(j).
% if observables_prctile is four-dimensional, the observables are the compartments of the ODE
% solutions for different exposure intervals.
% observables_prctile is then an mxkxnxq array where k is the number of inter-exposure intervals
% and q is the number of compartments in the ODE.
% days: vector of length r: x-values to plot
% max_IEI: scalar integer. maximum inter-exposure interval used in postprocessing
% (used to map days to indices in observables_prctile array)
% interval_idx: scalar integer.
% only used if observables_prctile is four-dimensional, to indicate index of inter-exposure interval to plot
% comp: scalar integer.
% only used if observables_prctile is four-dimensional, to indicate compartment of ODE solution to plot
% area_colour: something which specifies a colour, such as [0,0,0] or 'b'

% output argument:
% h: graphics handle to area

function h = plot_area_wrapper(observables_prctile,days,max_IEI,interval_idx,comp,area_colour)

if(length(days) == size(observables_prctile,ndims(observables_prctile)))
    plot_y_ind = 1:length(days);
else
    plot_y_ind = days+max_IEI+1;
end

if(ismatrix(observables_prctile))
    lower = observables_prctile(1,plot_y_ind);
else
    lower = observables_prctile(1,interval_idx,plot_y_ind,comp);
end

% if bounds for area are below the axis limit, set to axis limit to make
% figure look nicer
lower(lower < 1) = 1;

if(ismatrix(observables_prctile))
    upper = observables_prctile(end,plot_y_ind);
else
    upper = observables_prctile(end,interval_idx,plot_y_ind,comp);
end
% upper(upper == -1) = 1;
h = plot_area(days,lower,upper,area_colour);
end