% plots data as markers

% input arguments:
% time: vector of length n: x-values
% data: vector of length n: y-values
% marker_style: struct with fields marker, face_colour and edge_colour
% which contain a marker string and colour indicators (e.g. 0,0,0], or 'b') respectively

% output argument:
% h: graphics handle to line

function h = plot_data(time,data,marker_style)
h = semilogy(time,data,...
    'LineStyle','none','Marker',marker_style.marker,'MarkerFaceColor',marker_style.face_colour,...
    'MarkerEdgeColor',marker_style.edge_colour,'MarkerSize',10,'LineWidth',2);
end