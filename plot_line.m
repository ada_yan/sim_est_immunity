% plots data as markers

% input arguments:
% time: vector of length n: x-values
% data: vector of length n: y-values
% line_style: struct with fields LineStyle and Color

% output argument:
% h: graphics handle to line

function h = plot_line(time,data,line_style)
h = semilogy(time,data,...
    'LineStyle',line_style.LineStyle,'LineWidth',2,'Color',line_style.Color);
end