% plots in paper

function plot_sim_est_paper()
addpath('general_functions');
data_set = {'main', 'zarnitsyna','different_params'};
for i = 1:length(data_set)
plot_generated_data_control(data_set{i});
close all
plot_generated_data(data_set{i});
close all
plot_fits(data_set{i});
close all
plot_knockout(data_set{i});
close all
plot_restrict_xreact(data_set{i});
close all
if(~strcmp(data_set{i},'main'))
    continue
end
plot_restrict_xreact_trajectories();
close all
plot_LL();
close all
plot_posteriors();
close all
plot_vary_likelihood();
close all
end
end

% single infection figure for Fig. 1 in main text and File S2 Fig 1
% (same figure)

function plot_generated_data_control(data_set)

dir_string = generate_dir_strings(data_set, 1);
filenames.no_noise = strcat(dir_string.sequential.true,'baselineVR_true.mat');
filenames.noise_csv = make_noise_filename(data_set, 'single');

start_day = 14;
end_day = 9;
% maps indices in data to days post-infection
max_IEI = 20;
days = -max_IEI:50-max_IEI;

% true viral load
load(filenames.no_noise,'observables_true')
no_noise = observables_true;

% noisy data
noise = csvread(filenames.noise_csv);

temp = -1*ones(length(days), size(noise, 2));
temp(max_IEI+2:max_IEI+end_day + 1,:) = noise;
noise = temp;
noise(noise == 0) = 1;


threshold = 10;
ymax = 1e10;
style = setup_style();

% plot
close all
for j = 1:size(noise, 2)
    plot_data(days,noise(:,j),style.challenge_marker_noise);
    g = gca;
    g.YScale = 'log';
    hold on
end
plot_line(days,no_noise(1,days+max_IEI+1,2),style.challenge_line);
semilogy(days,threshold*ones(size(days)),'k:','LineWidth',2)
hold off


xlabel('Time (days)','Interpreter','latex')
ylabel('$V_{tot}$ (RNA copy no./100$\mu$L)','Interpreter','latex')

g.FontSize = 20;
axis([-start_day,end_day,1,ymax]);
h = gcf;
h.PaperPositionMode = 'auto';
draw_arrow(g, start_day, start_day + end_day, style.challenge_line.LineStyle);

filename_out = 'data_';

save_eps_fig(gcf,strcat(dir_string.figs,filename_out,'control'),1)


end

% sequential infection figures for Fig. 1 in main text, Fig S1, File S2 Fig
% 1, File S5 Fig 1, File S6 Fig 1
function plot_generated_data(data_set)

% for 'main' data set, have low and high cross-reactivity options
low_vec = make_low_vec(data_set);

for low = low_vec
    plot_generated_data_inner(data_set, low)
end

% input arguments:
% low: scalar logical: 1 = plot viral load for low cross-reactivity data
% 0 = plot viral load for high cross-reactivity data set in File S2

    function plot_generated_data_inner(data_set, low)
        dir_string = generate_dir_strings(data_set, low);
        filenames.no_noise = strcat(dir_string.sequential.true,'baselineVR_true.mat');
        if(low)
            filenames.noise_csv = make_noise_filename(data_set, 'sequential');
        else
            filenames.noise_csv = make_noise_filename(data_set, 'high_xreact');
        end
        
        % intervals to plot
        intervals_plot = [0,1,3,5,7,10,14];
        intervals = [0,1,3,5,7,10,14,2,6,20];
        intervals_idx = find(ismember(intervals,intervals_plot));
        
        start_day = intervals_plot(end);
        end_day = 9;
        % maps indices in data to days post-infection
        max_IEI = 20;
        days = -max_IEI:50-max_IEI;
        
        % true viral load
        load(filenames.no_noise,'observables_true')
        no_noise = observables_true;
        
        % noisy data
        noise = csvread(filenames.noise_csv);
        n_intervals_plot = length(intervals_plot);
        noise = [-1*ones(n_intervals_plot,size(noise,2));noise;...
            -1*ones(length(days)-n_intervals_plot-(end_day - start_day),size(noise,2))];
        noise(noise == 0) = 1;        
        
        threshold = 10;
        ymax = 1e10;
        style = setup_style();
        
        % plot
        for j =intervals_idx
            clear k
            close all
            k(1) = plot_data(days,noise(days+max_IEI+1,2*j),style.challenge_marker_noise);
            g = gca;
            g.YScale = 'log';
            hold on
            k(2) = plot_line(days,no_noise(j,days+max_IEI+1,2),style.challenge_line);
            if(intervals(j) > 0)
                k(3) = plot_data(days,noise(days+max_IEI+1,2*(j-1)+1),style.primary_marker_noise);
                k(4) = plot_line(days,no_noise(j,days+max_IEI+1,1),style.primary_line);
            end
            semilogy(days,threshold*ones(size(days)),'k:','LineWidth',2)
            hold off            
            
            xlabel('Time (days)','Interpreter','latex')
            ylabel('$V_{tot}$ (RNA copy no./100$\mu$L)','Interpreter','latex')
            if(intervals(j) == 1)
                ylabels = {'`true'' (primary)',...
                    '`true'' (challenge)',...
                    'noisy (primary)',...
                    'noisy (challenge)'};
                make_legend(k([4,2,3,1]),ylabels);
            end
            g.FontSize = 20;
            axis([-start_day,end_day,1,ymax]);
            h = gcf;
            h.PaperPositionMode = 'auto';
            draw_arrow(g, start_day, start_day + end_day, style.challenge_line.LineStyle);
            if(intervals(j) > 0)
                draw_arrow(g, start_day - intervals(j),...
                    start_day + end_day, style.primary_line.LineStyle);
            end
            
            filename_out = 'data_';
            
            save_eps_fig(gcf,strcat(dir_string.figs,filename_out,num2str(intervals(j))),1)
            
        end
        
    end
end

% plot Figs 2 & 4 in main text, File S2, File S5, File S6

function plot_fits(data_set)
% for 'main' data set, have low and high cross-reactivity options
low_vec = make_low_vec(data_set);
for low = low_vec
    for extend = [0,1]
        plot_fits_inner(data_set,low,extend)
    end
end
end

% input arguments:
% low: scalar logical: 1 = plot viral load for low cross-reactivity data
% 0 = plot viral load for high cross-reactivity data set in File S2
% extend: scalar logical: 1 = plot viral load for inter-exposure intervals
% absent from data
% 0 = plot viral load for inter-exposure intervals present in data
function plot_fits_inner(data_set,low, extend)
dir_string = generate_dir_strings(data_set,low);
filenames.sequential_prctile = strcat(dir_string.sequential.samples{1},'baselineVR_prctile.mat');
filenames.no_noise = strcat(dir_string.sequential.true,'baselineVR_true.mat');
filenames.single_prctile = ...
    strcat(dir_string.single.samples{1},'baselineVR_prctile.mat');


% true viral load
load(filenames.no_noise,'observables_true')
no_noise = observables_true;

% prediction intervals
load(filenames.sequential_prctile,'observables_prctile');
sequential_prctile = observables_prctile;

load(filenames.single_prctile,'observables_prctile');
single_prctile = observables_prctile;
if(extend)
    if(low)
        intervals_plot = [2,20];
    else
        intervals_plot = [2,6,20];
    end
else
    intervals_plot = [0,1,3,5,7,10,14];
end


intervals = [0,1,3,5,7,10,14,2,6,20];
if(low)
    intervals_main = [0,1,14,2,6,20];
else
    intervals_main = [0,1,7,14,2,6,20];
end
intervals_idx = find(ismember(intervals,intervals_plot));

% maps indices in data to days post-infection
max_IEI = max(intervals);
days = -max_IEI:50-max_IEI;

threshold = 10;
ymax = 1e10;
end_day = 9;
style = setup_style();

% plot
for j =intervals_idx
    close all
    clear k
    if(intervals(j) == 0)
        if(low)
            k(1) = plot_area_wrapper(single_prctile,days,max_IEI,j,2,style.single_area);
            g = gca;
            g.YScale = 'log';
            hold on
        end
        k(2) = plot_area_wrapper(sequential_prctile,days,max_IEI,j,2,style.sequential_area);
        g = gca;
        g.YScale = 'log';
        hold on
        k(3) = plot_line(days,no_noise(j,days+max_IEI+1,2),style.challenge_line);
    else
        if(extend)
            if(low)
                k(1) = plot_area_wrapper(single_prctile,days,max_IEI,j,2,style.single_area);
                g = gca;
                g.YScale = 'log';
                hold on
            end
            k(2) = plot_area_wrapper(sequential_prctile,days,max_IEI,j,2,style.sequential_area);
            g = gca;
            g.YScale = 'log';
            hold on
        else
            k(1) = plot_area_wrapper(sequential_prctile,days,max_IEI,j,1,style.primary_area);
            g = gca;
            g.YScale = 'log';
            hold on
            k(2) = plot_area_wrapper(sequential_prctile,days,max_IEI,j,2,style.sequential_area);
        end
        k(3) = plot_line(days,no_noise(j,days+max_IEI+1,1),style.primary_line);
        k(4) = plot_line(days,no_noise(j,days+max_IEI+1,2),style.challenge_line);
    end
    semilogy(days,threshold*ones(size(days)),'k:','LineWidth',2)
    hold off
    xlabel('Time (days)','Interpreter','latex')
    ylabel('$V_{tot}$ (RNA copy no./100$\mu$L)','Interpreter','latex')
    if(intervals(j) == 0 && low)
        ylabels = {'pred. by single',...
            'pred. by sequential'};
        make_legend(k([1,2]),ylabels);
    elseif(intervals(j) == 1 || intervals(j) == 2)
        ylabels = {'`true'' (primary)',...
            '`true'' (challenge)'};
        make_legend(k([3,4]),ylabels);
    elseif(intervals(j) == 14 || intervals(j) == 20)
        if(extend)
            if(low)
                ylabels = {'pred. by single',...
                    'pred. by sequential'};
                make_legend(k([1,2]),ylabels);
            else
                ylabels = {'pred.'};
                make_legend(k(2),ylabels);
            end
        else
            ylabels = {'pred. by sequential (primary)',...
                'pred. by sequential (challenge)'};
            make_legend(k([1,2]),ylabels);
        end
    end
    g.FontSize = 20;
    start_day = max(intervals_plot);
    axis([-start_day,end_day,1,ymax]);
    
    h = gcf;
    h.PaperPositionMode = 'auto';
    draw_arrow(g, start_day, start_day + end_day, style.challenge_line.LineStyle);
    if(intervals(j) > 0)
        draw_arrow(g, start_day - intervals(j),...
            start_day + end_day, style.primary_line.LineStyle);
    end
    filename_str = 'prctile';
    if(extend)
        filename_out = 'VR_extend_';
    else
        filename_out = 'VR_';
    end
    if(low)
        save_eps_fig(gcf,strcat(dir_string.figs_si,'low_xreact/',filename_out,num2str(intervals(j)),filename_str),1)
    end
    if(low || ismember(intervals(j),intervals_main))
        save_eps_fig(gcf,strcat(dir_string.figs,filename_out,num2str(intervals(j)),filename_str),1)
    end
end

end

% plot Fig. 3 in main text, File S2, File S5, File S6
function plot_knockout(data_set)

% for 'main' data set, have low and high cross-reactivity options
low_vec = make_low_vec(data_set);

for low = low_vec
    plot_knockout_inner(data_set,low)
end

% input arguments:
% low: scalar logical: 1 = plot viral load for low cross-reactivity data
% 0 = plot viral load for high cross-reactivity data set in File S2

    function plot_knockout_inner(data_set,low)
        if(low)
            model_names = {'sequential','single'};
        else
            model_names = {'sequential'};
        end
        
        dir_string = generate_dir_strings(data_set,low);
        knockout_names = {'baseline','knockout_adaptive','no_immune',...
            'knockout_innate','knockout_humoral','knockout_cellular'};
        
        for i = 1:length(knockout_names)
            for j = 1:length(model_names)
                % predictions of the viral load by the fitted model
                % when immune components are disabled
                
                filenames.(model_names{j}).(knockout_names{i}) = ...
                    strcat(dir_string.(model_names{j}).samples{1},knockout_names{i},'VR_prctile.mat');
                load(filenames.(model_names{j}).(knockout_names{i}),'observables_prctile');
                data.(model_names{j}).(knockout_names{i}) = observables_prctile;
                
            end
            % true viral load when immune components are disabled
            filenames.true.(knockout_names{i}) = ...
                strcat(dir_string.sequential.true,knockout_names{i},'VR_true.mat');
            load(filenames.true.(knockout_names{i}),'observables_true')
            % true viral load for baseline model
            if(strcmp(knockout_names{i},'baseline'))
                data.true.(knockout_names{i}) = ...
                    squeeze(observables_true(1,21:length(observables_true),2));
            else
                data.true.(knockout_names{i}) = observables_true;
            end
        end
        
        ymax = 1e10;
        days = 0:30;
        style = setup_style();
        
        % timing of innate immunity
        t_innate = 1;
        switch data_set
            case 'main'
                % timing of adaptive and cellular adaptive immunity
                t_adaptive = 4;
                % timing of humoral adaptive immunity
                t_humoral = 4;
            case 'zarnitsyna'
                t_adaptive = 2;
                t_humoral = 0;
            case 'different_params'
                t_adaptive = 3;
                t_humoral = 0;
        end
        % plot
        for i = 1:length(knockout_names)
            if(strcmp(knockout_names{i},'baseline'))
                continue
            end
            
            if(low)
                
                k(1) = plot_area_wrapper(data.single.(knockout_names{i}),days,0,[],[],style.single_area);
                g = gca;
                g.YScale = 'log';
                hold on
                k(2) = plot_area_wrapper(data.sequential.(knockout_names{i}),days,0,[],[],style.sequential_area);
            else
                k(1) = 0;
                k(2) = plot_area_wrapper(data.sequential.(knockout_names{i}),days,0,[],[],style.sequential_area);
                g = gca;
                g.YScale = 'log';
                hold on
            end
            k(3) = plot_line(days,data.true.baseline,style.baseline_line);
            k(4) = plot_line(days,data.true.(knockout_names{i}),style.primary_line);
            switch knockout_names{i}
                case {'knockout_innate','no_immune'}
                    t_immune = t_innate;
                case 'knockout_humoral'
                    t_immune = t_humoral;
                otherwise
                    t_immune = t_adaptive;
            end
            plot(t_immune*[1,1],[1,1e10],'Color',.3*ones(1,3),'LineWidth',2,'LineStyle',':')
            hold off
            xlabel('Time (days)','Interpreter','latex')
            ylabel('$V_{tot}$ (RNA copy no./100$\mu$L)','Interpreter','latex')
            g.FontSize = 20;
            axis([0,15,1,ymax]);
            if(strcmp(knockout_names{i},'knockout_adaptive'))
                leg = legend(k([3,4]),{'`true'' (baseline)','`true'' (immunity suppressed)'});
            elseif(low && strcmp(knockout_names{i},'no_immune'))
                leg = legend(k([2,1]),{'fitted (sequential)','fitted (single)'});
            end
            try
                leg.Interpreter = 'latex';
            end
            filename_out = strcat(knockout_names{i},'VR_');
            
            save_eps_fig(gcf,strcat(dir_string.figs,filename_out,'0prctile'),1)
            
        end
        
    end
end

% plot Fig. 5 in main text, File S2, File S5, File S6
function plot_restrict_xreact(data_set)
% for 'main' data set, have low and high cross-reactivity options
low_vec = make_low_vec(data_set);

for low = low_vec
    plot_restrict_xreact_inner(data_set, low)
end

% input arguments:
% low: scalar logical: 1 = plot viral load for low cross-reactivity data
% 0 = plot viral load for high cross-reactivity data set in File S2

    function plot_restrict_xreact_inner(data_set, low)
        restrict_names = {'XC','XIT','XI','XT','XI1','XI2','XI3'};
        restrict_names_si = {'XI1','XI2','XI3'};
        
        dir_string = generate_dir_strings(data_set, low);
        
        % true baseline viral load
        filenames.no_noise = strcat(dir_string.sequential.true,'baselineVR_true.mat');
        load(filenames.no_noise,'observables_true')
        no_noise = observables_true;
        
        for i = 1:length(restrict_names)
            if(ismember(restrict_names{i}, restrict_names_si))
                if(~low || ~strcmp(data_set, 'main'))
                    continue
                end
            end
            
            
            % predicted viral load for model where the components mediating
            % cross-protection are restricted
            filenames.sequential.(restrict_names{i}) = ...
                strcat(dir_string.sequential.samples{1},restrict_names{i},'VR_prctile.mat');
            load(filenames.sequential.(restrict_names{i}),'observables_prctile');
            data.sequential.(restrict_names{i}) = observables_prctile;
            
            % true viral load for model where the components mediating
            % cross-protection are restricted
            filenames.true.(restrict_names{i}) = ...
                strcat(dir_string.sequential.true,restrict_names{i},'VR_true.mat');
            load(filenames.true.(restrict_names{i}),'observables_true')
            data.true.(restrict_names{i}) = observables_true;
        end
        
        max_IEI = 20;
        days = -max_IEI:50-max_IEI;
        ymax = 1e10;

        intervals = [0,1,7,14];
        if(low)
            intervals_plot = 1;
        else
            intervals_plot = [1,7,14];
        end

        intervals_baseline = [0,1,3,5,7,10,14,2,6,20];
        intervals_idx = find(ismember(intervals,intervals_plot));
        intervals_baseline_idx = find(ismember(intervals_baseline,intervals_plot));
        style = setup_style();
        
        ylabels = {'`true'' (baseline)','`true'' (modified)', 'predicted (modified)'};
        
        % plot
        for j = 1:length(intervals_idx)
            for i = 1:length(restrict_names)
                clear k
            if(ismember(restrict_names{i}, restrict_names_si))
                if(~low || ~strcmp(data_set, 'main'))
                    continue
                end
            end
                
                k(1) = plot_area_wrapper(data.sequential.(restrict_names{i}),days,20,intervals_idx(j),2,style.sequential_area);
                g = gca;
                g.YScale = 'log';
                hold on
                % for zarnitsyna data, only calculated true viral load for
                % models XC etc for one-day IEI; for the other data,
                % calculated for IEIs of 1, 7 and 14 days, hence different
                % indexing
                if(strcmp(data_set,'zarnitsyna'))
                    k(2) = plot_line(days,squeeze(data.true.(restrict_names{i})(1,:,2)),style.challenge_line);
                else
                    k(2) = plot_line(days,squeeze(data.true.(restrict_names{i})(intervals_idx(j),:,2)),style.challenge_line);
                end
                k(3) = plot_line(days,no_noise(intervals_baseline_idx(j),days+max_IEI+1,2),style.baseline_line);
                hold off
                xlabel('Time (days)','Interpreter','latex')
                ylabel('$V_{tot}$ (RNA copy no./100$\mu$L)','Interpreter','latex')
                if((strcmp(restrict_names{i},'XC') || strcmp(restrict_names{i},'XI1'))...
                        && intervals(intervals_idx(j)) == 1)
                    leg = legend(k([3,2,1]),ylabels);
                    leg.Interpreter = 'latex';
                end
                
                g.FontSize = 20;
                axis([0,15,1,ymax]);
                if(ismember(restrict_names{i}, restrict_names_si))
                    save_dir = dir_string.figs_si;
                else
                    save_dir = dir_string.figs;
                end
                filename_out = strcat(save_dir,restrict_names{i},'_prctile',num2str(intervals(intervals_idx(j))));
                save_eps_fig(gcf,filename_out,1)
            end
        end
    end
end

% plot Fig S2
function plot_restrict_xreact_trajectories()

model = {'XI','XT'};
data_set = 'main';
dir_string = generate_dir_strings(data_set,1);

% true baseline viral load
filenames.no_noise = strcat(dir_string.sequential.true,'baselineVR_true.mat');
load(filenames.no_noise,'observables_true')
no_noise = observables_true;

for i = 1:length(model)
% predicted viral load for model where the components mediating
% cross-protection are restricted
filenames.sequential.(model{i}) = ...
    strcat(dir_string.sequential.samples{1},model{i}, 'VR_trajectories.mat');
load(filenames.sequential.(model{i}),'observables');
data.sequential.(model{i}) = observables;

% true viral load for model where the components mediating
% cross-protection are restricted
filenames.true.(model{i}) = ...
    strcat(dir_string.sequential.true,model{i}, 'VR_true.mat');
load(filenames.true.(model{i}),'observables_true')
data.true.(model{i}) = observables_true;


max_IEI = 20;
days = -max_IEI:50-max_IEI;
ymax = 1e10;
intervals = [0,1,7,14];

intervals_plot = 1;

intervals_baseline = [0,1,3,5,7,10,14,2,6,20];
intervals_idx = find(ismember(intervals,intervals_plot));
intervals_baseline_idx = ismember(intervals_baseline,intervals_plot);
style = setup_style();

ylabels = {'`true'' (baseline)',...
    strcat('`true'' (', model{i}, ')'), ...
    strcat('predicted (', model{i}, ')'),...
    strcat('incorrect (', model{i}, ')')};

% plot
close all
n_lines = size(data.sequential.(model{i}),1);
for j = 1:n_lines
    % hard-coded selection criteria for the 'incorrect' trajectory
    if(strcmp(model{i}, 'XI'))
        incorrect = squeeze(data.sequential.(model{i})(j,intervals_idx,max_IEI + 4,2)) > 1e5;
    else
        incorrect = squeeze(data.sequential.(model{i})(j,intervals_idx,max_IEI + 3,2)) < 1e5;
    end
    if(incorrect)
        k(j) = semilogy(days,squeeze(data.sequential.(model{i})(j,intervals_idx,:,2)),...
            'Color',style.sequential_area_darker, 'LineWidth',2);
        incorrect_idx = j;
    else
        k(j) = semilogy(days,squeeze(data.sequential.(model{i})(j,intervals_idx,:,2)),...
            'Color',style.sequential_area);
    end
    hold on
end
k(n_lines + 1) = plot_line(days,squeeze(data.true.(model{i})(intervals_idx,:,2)),style.challenge_line);
k(n_lines + 2) = plot_line(days,no_noise(intervals_baseline_idx,days+max_IEI+1,2),style.baseline_line);
hold off
xlabel('Time (days)','Interpreter','latex')
ylabel('$V_{tot}$ (RNA copy no./100$\mu$L)','Interpreter','latex')

leg = legend(k([n_lines + 2, n_lines + 1, 1, incorrect_idx]),ylabels, 'FontSize',18);
leg.Interpreter = 'latex';

g = gca;
g.FontSize = 20;
axis([0,15,1,ymax]);
filename_out = strcat(dir_string.figs,model{i},'_trajectories',num2str(intervals_plot));
save_eps_fig(gcf,filename_out,1)
end

end

% File S3 Fig 1
function plot_LL()
for sequential = [0,1]
    plot_LL_subfigure(sequential)
end

% input argument:
% sequential: logical.
% 1 = plot for sequential infection data (Fig. 1 c-d)
% 0 = plot for single infection data (Fig. 1 a-b)

    function plot_LL_subfigure(sequential)
        dir_string = generate_dir_strings(data_set,1);
        
        if(sequential)
            exp = 'sequential';
        else
            exp = 'single';
        end
        
        burn_in = csvread(strcat(dir_string.(exp).samples{1},'burn_in.csv'));
        
        % log likelihood for true parameters
        LL = csvread(strcat(dir_string.(exp).true,'log_likelihood.csv'));
        LL_baseline = LL(1,2);
        
        % log likelihood at start of MCMC chain (including burn-in and
        % adaptive iterations)
        LL_start = csvread(strcat(dir_string.(exp).start{2},'log_likelihood.csv'));
        LL = csvread(strcat(dir_string.(exp).samples{2},'log_likelihood.csv'));
        start = find(LL(:,1) == 0);
        if(length(start) > 1)
            LL(start(2:end),:) = [];
        end
        
        % x-values: LL was written to file every 5 iterations
        LL_interval = 5;
        start_plot = 1+ceil(burn_in/LL_interval)*2;
        iter = size(LL,1);
        x_data = LL_interval*ceil(0:.5:(iter-start_plot)*.5);
        
        LL_range = 100;
        start_iter = LL_range/LL_interval*2+1;
        all_LLs = [LL_start(1:start_iter,2);LL(start_plot:iter,2);LL_baseline];
        ylim(1) = min(all_LLs);
        ylim(2) = max(all_LLs);
        x_data_start = LL_interval*ceil(0:.5:(start_iter-1)*.5);
        x_data_start = x_data_start(1:2:end);
        
        plot(x_data_start,LL_start(1:2:start_iter,2),'Marker','x','LineStyle','none',...
            'LineWidth',2,'MarkerSize',10)
        hold on
        plot([0,LL_range],LL_baseline*ones(1,2),'k','LineWidth',2)
        hold off
        axis([0,LL_range,ylim(1),ylim(2)])
        xlabel({'Iterations';'from start'},'Interpreter','latex')
        ylabel('Log likelihood','Interpreter','latex')
        h = gca;
        h.FontSize = 32;
        if(sequential)
            folder_name = 'low_xreact/';
        else
            folder_name = 'single_infection/';
        end
        save_eps_fig(gcf,strcat(dir_string.figs_si,folder_name,'LL1'),1)
        plot(x_data,LL(start_plot:iter,2))
        hold on
        plot(x_data,LL_baseline*ones(size(x_data)),'k','LineWidth',2)
        hold off
        ylim = [-1200,-1100];
        h = gca;
        h.FontSize = 32;
        axis([x_data(1),x_data(end),ylim(1),ylim(2)]);
        xlabel({'Iterations';'after burn-in'},'Interpreter','latex')
        ylabel('Log likelihood','Interpreter','latex')
        save_eps_fig(gcf,strcat(dir_string.figs_si,folder_name,'LL2'),1)
        
    end
end

% File S4 marginal prior and posterior figures 
function plot_posteriors()
exp = {'prior','single','sequential'};
for k = 1:length(exp)
    plot_posteriors_subfn(exp{k})
end

% input argument:
% exp: string indicating whether to plot samples from the prior,
% the model fitted to single infection data or
% the model fitted to sequential infection data.

    function plot_posteriors_subfn(exp)
        data_set = 'main';
        dir_string = generate_dir_strings(data_set,1);
        
        load(strcat(dir_string.sequential.true,'master.mat'),'model_temp')
        load(strcat(dir_string.sequential.true,'parameters.csv'))
        parameters_baseline = parameters(1,:);
        pnames = {model_temp(1).parameters.pname};
        n_parameters = length(pnames);
        
        combined_chains = cell(size(pnames));
        for i = 1:length(combined_chains)
            combined_chains{i} = [];
        end
        
        pnames_xlabel = return_pnames_xlabel();
        range = return_range();
        burn_in = csvread(strcat(dir_string.(exp).samples{1},'burn_in.csv'));
        
        % load parameters
        for i = 1:length(dir_string.(exp).samples)
            load(strcat(dir_string.(exp).samples{i},'parameters.csv'))
            
            for j = 1:size(parameters,2)
                if(strcmp(pnames{j},'pF_on_mean_pF'))
                    continue;
                end
                thinned_parameters = parameters(burn_in+1:end,j);
                combined_chains{j} = [combined_chains{j};thinned_parameters];
            end
        end
        
        switch exp
            case 'prior'
                folder_name = 'prior/';
            case 'single'
                folder_name = 'single_infection/';
            case 'sequential'
                folder_name = 'low_xreact/';
        end
        
        % plot histograms
        for j = 1:size(parameters,2)
            if(strcmp(pnames{j},'pF_on_mean_pF'))
                continue;
            end
            histogram(combined_chains{j},linspace(range(j,1),range(j,2),20));
            axis([range(j,1),range(j,2),0,length(combined_chains{j})/2]);
            hold on
            % plot true value
            if(~strcmp(exp,'prior'))
                plot(parameters_baseline(j)*ones(1,2),[0,length(combined_chains{j})],'r','LineWidth',10);
            end
            hold off
            h = gca;
            h.FontSize = 32;
            xlabel(pnames_xlabel{j},'Interpreter','latex');
            ylabel('Frequency','Interpreter','latex');
            h.Position = [0.25,0.35,.73,.5];
            h.LabelFontSizeMultiplier = 1.3;
            a = gcf;
            a.PaperPositionMode = 'auto';
            save_eps_fig(gcf,strcat(dir_string.figs_si,folder_name,'marginals/',pnames{j}),1)
        end
        
    end
end

% File S4 Fig 3
function plot_vary_likelihood()
data_set = 'main';
dir_string = generate_dir_strings(data_set,1);
load(strcat(dir_string.sequential.true,'master.mat'),'model_temp')
pnames = {model_temp(1).parameters.pname};
load(strcat(dir_string.sequential.true,'LL_vary_parameters.mat'),'LL','LL_default',...
    'baseline_values','varied_values')
pnames_xlabel = return_pnames_xlabel();
max_LL = -Inf;
for i = 1:length(pnames)
    if(strcmp(pnames{i},'deltaA') || strcmp(pnames{i},'deltaB'))
        max_LL = max([max_LL,LL{i}]);
    end
end
for i = 1:length(pnames)
    if(strcmp(pnames{i},'deltaA') || strcmp(pnames{i},'deltaB'))
        plotted_values = find(LL{i} > -Inf);
        % plot LL as parameter value is varied
        plot(varied_values{i}(plotted_values),LL{i}(plotted_values),'k','Marker','x','LineStyle','none','MarkerSize',20,'LineWidth',5)
        hold on
        % plot LL for true parameter value
        plot(varied_values{i}(plotted_values),LL_default*ones(size(plotted_values)),'r:','LineWidth',5)
        % plot true parameter value
        plot(baseline_values(i)*ones(1,2),[LL_default-30,max_LL],'r:','LineWidth',5)
        hold off
        g = gca;
        g.FontSize = 32;
        xlabel(pnames_xlabel{i},'Interpreter','latex','Interpreter','latex')
        ylabel('Log likelihood','Interpreter','latex')
        axis([varied_values{i}(plotted_values(1)),varied_values{i}(plotted_values(end)),...
            LL_default-30,max_LL+10])
        a = gcf;
        a.PaperPositionMode = 'auto';
        save_eps_fig(gcf,strcat(dir_string.figs_si,'low_xreact/LL_vary',pnames{i}),1)
    end
end
end

function leg = make_legend(handle, labels)
leg =  legend(handle,labels,'Interpreter','latex','FontSize',18,...
    'Location','northwest');
end

% return names of parameters in LaTeX formatting

function pnames_xlabel = return_pnames_xlabel()
pnames_xlabel = {'g','\delta_F','\phi','T_0','\rho','\alpha',...
    'R_0','r','\delta_I','(\delta_{Vinf} - \delta_{Vtot})','\delta_{Vtot}',...
    '','\kappa_F','s',...
    '\kappa_A','\delta_A','\delta_B','k_B','\beta_B',...
    '\tau_B','p_{Vratio}','\gamma','V_0',...
    '\beta_C','\delta_E','\tau_M','\tau_E','\epsilon',...
    'k_{C11}','k_{C12}','\kappa_{E11}','\sigma'};
pnames_xlabel = strcat('$\log_{10}',pnames_xlabel,'$');
pnames_xlabel{length(pnames_xlabel)} = '$\sigma$';
end

% return filename for noisy data
% data_set: 'main', 'zarnitsyna' or 'different_params'
% exp: 'sequential' or 'single'
function filename = make_noise_filename(data_set, exp)
filename = strcat('input/',data_set, '_data_', exp, '_noise1.csv');
if(strcmp(data_set, 'main'))
    filename = strrep(filename, 'main', 'ferret');
end
end

% for 'main' data set, have low and high cross-reactivity options
function low_vec = make_low_vec(data_set)
if(strcmp(data_set,'main'))
    low_vec = [0,1];
else
    low_vec = 1;
end
end