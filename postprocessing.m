% postprocessing of MCMC chains e.g. making predictions

% input argument:
% mex_flag: logical. 1 iff solving ODEs using precompiled files.
% data_set: 'main', 'zarnitsyna' or 'different_params'

function postprocessing(mex_flag, data_set)

intervals = [1,3,5,7,10,14,2,6,20];

switch data_set
    case 'main'
        exp = {'sequential','high_xreact','single'};
    otherwise
        exp = {'sequential','single'};
end

for i = 1:length(exp)
    dir_string = generate_dir_strings(data_set, ~strcmp(exp{i},'high_xreact'));
    if(strcmp(exp{i},'high_xreact'))
        dir_string_base = dir_string.sequential;
    else
        dir_string_base = dir_string.(exp{i});
    end
    
    burn_in = csvread(strcat(dir_string_base.samples{1},'burn_in.csv'));
    burn_in = burn_in(end,end); % sometimes burn_in.csv is incorrectly formatted -- quick fix        
    
    % if data set was generated using our model (i.e. not the Zarnitsyna et al model),
    % calculate fitted model predictions for viral load of baseline model
    calc_true = ~strcmp(data_set, 'main');
    if(calc_true)
        calc_observables_prctile_octave({dir_string_base.true},0,'',...
            0,1,'baseline',intervals,mex_flag)
    end
    calc_observables_prctile_octave(dir_string_base.samples,burn_in,1e4,...
        0,0,'baseline',intervals,mex_flag)
    
%     calculate true viral load and fitted model predictions
%     when immune components are absent
    calc_viral_load_knockout_func(dir_string_base.true,dir_string_base.samples,burn_in,mex_flag, calc_true)
    % calculate true viral load and fitted model predictions
    % when the mechanisms mediating cross-protection are restricted
    calc_viral_load_separate_func(dir_string_base.true,dir_string_base.samples,burn_in,mex_flag, calc_true)
    
    % calculate log likelihood around true parameter values    
    if(calc_true && strcmp(data_set, 'main') && strcmp(exp{i}, 'sequential'))
        likelihood_vary_parameters(dir_string.sequential.true)
    end
end
end