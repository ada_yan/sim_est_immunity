% function to calculate percentiles in Octave for n-d arrays
% in the body of the function, I pasted the body of the prctile function provided by Matlab.
% I have not provided this code in the repository because it is proprietary; please paste
% in the appropriate code before running (or write your own function).

function y = prctile_octave(x,p,dim)
%PRCTILE Percentiles of a sample.
%   Y = PRCTILE(X,P) returns percentiles of the values in X.  P is a scalar
%   or a vector of percent values.  When X is a vector, Y is the same size
%   as P, and Y(i) contains the P(i)-th percentile.  When X is a matrix,
%   the i-th row of Y contains the P(i)-th percentiles of each column of X.
%   For N-D arrays, PRCTILE operates along the first non-singleton
%   dimension.

error('prctile_octave undefined: see source code')

end