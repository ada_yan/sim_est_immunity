% wrapper for generating synthetic data and writing to file

% input argument:
% exp: string. 'single','single_fixed_xreact','sequential' or 'sequential_high_xreact'.

function print_data(exp)
if(strcmp(exp,'sequential_high_xreact'))
    filename = 'high_xreact';
else
    filename = exp;
end
load(strcat('results/start/parameters_',exp,'.mat'));
observables_array = generate_data(~strcmp(exp,'single'),p,.5,1);
csvwrite(strcat('input/ferret_data_',filename,'_noise1.csv'),observables_array);
observables_array = generate_data(~strcmp(exp,'single'),p,0,[]);
csvwrite(strcat('input/ferret_data_',filename,'_no_noise.csv'),observables_array);
end