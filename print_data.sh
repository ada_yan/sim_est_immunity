# generates all synthetics data used in the study
# split up into Matlab and Octave for historical reasons -- could do all in Octave

#!/bin/bash    
octave --eval 'addpath(genpath("MCMC_library_octave"));
addpath(genpath("general_functions"));
gen_parameters("single");
gen_parameters("sequential");
gen_parameters("sequential_high_xreact");'
matlab -r "addpath(genpath('general_functions'));print_data('single'); print_data('sequential');print_data('sequential_high_xreact');quit;"
