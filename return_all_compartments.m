% returns solution to model ODEs for all compartments

% input arguments:
% p: struct array of the form created by contruct_parameters_single
% values: vector of the same length as p. Contains the parameter values for which 
% we're solving the ODE.
% model: struct created by construct_data_and_model
% intervals: empty or a vector of integers of length m. If empty, this function returns the compartments
% for a single infection. If non-empty, this function returns the compartments
% for a single infection and for the inter-exposure intervals specified.
% mex_flag: logical. 1 iff solving ODEs using precompiled files.

% output arguments:
% observables: 2*(m+1)xnxq array containing the solutions to the ODEs
% for the m inter-exposure intervals, n timepoints and q compartments.

function observables = return_all_compartments(p,values,model,intervals,mex_flag)

% put values in struct form
for i = 1:length(values)
    p(i).value = values(i);
end

% define index which is zero days post-secondary-infection
sync_day = 20;
% integrate for a period of 50 days in total
end_day = 50;
end_integrate_time = end_day - sync_day;
% timepoints of integration
discrete_times = 0:end_integrate_time;

switch model
    case 'baseline'
        nV = 2;
        c = compartment_labelling(nV,model);
        % make a map of indices such that when we solve for the viral load
        % of the first strain before the second strain is introduced, we
        % can slot the results into the correct indices of the array which
        % stores the results for both strains
        
        % where the strain 1 solutions go for the two-strain ferrets
        keep_new = [c.T,c.R,c.I(1),c.V(1),c.F,c.C([1,3]),reshape(c.E(:,[1,3])',1,[]),c.M([1,3]),...
            c.B(:,1)',c.P(1),c.A(1),c.VR(1)];
        % where the strain 1 solutions go for the control ferret
        keep_new_control = [c.T,c.R,c.I(2),c.V(2),c.F,c.C([2,3]),reshape(c.E(:,[2,3])',1,[]),c.M([2,3]),...
            c.B(:,2)',c.P(2),c.A(2),c.VR(2)];
    case {'knockout_adaptive','no_immune','knockout_humoral','knockout_cellular','knockout_innate'}
        nV = 1;
        c = compartment_labelling(nV,model);
    case 'XC'
        nV = 2;
        c = compartment_labelling(nV,model);
        keep_new = [c.T(1),c.R(1),c.I(1),c.V(1),c.F(1),c.C([1,3]),reshape(c.E(:,[1,3])',1,[]),c.M([1,3]),...
            c.B(:,1)',c.P(1),c.A(1),c.VR(1)];
        keep_new_control = [c.T(2),c.R(2),c.I(2),c.V(2),c.F(2),c.C([2,3]),reshape(c.E(:,[2,3])',1,[]),c.M([2,3]),...
            c.B(:,2)',c.P(2),c.A(2),c.VR(2)];
    case 'XIT'
        nV = 2;
        c = compartment_labelling(nV,model);
        keep_new = [c.T,c.R,c.I(1),c.V(1),c.F,c.C([1,2]),reshape(c.E(:,[1,2])',1,[]),c.M([1,2]),...
            c.B(:,1)',c.P(1),c.A(1),c.VR(1)];
        keep_new_control = [c.T,c.R,c.I(2),c.V(2),c.F,c.C([3,4]),reshape(c.E(:,[3,4])',1,[]),c.M([3,4]),...
            c.B(:,2)',c.P(2),c.A(2),c.VR(2)];
    case 'XI'
        nV = 2;
        c = compartment_labelling(nV,model);
        keep_new = [c.T(1),c.R(1),c.I(1),c.V(1),c.F,c.C([1,2]),reshape(c.E(:,[1,2])',1,[]),c.M([1,2]),...
            c.B(:,1)',c.P(1),c.A(1),c.VR(1)];
        keep_new_control = [c.T(end),c.R(end),c.I(2),c.V(2),c.F,c.C([3,4]),reshape(c.E(:,[3,4])',1,[]),c.M([3,4]),...
            c.B(:,2)',c.P(2),c.A(2),c.VR(2)];
    case 'XT'
        nV = 2;
        c = compartment_labelling(nV,model);
        keep_new = [c.T,c.R(1),c.I(1),c.V(1),c.F(1),c.C([1,2]),reshape(c.E(:,[1,2])',1,[]),c.M([1,2]),...
            c.B(:,1)',c.P(1),c.A(1),c.VR(1)];
        keep_new_control = [c.T,c.R(2),c.I(2),c.V(2),c.F(2),c.C([3,4]),reshape(c.E(:,[3,4])',1,[]),c.M([3,4]),...
            c.B(:,2)',c.P(2),c.A(2),c.VR(2)];
    case {'XI1','XI2','XI3'}
        nV = 2;
        c = compartment_labelling(nV,model);
        keep_new = [c.T(1),c.R(1),c.I(1),c.V(1),c.F(1),c.C([1,2]),reshape(c.E(:,[1,2])',1,[]),c.M([1,2]),...
            c.B(:,1)',c.P(1),c.A(1),c.VR(1)];
        keep_new_control = [c.T(end),c.R(end),c.I(2),c.V(2),c.F(end),c.C([3,4]),reshape(c.E(:,[3,4])',1,[]),c.M([3,4]),...
            c.B(:,2)',c.P(2),c.A(2),c.VR(2)];
    otherwise
        error('unknown model')
end

% if we need the solution for sequential infection
if(nV == 2)
    uncalculated = 1:c.VR(end);
    uncalculated(keep_new) = [];
    uncalculated_control = 1:c.VR(end);
    uncalculated_control(keep_new_control) = [];
    observables = -1*ones((length(intervals)+1),end_day+1,c.VR(end));
    
    nV = 1;
    [p1,~] = transform(p,model);
    c = compartment_labelling(nV,model);
    iv = find_iv_subset(p,c,model);
    
    % indicces of strain 1 compartments
    keep_old = [c.T,c.R,c.I,c.V,c.F,c.C,reshape(c.E',1,[]),c.M,...
    c.B',c.P,c.A,c.VR];
    
    % solve ODE for first strain
    [sol_before,~] = solve_ode(model,p1,iv,c,1,...
        discrete_times,end_integrate_time,mex_flag);
    
    % slot ODE solutions for first strain into correct places in array
    for i = 1:length(intervals)
        days = sync_day-intervals(i)+1:end_day+1;
        days = days(1:min(length(days),length(discrete_times)));
        observables(i+1,days,keep_new) = sol_before.y(keep_old,1:length(days))';
        observables(i+1,days,uncalculated) = 0;
    end
    days = sync_day+1:min(end_day+1,sync_day+length(sol_before.t)+1);
    observables(1,days,keep_new_control) = sol_before.y(keep_old,1:length(days))';
    observables(1,days,uncalculated_control) = 0;
    
    nV = 2;
    c = compartment_labelling(nV,model);
    iv = find_iv_subset(p,c,model);
    end_integrate_time = end_day - sync_day;
    discrete_times = 0:end_integrate_time;
    
    % solve for first and second strains
    for i = 1:length(intervals)
        % make new initial conditions from final state of one-strain
        % integration
        iv(keep_new) = sol_before.y(keep_old,sol_before.t == intervals(i));
        [sol_after,~] = solve_ode(model,p1,iv,c,0,...
            discrete_times,end_integrate_time,mex_flag);
        observables(i+1,sync_day+1:sync_day+length(sol_after.t),:) =...
            sol_after.y';
        
    end
else
    % if we only need solutions for one strain
    [p1,~] = transform(p,model);
    c = compartment_labelling(nV,model);
    iv = find_iv_subset(p,c,model);
    [sol_before,~] = solve_ode(model,p1,iv,c,1,...
        discrete_times,discrete_times(end),mex_flag);
    observables= sol_before.y';
end
end