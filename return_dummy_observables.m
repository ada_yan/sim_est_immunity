% for the purposes of sampling from the prior,
% the likelihood doesn't depend on the data --
% just return 0 for the data

function observables = return_dummy_observables(~)

observables.V = 0;
end
