% wrapper for returning log likelihood
% the way in which this is written makes more sense if we have a hierarchical model

% input arguments
% p: struct array of the form created by contruct_parameters_single,
% containing the parameter values for which we want to calculate the likelihood
% data: struct created by construct_data_and_model
% model: struct created by construct_data_and_model
% LL_old: log likelihood at current position of MCMC chain
% recalc: logical: 1: calculate the likelihood at the parameter values
% 0: if LL_old is provided, the new LL is the same as LL_old (e.g. if we already calculated
% the LL at the previous step of the MCMC chain, or if the model were hierarchical and
% the parameter being changed only affects part of the likelihood calculation)

% output arguments:
% LL: scalar. log likelihood
% is_upper_bound: logical. If the integration failed at some timepoints, LL is an upper
% bound of the actual likelihood.

function [LL,is_upper_bound] = return_likelihood(p,data,model,~,LL_old,recalc)

is_upper_bound = 0;

if(isempty(LL_old))
    LL_old = Inf;
end
if(~recalc && LL_old ~= Inf)
    LL = LL_old;
else
    LL = model.equations.LL_p_prior(p);
    if(LL ~= -Inf)
        observables = model.equations.return_observables(p);
        sigma = p(end).value;
        [LL_new,is_upper_bound_new] =  model.equations.LL_data_p(data,observables,sigma);
        LL = LL + LL_new;
        is_upper_bound = is_upper_bound + is_upper_bound_new;
    end
end

end

