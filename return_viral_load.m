% returns solution to model ODEs for total viral load for baseline model

% input arguments:
% p: struct array of the form created by contruct_parameters_single,
% with the parameter values for which
% we're solving the ODE.
% sequential: logical: 1 if solving the ODEs for different inter-exposure intervals,
% 0 if solving for single infection only
% mex_flag: logical. 1 iff solving ODEs using precompiled files.

% output arguments:
% observables: struct of the same form as data.

function observables = return_viral_load(p,sequential,mex_flag)

if(sequential)
    intervals = [1,3,5,7,10,14]; % inter-exposure intervals
    % define index which is zero days post-secondary-infection
    sync_day = 14;
    end_day = 23; % integrate for a period of 23 days in total
    observables_temp = -1*ones(end_day,(length(intervals)+1)*2);
    
    end_integrate_time = sync_day;
    discrete_times = 1:end_integrate_time;
    
    nV = 1;
    [p1,~] = transform(p,'baseline');
    c = compartment_labelling(nV,'baseline');
    iv = find_iv_subset(p,c,'baseline');
    
    % solve ODE for first strain
    [sol_before,fail_flag] = solve_ode('baseline',p1,iv,c,1,...
        discrete_times,end_integrate_time,mex_flag);
    
    % slot ODE solutions for first strain into correct places in array
    for i = 1:length(intervals)
        days = sync_day-intervals(i)+1:min(sync_day-intervals(i)+length(sol_before.t),sync_day-1);
        observables_temp(days,2*i+1) = sol_before.y(c.VR,1:length(days))';
    end
    
    days = sync_day+1:min(end_day,sync_day+length(sol_before.t));
    observables_temp(days,2) = sol_before.y(c.VR,1:length(days))';
    
    if(fail_flag == 0)
        % solve for first and second strains
        keep_old = [c.T,c.R,c.I(1),c.V(1),c.F,c.C,reshape(c.E',1,[]),c.M,...
            c.B',c.P(1),c.A(1),c.VR(1)];
        
        nV = 2;
        c = compartment_labelling(nV,'baseline');
        iv = find_iv_subset(p,c,'baseline');
        keep_new = [c.T,c.R,c.I(1),c.V(1),c.F,c.C([1,3]),reshape(c.E(:,[1,3])',1,[]),c.M([1,3]),...
            c.B(:,1)',c.P(1),c.A(1),c.VR(1)];
        end_integrate_time = end_day - sync_day;
        discrete_times = 1:end_integrate_time;
        
        for i = 1:length(intervals)
            % make new initial conditions from final state of one-strain
            % integration
            iv(keep_new) = sol_before.y(keep_old,sol_before.t == intervals(i));
            [sol_after,fail_flag] = solve_ode('baseline',p1,iv,c,0,...
                discrete_times,end_integrate_time,mex_flag);
            if(fail_flag == 0 && (length(sol_after.t) ~= end_day - sync_day))
                error('observables of wrong length\n')
            end
            observables_temp(sync_day+1:sync_day+length(sol_after.t),2*i+1:2*i+2) =...
                sol_after.y(c.VR,:)';
        end
    end
    
    % make array into struct
    count = 1;
    for i = 1:length(intervals)+1
        for j = 1:2
            observables.(sprintf('V%d%d',j,i)) = observables_temp(:,count);
            count = count+1;
        end
    end
else
    end_integrate_time = 9;
    discrete_times = 1:end_integrate_time;
    
    nV = 1;
    [p1,~] = transform(p,'baseline');
    c = compartment_labelling(nV,'baseline');
    iv = find_iv_subset(p,c,'baseline');
    n_replicates = 13;
    
    [sol_before,~] = solve_ode('baseline',p1,iv,c,1,...
        discrete_times,end_integrate_time,mex_flag);
    for i = 1:n_replicates
        observables.(sprintf('V%d',i)) = sol_before.y(end,:)';
    end
end
end
