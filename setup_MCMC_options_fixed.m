% sets up parameters to run an MCMC chain from a fixed starting point
% for a small (1e4) number of iterations

% output argument:
% MCMC_options: struct containing the MCMC running parameters

function MCMC_options = setup_MCMC_options_fixed()

    no_temperatures = 7; 
    max_temperature = 50;    
    min_temperature = 1;
    temperatures = min_temperature*(max_temperature/min_temperature)...
        .^((0:(no_temperatures - 1))/(no_temperatures - 1));

    MCMC_options = struct('iterations_per_swap',5,'total_iterations',1e4,...
        'proposal_width_weight',linspace(.05,.5,no_temperatures),...
        'temperatures',temperatures,'random_start',0,...
        'calibrate_temperatures',10,'calibrate_proposal_widths',10); 
end