% defines plotting styles

% output argument:
% style: struct containing colours and marker shapes

function style = setup_style()
grey = .5*ones(1,3);
blue = [.5,.5,1];
green = [.5,1,.5];
dark_green = [0,.5,0];

primary_colour = 'k'; % 
challenge_colour = 'k';
noise_primary_marker = '+';
noise_challenge_marker = 'x';
default_linestyle = '-';
baseline_linestyle = ':';
primary_linestyle = '--';
default_color = 'k';
baseline_color = 'r';
style.primary_line.LineStyle = primary_linestyle;
style.primary_line.Color = default_color;
style.baseline_line.LineStyle = baseline_linestyle;
style.baseline_line.Color = baseline_color;
style.primary_marker_noise.face_colour = primary_colour;
style.primary_marker_noise.edge_colour = primary_colour;
style.primary_marker_noise.marker = noise_primary_marker;
style.challenge_line.LineStyle = default_linestyle;
style.challenge_line.Color = default_color;
style.challenge_marker_noise.face_colour = challenge_colour;
style.challenge_marker_noise.edge_colour = challenge_colour;
style.challenge_marker_noise.marker = noise_challenge_marker;
style.sequential_area = blue;
style.single_area = green;
style.primary_area = grey;
style.sequential_area_darker = dark_green;
end