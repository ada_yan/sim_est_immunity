% solve model ODEs either before or after the second strain is introduced

% input arguments:
% model: string. 'baseline','knockout_humoral','knockout_cellular','knockout_adaptive',
% 'no_immune', 'XIT', 'XI', 'XC','XI1', 'XI2' or 'XI3'
% p: vector of parameter values
% iv: vector of initial conditions
% c: struct labelling compartments created by compartment_labelling
% before: logical.
% 1 = solving before second strain is introduced,
% 0 = solving after second strain is introduced
% discrete_times: vector of times at which to solve
% end_integrate_time: in practice this is discrete_times(end)
% mex_flag: logical. 1 iff solving ODEs using precompiled files.

% output arguments:
% sol: struct containing fields t and y: solution to the ODE
% fail_flag: integer: historically used for dealing with errors, now doesn't do anything

function [sol,fail_flag] = solve_ode(model,p,iv,c,before,...
    discrete_times,end_integrate_time,mex_flag)

if(before)
    [sol,fail_flag] = solve_ode_before(model,p,iv,c,discrete_times,end_integrate_time,mex_flag);
else
    [sol,fail_flag] = solve_ode_after(model,p,iv,c,discrete_times,end_integrate_time,mex_flag);
end

end

% solve model ODEs before the second strain is introduced

% input and output arguments: same as solve_ODE

function [sol,fail_flag] = solve_ode_before(model,p,iv,c,discrete_times,end_integrate_time,mex_flag)
nV = 1;
threshold = 1e-1;

% solve virus 1 up until iei or end of first infection, whichever comes
% first
[sol.t,sol.y,~,fail_flag] = solve_truncate(model,[],[],[0,end_integrate_time],discrete_times,iv,p,1,c,threshold,nV,mex_flag);

if(fail_flag == 0)
    if(sol.t(end) < end_integrate_time)
        iv = sol.y(:,end);
        % set all viral load and infected cells to 0 (to avoid attoviruses)
        iv([c.I,c.V]) = 0;
        % continue integrating with remaining strain until end
        % integrating time
        [sol.t,sol.y,~,fail_flag] = solve_truncate...
            (model,sol.t,sol.y,[sol.t(end),end_integrate_time],discrete_times,...
            iv,p,[],c,threshold,nV,mex_flag);
    end
end

sol.y(sol.y < 0) = 0; % machine precision means solution sometimes goes negative

% return solution at discrete times
if(~isempty(discrete_times))
    sol.y = sol.y(:,ismember(sol.t,discrete_times));
    sol.t = sol.t(ismember(sol.t,discrete_times));
end

end

% solve model ODEs before the second strain is introduced

% input and output arguments: same as solve_ODE

function [sol,fail_flag] = solve_ode_after(model,p,iv,c,...
    discrete_times,end_integrate_time,mex_flag)
nV = 2;
threshold = 1e-1;
truncated = (iv(c.V(1)) < threshold) && (iv(c.I(1)) < threshold);

if(truncated)
    % if infection with virus 1 has finished by iei
    iv([c.I(1),c.V(1)]) = 0;
    [sol.t,sol.y,~,fail_flag] = solve_truncate(model,[],[],[0,end_integrate_time],...
        discrete_times,iv,p,2,c,threshold,nV,mex_flag);
else
    % solve for both viruses 1 and 2 until one infection finishes
    [sol.t,sol.y,truncated,fail_flag] = solve_truncate(model,[],[],...
        [0,end_integrate_time],discrete_times,iv,p,1:nV,c,threshold,nV,mex_flag);
    
    if(fail_flag == 0 && sol.t(end) < end_integrate_time) % if one of the infections finishes before the end integrating time
        iv = sol.y(:,end);
        % set viral load and infected cells for finished virus to 0 (to avoid attoviruses)
        iv([c.I(truncated),c.V(truncated)]) = 0;
        % continue integrating with remaining strain until end
        % integrating time
        not_truncated = 1:nV;
        not_truncated(truncated) = [];
        [sol.t,sol.y,~,fail_flag] = solve_truncate...
            (model,sol.t,sol.y,[sol.t(end),end_integrate_time],discrete_times,...
            iv,p,not_truncated,c,threshold,nV,mex_flag);
    end
end

if((fail_flag == 0) && (sol.t(end) < end_integrate_time))
    iv = sol.y(:,end);
    % set all viral load and infected cells to 0 (to avoid attoviruses)
    iv([c.I,c.V]) = 0;
    % continue integrating with remaining strain until end
    % integrating time
    [sol.t,sol.y,~,fail_flag] = solve_truncate...
        (model,sol.t,sol.y,[sol.t(end),end_integrate_time],discrete_times,...
        iv,p,[],c,threshold,nV,mex_flag);
end

sol.y(sol.y < 0) = 0; % machine precision means solution sometimes goes negative

% return solution at discrete times
if(~isempty(discrete_times))
    sol.y = sol.y(:,ismember(sol.t,discrete_times));
    sol.t = sol.t(ismember(sol.t,discrete_times));
end

end

% truncate solution if both the number of infected cells and the amount of
% infectious virus for one strain go below the threshold

% input arguments:
% sol_t: vector of times at which the ODE was solved
% sol_y: solution of ODEs at the above times
% strains: vector of integers. Indicates which strains (1, 2 or both) are above the threshold
% at the start of ODE solving.
% c: same as solve_ODE
% threshold: scalar

% output arguments:
% sol_t: truncated version of input sol_t
% sol_y: truncated version of input sol_y
% truncated: vector of integers indicating which strains were truncated

function [sol_t,sol_y,truncated] = truncate_at_threshold(sol_t,sol_y,strains,c,threshold)
if(length(strains) == 1)
    truncate = find(sol_y(c.V(strains),:) < threshold & sol_y(c.I(strains),:) < threshold,1);
    if(isempty(truncate))
        truncate = Inf; % indicates no truncation occurred
    end
else
    truncate = zeros(length(strains),1);
    for i = 1:length(strains)
        truncate_temp = find(sol_y(c.V(strains(i)),:) < threshold & sol_y(c.I(strains(i)),:) < threshold,1);
        if(isempty(truncate_temp))
            truncate(i) = Inf; % have to allocate some value to indicate no truncation occurred
        else
            truncate(i) = truncate_temp;
        end
    end
    [truncate,which_truncate] = min(truncate);
end
if(truncate == Inf)
    truncated = 0;
else
    sol_t = sol_t(1:truncate);
    sol_y = sol_y(:,1:truncate);
    if(length(strains) == 1)
        truncated = 1;
    else
        truncated = strains(which_truncate);
    end
end
end

% given an interval over which to solve an ODE and a set of measurement time points,
% returns the times at which the ODE needs to be solved
% (basically appending the start and end times if necessary)

% input arguments:
% solving interval: vector of length 2: the interval over which to solve the ODE
% discrete_times: empty or vector: measuring times within the interval

% output arguments:
% timespan: vector: the times at which the ODE needs to be solved
function timespan = make_timespan(solving_interval,discrete_times)
if(isempty(discrete_times))
    timespan = solving_interval;
else
    timespan = unique([solving_interval(1),...
        discrete_times(discrete_times > solving_interval(1) &...
        discrete_times < solving_interval(2)),solving_interval(2)]);
end
end

% solve the ODE (continuing from previous solution) and truncate at the appropriate point

% input arguments:
% model: string: same as solve_ODE
% sol_t: vector: times for which the ODE has been solved already (for concatenation to new solution)
% sol_y: matrix: solution of ODE at those times (for concatenation to new solution)
% solving interval: vector of length 2: the interval over which to solve the ODE
% discrete_times: empty or vector: measuring times within the interval
% c: struct: same as solve_ODE
% threshold: scalar (same as truncate_at_threshold)
% nV: scalar integer:
% 1 if solving for single infection, or before the second strain is introduced
% 2 if solving after second strain is introduced
% mex_flag: same as solve_ODE

% output arguments:
% sol_t: vector: new times for which the ODE was solved, concatenated to the input sol_t
% sol_y: matrix: solution to the ODE at those times, concatenated to the input sol_y
% truncated: vector of integers indicating which strains were truncated
% fail_flag: same as solve_ODE

function [sol_t,sol_y,truncated,fail_flag] = solve_truncate(model,sol_t,sol_y,solving_interval,discrete_times,iv,p,...
    strains,c,threshold,nV,mex_flag)

int_tol = [1e-6 1e-8 1]; % absolute tolerance, relative tolerance, and max
% computing time to spend on integration
fail_flag = 1; % to keep track of integration failures

% times at which to solve
timespan = make_timespan(solving_interval,discrete_times);
compartment_map = 1:length(iv);

% Each single-strain model is associated with two ODEs: ODEfn11 and ODEfn10.
% Each two-strain model is additionally associated with ODEfn22, ODEfn21 and ODEfn20.
% ODEfnij solves the ODEs for the case where we are modelling the presence of i strains 
% in the host, but only j out of those i strains have more than 0 infected cells
% or infectious virions (although the total viral load/associated immune 
% compartments may be non-zero.)
% I could have used the same ODE for all of these cases, but I found that explicitly
% excluding the infected cell and viral load compartments when the infection has
% finished helps avoid numerical errors.

% enumerating all the ODEs...

if(mex_flag)
    switch model
        case {'baseline', 'knockout_innate'}
            if(IsOctave)
                ODEfn11 = @ODE_1strain_c; % precompiled mex files
                ODEfn10 = @ODE_1strain_strain0_c;
                ODEfn22 = @ODE_2strain_c;
                ODEfn21 = @ODE_2strain_strain1_c;
                ODEfn20 = @ODE_2strain_strain0_c;
            else
                ODEfn11 = @ODE_1strain_matlab_c;
                ODEfn10 = @ODE_1strain_strain0_matlab_c;
                ODEfn22 = @ODE_2strain_matlab_c;
                ODEfn21 = @ODE_2strain_strain1_matlab_c;
                ODEfn20 = @ODE_2strain_strain0_matlab_c;
            end
        case 'knockout_humoral'
            ODEfn11 = @ODE_1strain_knockout_A_c;
            ODEfn10 = @ODE_1strain_strain0_knockout_A_c;
        case {'knockout_cellular','knockout_adaptive'}
            ODEfn11 = @ODE_1strain_knockout_E_c;
            ODEfn10 = @ODE_1strain_strain0_knockout_E_c;
        case 'no_immune'
            ODEfn11 = @ODE_1strain_no_immune_c;
            ODEfn10 = @ODE_1strain_strain0_no_immune_c;
        case 'XIT'
            ODEfn11 = @ODE_1strain_c;
            ODEfn10 = @ODE_1strain_strain0_all_c;
            ODEfn22 = @ODE_2strain_XIT_c;
            ODEfn21 = @ODE_2strain_strain1_XIT_c;
            ODEfn20 = @ODE_2strain_strain0_XIT_c;
        case 'XT'
            ODEfn11 = @ODE_1strain_c;
            ODEfn10 = @ODE_1strain_strain0_all_c;
            ODEfn22 = @ODE_2strain_XT_c;
            ODEfn21 = @ODE_2strain_strain1_XT_c;
            ODEfn20 = @ODE_2strain_strain0_XT_c;
        case 'XI'
            ODEfn11 = @ODE_1strain_c;
            ODEfn10 = @ODE_1strain_strain0_all_c;
            ODEfn22 = @ODE_2strain_XI_c;
            ODEfn21 = @ODE_2strain_strain1_XI_c;
            ODEfn20 = @ODE_2strain_strain0_XI_c;
        case 'XC'
            ODEfn11 = @ODE_1strain_c;
            ODEfn10 = @ODE_1strain_strain0_all_c;
            ODEfn22 = @ODE_2strain_XC_c;
            ODEfn21 = @ODE_2strain_strain1_XC_c;
            ODEfn20 = @ODE_2strain_strain0_XC_c;
        case 'XI1'
            ODEfn11 = @ODE_1strain_c;
            ODEfn10 = @ODE_1strain_strain0_all_c;
            ODEfn22 = @ODE_2strain_XI1_c;
            ODEfn21 = @ODE_2strain_strain1_XI1_c;
            ODEfn20 = @ODE_2strain_strain0_XI1_c;
        case 'XI2'
            ODEfn11 = @ODE_1strain_c;
            ODEfn10 = @ODE_1strain_strain0_all_c;
            ODEfn22 = @ODE_2strain_XI2_c;
            ODEfn21 = @ODE_2strain_strain1_XI2_c;
            ODEfn20 = @ODE_2strain_strain0_X0_c;
        case 'XI3'
            ODEfn11 = @ODE_1strain_c;
            ODEfn10 = @ODE_1strain_strain0_all_c;
            ODEfn22 = @ODE_2strain_XI3_c;
            ODEfn21 = @ODE_2strain_strain1_XI3_c;
            ODEfn20 = @ODE_2strain_strain0_X0_c;
    end
else
    switch model
        case {'baseline', 'knockout_innate'}
            if(IsOctave)
                ODEfn11 = @ODE_1strain; % .m files with no precompiling
                ODEfn10 = @ODE_1strain_strain0;
                ODEfn22 = @ODE_2strain;
                ODEfn21 = @ODE_2strain_strain1;
                ODEfn20 = @ODE_2strain_strain0;
            else
                ODEfn11 = @ODE_1strain_matlab;
                ODEfn10 = @ODE_1strain_strain0_matlab;
                ODEfn22 = @ODE_2strain_matlab;
                ODEfn21 = @ODE_2strain_strain1_matlab;
                ODEfn20 = @ODE_2strain_strain0_matlab;
            end
        case 'knockout_humoral'
            ODEfn11 = @ODE_1strain_knockout_A;
            ODEfn10 = @ODE_1strain_strain0_knockout_A;
        case {'knockout_cellular','knockout_adaptive'}
            ODEfn11 = @ODE_1strain_knockout_E;
            ODEfn10 = @ODE_1strain_strain0_knockout_E;
        case 'no_immune'
            ODEfn11 = @ODE_1strain_no_immune;
            ODEfn10 = @ODE_1strain_strain0_no_immune;
        case 'XIT'
            ODEfn11 = @ODE_1strain;
            ODEfn10 = @ODE_1strain_strain0_all;
            ODEfn22 = @ODE_2strain_XIT;
            ODEfn21 = @ODE_2strain_strain1_XIT;
            ODEfn20 = @ODE_2strain_strain0_XIT;
        case 'XT'
            ODEfn11 = @ODE_1strain;
            ODEfn10 = @ODE_1strain_strain0_all;
            ODEfn22 = @ODE_2strain_XT;
            ODEfn21 = @ODE_2strain_strain1_XT;
            ODEfn20 = @ODE_2strain_strain0_XT;
        case 'XI'
            ODEfn11 = @ODE_1strain;
            ODEfn10 = @ODE_1strain_strain0_all;
            ODEfn22 = @ODE_2strain_XI;
            ODEfn21 = @ODE_2strain_strain1_XI;
            ODEfn20 = @ODE_2strain_strain0_XI;
        case 'XC'
            ODEfn11 = @ODE_1strain;
            ODEfn10 = @ODE_1strain_strain0_all;
            ODEfn22 = @ODE_2strain_XC;
            ODEfn21 = @ODE_2strain_strain1_XC;
            ODEfn20 = @ODE_2strain_strain0_XC;
        case 'XI1'
            ODEfn11 = @ODE_1strain;
            ODEfn10 = @ODE_1strain_strain0_all;
            ODEfn22 = @ODE_2strain_XI1;
            ODEfn21 = @ODE_2strain_strain1_XI1;
            ODEfn20 = @ODE_2strain_strain0_XI1;
        case 'XI2'
            ODEfn11 = @ODE_1strain;
            ODEfn10 = @ODE_1strain_strain0_all;
            ODEfn22 = @ODE_2strain_XI2;
            ODEfn21 = @ODE_2strain_strain1_XI2;
            ODEfn20 = @ODE_2strain_strain0_X0;
        case 'XI3'
            ODEfn11 = @ODE_1strain;
            ODEfn10 = @ODE_1strain_strain0_all;
            ODEfn22 = @ODE_2strain_XI3;
            ODEfn21 = @ODE_2strain_strain1_XI3;
            ODEfn20 = @ODE_2strain_strain0_X0;
    end
end

while(fail_flag > 0 && fail_flag < 4) % keep looping if fail_flag, up to 3 times
    switch length(c.V)
        case 1
            switch length(strains)
                case 1 % active infection with one strain
                    [sol_t_temp,sol_y_temp] = wrapper(ODEfn11,...
                        compartment_map,timespan,iv,p,int_tol,mex_flag);
                case 0 % infection with one strain is over
                    switch model
                        case {'baseline', 'knockout_cellular','knockout_adaptive','knockout_innate'}
                        	% the compartment maps are used to exclude
                        	% unnecessary compartments from the initial
                        	% conditions once infection with a given strain is over
                            compartment_map([c.I,c.V,c.B(1)]) = 0;
                        case {'knockout_humoral','XIT','XI','XC','XT','XI1','XI2','XI3'}
                            compartment_map([c.I,c.V]) = 0;
                        case 'no_immune'
                            compartment_map([c.T,c.I,c.V]) = 0;
                    end
                    [sol_t_temp,sol_y_temp] = wrapper(ODEfn10,...
                        compartment_map,timespan,iv,p,int_tol,mex_flag);
                otherwise
                    error('unknown number of strains')
            end
        case 2
            switch length(strains)
                case 2 % active infection with two strains
                    [sol_t_temp,sol_y_temp] = wrapper(ODEfn22,...
                        compartment_map,timespan,iv,p,int_tol,mex_flag);
                case 1 % host was infected with two strains, but infection with one is over
                    exc_strain = 1:nV;
                    exc_strain(strains) = [];
                    switch model
                        case 'baseline'
                            compartment_map([c.I(exc_strain),c.V(exc_strain),c.C(exc_strain),...
                                reshape(c.E(:,exc_strain),1,[]),c.M(exc_strain),...
                                reshape(c.B(:,exc_strain),1,[]),c.P(exc_strain),...
                                c.A(exc_strain)]) = 0;
                            % the compartment map is written for the case
                            % where infection with the first strain is finished
                            % but infection with the second strain is ongoing.
                            % if the converse is true, flip the compartment map
                            if(strains == 2)
                                compartment_map(end-1:end) = fliplr(compartment_map(end-1:end));
                            end
                        case {'XIT','XT','XI','XC','XI1','XI2','XI3'}
                            if(strains == 2)
                                compartment_map = flip_map(model,compartment_map,c);
                            end
                            compartment_map([c.I(exc_strain),c.V(exc_strain)]) = 0;
                    end
                    [sol_t_temp,sol_y_temp] = wrapper(ODEfn21,...
                        compartment_map,timespan,iv,p,int_tol,mex_flag);
                case 0 % infection with both strains is over
                    switch model
                        case 'baseline'
                            compartment_map(1:c.A(2)) = 0;
                        case {'XIT','XT','XI','XC','XI1','XI2','XI3'}
                            compartment_map([c.I,c.V]) = 0;
                    end
                    [sol_t_temp,sol_y_temp] = wrapper(ODEfn20,...
                        compartment_map,timespan,iv,p,int_tol,mex_flag);
                otherwise
                    error('unknown number of strains')
            end
        otherwise
            error('more than 2 strains')
    end
    
    % find where integration failed if applicable
    int_fail_at = find(sol_t_temp ~= timespan,1);
    
    % truncate indicated strains when they reach below threshold
    if(isempty(strains))
        truncated = 0;
    else
        [sol_t_temp,sol_y_temp,truncated] = ...
        truncate_at_threshold(sol_t_temp,sol_y_temp,strains,c,threshold);
    end

    % if integration failed and truncation hasn't been applied before the
    % point at which the integration failed, increase the integration tolerance
    % and try again
    if(~isempty(int_fail_at) && length(sol_t_temp) > int_fail_at - 2)
        fail_flag = fail_flag + 1;
        int_tol(3) = int_tol(3)*10;
    else
        fail_flag = 0;
    end
end

% detect if integration ended prematurely
if(fail_flag)
    fprintf('integration failure \n')
    fail_flag = 1;
    sol_t_temp = timespan(1:length(sol_t_temp));
end

% concatenate to previous solution
try
	% if previous solution exists
    if(~isempty(sol_t) && sol_t(end) == sol_t_temp(1))
        sol_t = [sol_t(1:end-1),sol_t_temp];
        sol_y = [sol_y(:,1:end-1),sol_y_temp];
    else
    	% if solving for the first time
        sol_t = [sol_t,sol_t_temp];
        sol_y = [sol_y,sol_y_temp];
    end
catch
    fprintf('empty sol_t_temp \n')
    fail_flag = 2;
end
end

% wrapper for solving ODEs

% input arguments:
% odefn: function handle for ODE to be solved
% compartment_map: vector of length m, with n non-zero entries where n is the
% number of compartments in the ODE
% timespan: vector of timepoints at which to solve ODE
% iv: vector of length m: initial values
% p: vector of parameter values
% int_tol: vector of integration tolerances
% mex_flag: same as for solve_ODE

% output arguments:
% sol_t: times at which ODE was solved
% sol_y: solution of ODE at those timepoints

function [sol_t,sol_y] = wrapper(odefn,compartment_map,timespan,iv,p,int_tol,mex_flag)

% make map to extract relevant compartments (generally the non-zero compartments)
exc_compartments = find(compartment_map == 0);
compartments_new = compartment_map;
compartments_new(exc_compartments) = [];

% initialise solution array
sol_y = zeros(length(iv),length(timespan));

% solve for non-zero compartments
if(mex_flag)
    [sol_t,sol_y_temp] = odefn(timespan,iv(compartments_new),p,[],int_tol);
else
    [sol_y_temp,~,~] = lsode(@(x,t) odefn(t,x,p,[]),iv(compartments_new),timespan);
    sol_y_temp = sol_y_temp';
    sol_t = timespan;
end

% if timespan is of length greater than 2, the solver returns the solution at
% the times indicated in timespan. If timespan is of length 2, the solver returns
% additional timepoints in between, so need to exclude these

if(length(timespan) == 2)
    sol_t = [sol_t(1),sol_t(end)];
    sol_y_temp = [sol_y_temp(:,1),sol_y_temp(:,end)];
end

% slot solutions for relevant compartments into correct rows of array
sol_y(compartments_new,:) = sol_y_temp;

% assume that irrelevant compartments stayed at their initial values
if(~isempty(exc_compartments))
    sol_y(exc_compartments,:) = repmat(iv(exc_compartments),1,length(timespan));
end
end

% function to flip a compartment map
% (see solve_truncate: the compartment map is written for the case
% where infection with the first strain is finished
% but infection with the second strain is ongoing.
% if the converse is true, flip the compartment map)

% input arguments:
% model: string. 'baseline','XIT', 'XI', 'XC','XI1', 'XI2' or 'XI3'
% map_in: vector. input compartment map
% c: struct: same as for solve_ODE

% output argument:
% map_out: vector. flipped map

function map_out = flip_map(model,map_in,c)
map_out = map_in;

map_out(c.B(:,1)) = map_in(c.B(:,2));
map_out(c.B(:,2)) = map_in(c.B(:,1));
map_out(c.P) = fliplr(map_in(c.P));
map_out(c.A) = fliplr(map_in(c.A));
map_out(c.VR) = fliplr(map_in(c.VR));

switch model
    case 'XIT'
        map_out(c.C([1,2])) = map_in(c.C([3,4]));
        map_out(c.C([3,4])) = map_in(c.C([1,2]));
        temp1 = c.E(:,[1,2]);
        temp2 = c.E(:,[3,4]);
        temp1 = reshape(temp1,1,[]);
        temp2 = reshape(temp2,1,[]);
        map_out(temp1) = map_in(temp2);
        map_out(temp2) = map_in(temp1);
        map_out(c.M([1,2])) = map_in(c.M([3,4]));
        map_out(c.M([3,4])) = map_in(c.M([1,2]));
    case 'XT'
        map_out(c.R) = fliplr(map_in(c.R));
        map_out(c.F) = fliplr(map_in(c.F));
        map_out(c.C([1,2])) = map_in(c.C([3,4]));
        map_out(c.C([3,4])) = map_in(c.C([1,2]));
        temp1 = c.E(:,[1,2]);
        temp2 = c.E(:,[3,4]);
        temp1 = reshape(temp1,1,[]);
        temp2 = reshape(temp2,1,[]);
        map_out(temp1) = map_in(temp2);
        map_out(temp2) = map_in(temp1);
        map_out(c.M([1,2])) = map_in(c.M([3,4]));
        map_out(c.M([3,4])) = map_in(c.M([1,2]));
    case 'XI'
        map_out(c.T) = fliplr(map_in(c.T));
        map_out(c.R) = fliplr(map_in(c.R));
        map_out(c.C([1,2])) = map_in(c.C([3,4]));
        map_out(c.C([3,4])) = map_in(c.C([1,2]));
        temp1 = c.E(:,[1,2]);
        temp2 = c.E(:,[3,4]);
        temp1 = reshape(temp1,1,[]);
        temp2 = reshape(temp2,1,[]);
        map_out(temp1) = map_in(temp2);
        map_out(temp2) = map_in(temp1);
        map_out(c.M([1,2])) = map_in(c.M([3,4]));
        map_out(c.M([3,4])) = map_in(c.M([1,2]));
    case 'XC'
        map_out(c.T) = fliplr(map_in(c.T));
        map_out(c.R) = fliplr(map_in(c.R));
        map_out(c.F) = fliplr(map_in(c.F));
        map_out(c.C(1:2)) = fliplr(map_in(c.C(1:2)));
        map_out(c.E(:,1)) = map_in(c.E(:,2));
        map_out(c.E(:,2)) = map_in(c.E(:,1));
        map_out(c.M(1:2)) = fliplr(map_in(c.M(1:2)));
    case {'XI1','XI2','XI3'}
        map_out(c.T) = fliplr(map_in(c.T));
        map_out(c.R) = fliplr(map_in(c.R));
        map_out(c.F) = fliplr(map_in(c.F));
        map_out(c.C([1,2])) = map_in(c.C([3,4]));
        map_out(c.C([3,4])) = map_in(c.C([1,2]));
        temp1 = c.E(:,[1,2]);
        temp2 = c.E(:,[3,4]);
        temp1 = reshape(temp1,1,[]);
        temp2 = reshape(temp2,1,[]);
        map_out(temp1) = map_in(temp2);
        map_out(temp2) = map_in(temp1);
        map_out(c.M([1,2])) = map_in(c.M([3,4]));
        map_out(c.M([3,4])) = map_in(c.M([1,2]));
end
end