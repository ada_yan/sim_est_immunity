% transforms a parameter struct into a vector of parameter values

% input arguments:
% p: struct array of the form created by construct_parameters_single
% model: struct of the form created by construct_data_and_model

% output arguments:
% p_out: vector of parameter values
% in_range_auxiliary: logical. whether the transformed variables
% beta and pV are in range of the prior

function [p_out,in_range_auxiliary] = transform(p,model)
p_out = [p.value];
types = {p.type};
names = {p.pname};

% unlog everything except sigma
p_out(~strcmp(types,'O')) = 10.^p_out(~strcmp(types,'O'));

% extract parameters needed to calculate beta and pV
R_0 = p_out(strcmp(names,'R_0'));
r = p_out(strcmp(names,'r'));
delta_I = p_out(strcmp(names,'deltaI'));
delta_V = p_out(strcmp(names,'deltaV_minus_deltaVR')) + ...
    p_out(strcmp(names,'deltaVR'));
T_0 = p_out(strcmp(names,'T_0'));

% use beta and pV instead of R_0 and r when solving DE
beta1_T_0 = (-r^2-delta_I*r)/(r - delta_I*(R_0 - 1)) - delta_V;
pV = R_0*delta_I*(beta1_T_0 + delta_V)/beta1_T_0;
beta1 = beta1_T_0/T_0;

in_range_auxiliary = (beta1 > 1e-12 && beta1 < 1e-4 && pV > 1e-6 && pV < 1e6);

% replace the entries in o_out for R_0 and r with those for beta and pV
p_out(strcmp(names,'R_0')) = beta1;
p_out(strcmp(names,'r')) = pV;

% exclude parameters which don't exist in the ODEs for given models
switch model
    case {'baseline','XC','XIT','XI','XT',...
            'XI1','XI2','XI3','no_immune'}
        p_out(strcmp(types,'O')|strcmp(names,'gamma_0')|strcmp(names,'V_0')) = [];
    case 'knockout_humoral'
        p_out(strcmp(types,'O')|strcmp(names,'gamma_0')|strcmp(names,'V_0')|...
            strcmp(names,'pA_times_kappaA_times_B_0')|strcmp(names,'deltaA')|...
            strcmp(names,'deltaB')|strcmp(names,'kB')|strcmp(names,'betaB')|...
            strcmp(names,'tauB')) = [];
    case 'knockout_cellular'
        p_out(strcmp(types,'O')|strcmp(types,'T')|strcmp(types,'B')|...
            strcmp(names,'gamma_0')|strcmp(names,'V_0')) = [];
    case 'knockout_adaptive'
        p_out(strcmp(names,'pA_times_kappaA_times_B_0')) = 0;
        p_out(strcmp(types,'O')|strcmp(types,'T')|strcmp(types,'B')|...
            strcmp(names,'gamma_0')|strcmp(names,'V_0')) = [];
    case 'knockout_innate'
        p_out(strcmp(names,'kappaF_times_mean_pF')) = 0;
        p_out(strcmp(names,'s_times_mean_pF')) = 0;
        p_out(strcmp(names,'phi_times_mean_pF')) = 0;
        p_out(strcmp(types,'O')|strcmp(names,'gamma_0')|strcmp(names,'V_0')) = [];
    otherwise
        error('unknown model')
end
end